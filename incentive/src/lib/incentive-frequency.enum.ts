export enum IncentiveFrequency {
  /** The Weekly. */
  Weekly = 0,
  /** The Biweekly. */
  Biweekly = 1,
  /** The Semi_Monthly. */
  Semi_Monthly = 2,
  /** The Monthly */
  Monthly = 3,
}
export namespace IncentiveFrequency {
  export function toString(incentiveFrequency: IncentiveFrequency): string {
    return IncentiveFrequency[incentiveFrequency];
  }
  export function getEnum(incentiveFrequency: String): IncentiveFrequency {
    let tmp!: IncentiveFrequency;
    switch (incentiveFrequency) {
      case 'Weekly':
        tmp = IncentiveFrequency.Weekly;
        break;
      case 'Biweekly':
        tmp = IncentiveFrequency.Biweekly;
        break;
      case 'Semi_Monthly':
        tmp = IncentiveFrequency.Semi_Monthly;
        break;
      case 'Monthly':
        tmp = IncentiveFrequency.Monthly;
        break;
      default:
        tmp = IncentiveFrequency.Monthly;
        break;
    }
    return tmp;
  }
}
