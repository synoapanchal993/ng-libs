// import { TenantReport } from "./tenantReport.model";
// import { TenantComponent } from "./tenantComponent.model";
// import { IndustryMenu } from './industryMenu.model';
// import { IndustryRoute } from './industryRoute.model';
// import { MetricAuditStatus } from './metricAuditStatus.model';
// import { Permission } from './permission.model';
// import { Dashboard } from './dashboard.model';
// import { RoleMediaAccess } from './roleMediaAccess.model';

import { IndustryRoute } from './industryRoute.model';
import { IndustryMenu } from './industryMenu.model';
import { Permission } from './permission.model';

export class Role {
  intId: number = -1;
  description?: string;
  name?: string;
  shortName?: string;
  mapTo?: string = 'ROLE';
  newRoleName?: string;
  tenantId?: number;
  tenantName?: string;
  weight?: number;
  dashboardRights?: any = {};
  dashboardNavigations?: any = {};
  displayName?: string;
  // reports?: TenantReport[];
  // components?: TenantComponent[];
  roleIndustryMenu?: IndustryMenu[];
  roleIndustryRoute?: IndustryRoute[];
  // metricAuditStatus?: MetricAuditStatus[];
   rolePermissions?: Permission[];
  // roleMediaAccess?: RoleMediaAccess[];
  // roleDashboards?: Dashboard[];
  dashboardUrl?: string;
  roleTypeId?: number;
}
