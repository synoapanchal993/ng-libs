import { ExportData } from './export-data';

export class IncentiveSearchForm {
  customFilters: any[] | null = null;
  tenantLocationId: number | null = null;
  userIds: number[] | null = null;
  activeStatus: number | null = null;
  fromDate?: string;
  toDate?: string;
  orderBy?: string = 'updatedOn';
  sortBy?: string = 'DESC';
  page?: number = 0;
  pageSize?: number = 10;
  totalPages?: number = 0;
  // To export excel
  exportData?: ExportData;
  currentUser?: any= null;
  tenantId?: any= null;
  tenantType?: any= null;

}
