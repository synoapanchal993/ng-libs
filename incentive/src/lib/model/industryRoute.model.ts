export class IndustryRoute {
  activeStatus!: string;
  defaultAllow!: boolean;
  isOpen!: boolean;
  url!: string;
  name!: string;
  allowInVirtualTenant!: boolean;
}
