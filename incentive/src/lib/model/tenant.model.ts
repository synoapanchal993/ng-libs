export class Tenant {
  intId: number = -1;
  id!: number;
  name: string = '';
  address?: string;
  cfo?: string;
  column1?: string;
  company?: string;
  coo?: string;
  department?: string;
  ebitda?: number;
  email?: string;
  emailCFO?: string;
  emailCOO?: string;

  country?: string;
  geographyId?: number;
  currency?: string;
  locality?: string;
  postalCode?: string;
  state?: string;
  hotels?: number;
  jsonData?: string;
  landline?: string;
  landline2?: string;
  landlineCOO?: string;
  mobile?: string;
  mobileCFO?: string;
  mobileCOO?: string;
  oldId?: number;
  phone?: string;
  president?: string;
  activeStatus?: string;
  turnover?: number;
  geographyAddress?: string;
  geographyCountry?: string;
  geographyLocality?: string;
  geographyPostalcode?: string;
  geographyState?: string;
  // roles?: Role[]; //TODO
  // regions?: Region[];//TODO
  // productCategories?: ProductCategory[];//TODO
  // products?: Product[];//TODO
  jsonMap?: any;
  createdOn?: number;
  updatedOn?: number;
  // cByIdentity?: string;
  // uByIdentity?: string;
  // cnameTime: string= "";
  // unameTime: string="";
  tenantType: string = 'Normal';
  agreement: string = '';
  themeColor: string = '#2C3E50';
  menuColor: string = '#e02222';
}
