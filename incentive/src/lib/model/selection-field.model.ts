export class SelectionField {
  displayName!: string;
  isDisable: boolean = false;
  isChecked: boolean = false;
  mappingField?: string;
  type!: string;
  default?: boolean = false;
  tableMappingField?: string;
  isCustom?:boolean=false;
}
