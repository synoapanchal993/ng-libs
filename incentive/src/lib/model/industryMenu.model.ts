import { IndustryRoute } from './industryRoute.model';

export class IndustryMenu {
  parentMenuId!: number;
  activeStatus!: string;
  childMenus!: IndustryMenu[];
  defaultAllow!: boolean;
  isOpen!: boolean;
  menuRoutes!: IndustryRoute[];
  platforms!: string;
  url!: string;
  urlMatcher!: string;
  name!: string;
  allowInVirtualTenant!: boolean;
}
