import { SelectionField } from './selection-field.model';

export class ExportData {
  groupedData?: boolean = false;
  exportExcelFields: SelectionField[] = [];
  groupBy?: string;
  fileName: string = 'Data';
  entityName: string = "Entity";
}
