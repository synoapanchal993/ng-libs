import { SelectionField } from './selection-field.model';

export class ExportIncentiveInfo {}

export function getSelectionFields(): SelectionField[] {
  return [
    {
      displayName: 'Status',
      type: 'text',
      isDisable: false,
      isChecked: true,
      mappingField: 'activeStatus',
      default: true,
      tableMappingField: 'activeStatus',
      isCustom:false
    },
    {
      displayName: 'Name',
      type: 'text',
      isDisable: false,
      isChecked: true,
      mappingField: 'name',
      default: true,
      tableMappingField: 'name',
      isCustom:false
    },
    {
      displayName: 'Location',
      type: 'text',
      isDisable: false,
      isChecked: true,
      mappingField: 'locationName',
      default: true,
      tableMappingField: 'locationName',
    },
    {
      displayName: 'Department',
      type: 'text',
      isDisable: true,
      isChecked: true,
      mappingField: 'locationGroupName',
      default: true,
      tableMappingField: 'locationGroupName',
    },
    {
      displayName: 'Frequency',
      type: 'text',
      isDisable: false,
      isChecked: true,
      mappingField: 'frequency',
      default: true,
      tableMappingField: 'frequency',
      isCustom:true
    },
    {
      displayName: 'Timeframe',
      type: 'text',
      isDisable: false,
      isChecked: true,
      mappingField: 'timeFrame',
      default: true,
      tableMappingField: 'fromDate,toDate',
      isCustom:true
    },
    {
      displayName: 'Created By',
      type: 'text',
      isDisable: false,
      isChecked: true,
      mappingField: 'createdByName',
      default: false,
      tableMappingField: 'createdByName',
      isCustom:true
    },
    {
      displayName: 'Created On',
      type: 'date',
      isDisable: false,
      isChecked: true,
      mappingField: 'createdOn',
      default: false,
      tableMappingField: 'createdOn',
      isCustom:true
    },
    {
      displayName: 'Updated By',
      type: 'text',
      isDisable: false,
      isChecked: true,
      mappingField: 'updatedByName',
      default: true,
      tableMappingField: 'updatedByName',
      isCustom:true
    },
    {
      displayName: 'Updated On',
      type: 'date',
      isDisable: false,
      isChecked: true,
      mappingField: 'updatedOn',
      default: true,
      tableMappingField: 'updatedOn',
      isCustom:true
    }
    
  ];
}
