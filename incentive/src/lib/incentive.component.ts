import { Component, OnInit } from '@angular/core';
import { AppTooltipService, TooltipEntity } from '@fpg-component/app-tooltip';

@Component({
  selector: 'lib-incentive',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./incentive.component.scss'],
})
export class IncentiveComponent implements OnInit {
  constructor(private appTooltipService: AppTooltipService) {}

  ngOnInit(): void {
    this.appTooltipService.initialteTooltipMap(TooltipEntity.getTooltipEnum(7)); /* call the user management entity */
  }
}
