export enum Status {
  /** The Tenant location. */
  /** The Normal. */
  Normal = 0,

  /** The Blocked. */
  Blocked = 1,

  /** The Deleted. */
  Deleted = 2,

  /** The Deactivated. */
  Deactivated = 3,

  /** The pending. */
  Pending = 4,

  /** The Approved. */
  Approved = 5,

  /** The Archive. */
  Archive = 6,

  /** The Onboarding. */
  Ready_to_onboard = 7,

  /** The Start Onboarding. */
  Onboarding = 8,

  /** The Completed.  - This status is not used in backend, used only in frontend and pass normal status in backend when we found completed status*/
  Launched = 9,

  /** The Suspended. */
  Suspended = 10,

  /** The Ready to launch. */
  Ready_to_launch = 11,

  Completed = 13,

  In_Progress = 14,
  Draft = 12,
}

export namespace Status {
  export function getEnum(status: String): Status {
    let tmp!: Status;
    switch (status) {
      case 'Normal':
        tmp = Status.Normal;
        break;
      case 'Deactivated':
        tmp = Status.Deactivated;
        break;
      case 'Archive':
        tmp = Status.Archive;
        break;
      case 'Draft':
        tmp = Status.Draft;
        break;
      default:
        tmp = Status.Normal;
        break;
    }
    return tmp;
  }
  export function getStatusName(status: string): string {
    if (status == 'Normal') {
      return 'Active';
    } else if (status == 'Deactivated') {
      return 'Deactivated';
    } else if (status == 'Draft') {
      return 'Draft';
    }
    return status;
  }
  export function toString(status: Status): string {
    return Status[status];
  }

  export function getColor(status: string): string {
    if (status === Status[Status.Normal]) {
      return '#32cd32';
    } else if (status === Status[Status.Deactivated]) {
      return 'grey';
    } else if (status === Status[Status.Archive]) {
      return 'RED';
    } else if (status === Status[Status.Draft]) {
      return 'Orange';
    }
    return 'YELLOW';
  }
  export function getAllStatusColors() {
    let statusColors: any = {};
    for (let item in Status) {
      if (isNaN(Number(item))) {
        statusColors[item.toString()] = Status.getColor(item);
      }
    }
    return statusColors;
  }
}
