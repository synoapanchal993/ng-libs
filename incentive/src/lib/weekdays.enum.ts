export enum WeekDays {
  Sunday = 0,
  Monday = 1,
  Tuesday = 2,
  Wednesday = 3,
  Thursday = 4,
  Friday = 5,
  Saturday = 6,
}

export namespace WeekDays {
  export function toString(weekDays: WeekDays): string {
    return WeekDays[weekDays];
  }
  export function getEnum(weekDays: String): WeekDays {
    let tmp!: WeekDays;
    switch (weekDays) {
      case 'Sunday':
        tmp = WeekDays.Sunday;
        break;
      case 'Monday':
        tmp = WeekDays.Monday;
        break;
      case 'Tuesday':
        tmp = WeekDays.Tuesday;
        break;
      case 'Wednesday':
        tmp = WeekDays.Wednesday;
        break;
      case 'Thursday':
        tmp = WeekDays.Thursday;
        break;
      case 'Friday':
        tmp = WeekDays.Friday;
        break;
      case 'Saturday':
        tmp = WeekDays.Saturday;
        break;
      default:
        tmp = WeekDays.Monday;
        break;
    }
    return tmp;
  }
}
