import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncentiveSummaryComponent } from './incentive-summary.component';

describe('IncentiveSummaryComponent', () => {
  let component: IncentiveSummaryComponent;
  let fixture: ComponentFixture<IncentiveSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IncentiveSummaryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncentiveSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
