import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  AfterViewChecked,
  ChangeDetectorRef,
} from '@angular/core';
import { IncentiveService } from '../service/incentive.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { CustomModalComponent } from '@fpg-component/custom-modal';
import { IAMService } from '../service/iam.service';
import { NavigationExtras, Router } from '@angular/router';
import * as moment from 'moment';
import {
  EventTracking,
  EventsJson,
  ModuleConstant,
  SubModuleConstant,
  ActionConstant,
  ActionOnConstant,
  EntityTypeConstant,
} from 'event-framework';
@Component({
  selector: 'incentive-summary',
  templateUrl: './incentive-summary.component.html',
  styleUrls: ['./incentive-summary.component.scss'],
})
export class IncentiveSummaryComponent implements OnInit, AfterViewChecked {
  @Input() finalObject: any = {};
  @Input() showEdit: boolean = false;
  @Input() userInAnotherPlan: any[] = [];
  @Output() editClick = new EventEmitter();
  @Output() closeClick = new EventEmitter();
  @Input() cycleIntervals: any;
  @Input() hideOnEmail: boolean = false;
  @BlockUI()
  blockUI!: NgBlockUI;
  @ViewChild(CustomModalComponent)
  customModal!: CustomModalComponent;
  newMonthlyLocationTiers: any = [];
  isFirefox: boolean = false;
  canEditUserProfile: boolean = false;
  industryId: any;
  constructor(
    private incentiveService: IncentiveService,
    public iamservice: IAMService,
    private router: Router,
    private eventTracking: EventTracking
  ) {}

  ngOnInit(): void {
    this.isFirefox = navigator.userAgent.search('Firefox') > 0;
    this.industryId = this.iamservice.getIndustry();
    this.canEditUserProfile = this.iamservice.isUrlAccessible(
      '/configuration/user/edit',
      true
    );
  }
  ngAfterViewChecked(): void {
    /* Location tier code hide */
    /* if (!this.finalObject.isLocationBasedCommon && this.finalObject.monthlyLocationTiers !== undefined
      && this.finalObject.monthlyLocationTiers !== null) {
      this.generateLocationCycle();
    } */
  }
  goto(val: any) {
    this.editClick.emit(val);
  }
  unlockInterval(kid: string) {
    this.blockUI.start('Loading...');
    this.incentiveService
      .unLockIncentivePayout(kid)
      .subscribe((response: any) => {
        this.blockUI.stop();
        if (response && response.errors) {
          const error = response.errors[0].message;
          this.openModal(
            'Oops! Something went wrong. Please try again later.',
            false,
            false,
            true
          );
        } else if (response.data && response.data.unLockIncentivePayout) {
          this.cycleIntervals.intervals.find(
            (x: any) => x.kid === kid
          )!.locked = false;
          let interval = this.cycleIntervals.intervals.find(
            (x: any) => x.kid === kid
          );
          const eventData = new EventsJson();
          eventData.module = ModuleConstant.INCENTIVE_PLAN;
          eventData.subModule = SubModuleConstant.INCENTIVE_SUMMARY;
          eventData.action = ActionConstant.RECORD_UPDATE;
          eventData.actionOn = ActionOnConstant.INCENTIVE_PLAN_CYCLE_UNLOCK;
          eventData.entityType = EntityTypeConstant.INCENTIVE_PLAN;
          eventData.entityId = kid;
          (eventData.tenantLocationId = this.finalObject.incentivePlanEntitys[0].tenantLocationId),
            (eventData.varianceFromDate = moment(
              interval.fromDateStr,
              'MMM DD, YYYY'
            ).format('YYYY-MM-DD')),
            (eventData.varianceToDate = moment(
              interval.toDateStr,
              'MMM DD, YYYY'
            ).format('YYYY-MM-DD')),
            (eventData.varianceType = this.finalObject.frequency),
            (eventData.version = 2),
            this.eventTracking.createEvent(eventData);
          this.unlockSuccess();
        }
      });
  }
  unlockSuccess() {
    let modalData = {
      modalClassName: 'modal-md',
      width: '100%',
      type: 'Success',
      title: 'Success',
      text: 'Interval unlocked successfully',
      data: {},
      buttons: [
        {
          text: 'Ok',
          value: true,
          visible: true,
          className: 'btn-primary',
          closeModal: true,
          onClick: 'cancel',
        },
      ],
      closeOnClickOutside: false,
      closeOnEsc: true,
    };
    this.customModal.open(modalData);
  }

  unlockIntervalConfirmation(kid: string) {
    let modalData = {
      modalClassName: 'modal-md',
      width: '100%',
      type: 'Confirm',
      title: 'Confirm',
      text: 'Are you sure, you want to unlock the calculation?',
      data: kid,
      buttons: [
        {
          text: 'Yes',
          value: true,
          visible: true,
          className: 'btn-primary',
          closeModal: true,
          onClick: 'yesUnlock',
        },
        {
          text: 'Cancel',
          value: true,
          visible: true,
          className: 'btn-secondary',
          closeModal: true,
          onClick: 'cancel',
        },
      ],
      closeOnClickOutside: false,
      closeOnEsc: true,
    };
    this.customModal.open(modalData);
  }
  modalClickEvent(event: any) {
    let action = event.event;
    let data = event.data;
    if (action == 'yesUnlock') {
      this.customModal.close();
      // this.changeStatus(data.kid, Status.Normal);
      this.unlockInterval(data);
    } else {
      this.customModal.close();
    }
  }
  generateLocationCycle(): void {
    const monthlyLocationTier = this.finalObject.monthlyLocationTiers;
    const newMonthlyLocationTiers: any = [];
    let fromDate = '';
    let j = 0;
    monthlyLocationTier.forEach((tier: any, i: number) => {
      if (tier.fromDate !== fromDate) {
        newMonthlyLocationTiers.push({
          month: tier.month,
          year: tier.year,
          fromDate: tier.fromDate,
          toDate: tier.toDate,
          day: tier.day,
          tierThresholds: [],
          frequency: tier.frequency,
        });
        newMonthlyLocationTiers[j].tierThresholds.push({
          tierThreshold: tier.tierThreshold,
          isLocationTierEnabled: tier.isLocationTierEnabled,
        });
        j = j + 1;
      } else {
        let k = j - 1;
        newMonthlyLocationTiers[k].tierThresholds.push({
          tierThreshold: tier.tierThreshold,
          isLocationTierEnabled: tier.isLocationTierEnabled,
        });
      }
      fromDate = tier.fromDate;
    });
    this.newMonthlyLocationTiers = newMonthlyLocationTiers;
  }

  openUserProfile(userId: number) {
    if (this.canEditUserProfile) {
      let navigationExtras: NavigationExtras = {
        queryParams: { tab: 'information' },
      };
      const eventData = new EventsJson();
      eventData.module = ModuleConstant.INCENTIVE_PLAN;
      eventData.subModule = SubModuleConstant.INCENTIVE_SUMMARY;
      eventData.action = ActionConstant.RECORD_VIEW;
      eventData.actionOn = ActionOnConstant.USER_PROFILE;
      eventData.entityType = EntityTypeConstant.USER;
      eventData.entityId = userId.toString();
      this.eventTracking.createEvent(eventData);
      this.closeClick.next();
      this.router.navigate([]).then((result) => {
        window.open('user/profile/' + userId + '?tab=information', '_blank');
      });
      //this.router.navigate(['user/profile/' + userId], navigationExtras);
    }
  }
  openModal(
    message: string,
    isSuccess: boolean,
    displayOkButton: boolean,
    showCloseButton: boolean
  ) {
    let modalData = {
      modalClassName: 'modal-md',
      width: '100%',
      type: isSuccess ? 'Success' : 'Error',
      title: isSuccess ? 'Success' : 'Error',
      html: {
        text: ' ' + message,
      },
      showCloseButton: showCloseButton,
      buttons: [
        displayOkButton
          ? {
              text: 'OK',
              value: true,
              visible: true,
              className: 'btn-primary',
              closeModal: true,
            }
          : {},
      ],
      closeOnClickOutside: false,
      closeOnEsc: true,
    };
    this.customModal.open(modalData);
  }
}
