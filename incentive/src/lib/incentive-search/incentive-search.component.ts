import {
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
  OnDestroy,
  ChangeDetectorRef,
} from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Filter, FilterService } from '@fpg-component/filter';
import {
  CustomPaginationService,
  PageConfig,
  TableHeader,
  TableService,
} from '@fpg-component/table';
import * as moment from 'moment';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { getIntervalWithLockedInfo } from '../incentive-add/incentive-step-info';
import { IncentiveSearchForm } from '../model/incentive-search-form.model';
import { IncentiveService } from '../service/incentive.service';
import { Status } from '../status.enum';
import { IncentiveFrequency } from '../incentive-frequency.enum';
import { WeekDays } from '../weekdays.enum';
import { CustomModalComponent } from '@fpg-component/custom-modal';
import { IAMService } from '../service/iam.service';
import { CurrencyPipe, getCurrencySymbol, DecimalPipe } from '@angular/common';
import { CurrencySymbolPipe } from '../currencySymbolPipe.pipe';
import {
  AppTooltipDirective,
  AppTooltipService,
  TooltipEntity,
} from '@fpg-component/app-tooltip';
import { ExportData } from '../model/export-data';
import { SelectionField } from '../model/selection-field.model';
import { getSelectionFields } from '../model/export-incentive-info';
import { UtilService } from '../service/util.service';
import * as FileSaver from 'file-saver';
import { includes } from 'lodash';
import {
  EventTracking,
  EventsJson,
  ModuleConstant,
  SubModuleConstant,
  ActionConstant,
  ActionOnConstant,
  EntityTypeConstant,
} from 'event-framework';
import * as htmlToImage from 'html-to-image';
import { UploadService } from 'shared-utility';
declare var saveAs: any;

@Component({
  selector: 'incentive-search',
  templateUrl: './incentive-search.component.html',
  styleUrls: ['./incentive-search.component.scss'],
})
export class IncentiveSearchComponent implements OnInit, OnDestroy {
  tenantId!: number;
  locations: any;
  stepForm: any;
  hideOnEmail: boolean = false;
  firstDayOfMonth = moment([moment().year(), moment().month()]);
  initialDates: moment.Moment[] = [
    this.firstDayOfMonth,
    moment(this.firstDayOfMonth).endOf('month'),
  ];
  // table
  _tableHeaders!: TableHeader[];
  _tableData: any[] = [];
  userInAnotherPlan: any[] = [];
  dataLoaded: boolean = false;
  tableId = 'incentive_table';
  pageChangedSubscription?: Subscription;
  incentiveSearchForm: IncentiveSearchForm = new IncentiveSearchForm();
  pageConfig: PageConfig = new PageConfig();
  filterInput: Filter[] = [];
  storedFilterId: string = 'incentive-search';
  finalObject: any = {};
  cycleIntervals: any = {};
  // table

  modalRef!: BsModalRef;
  @ViewChild('template', { static: true })
  template!: TemplateRef<any>;
  @BlockUI()
  blockUI!: NgBlockUI;
  @ViewChild(CustomModalComponent)
  customModal!: CustomModalComponent;
  canEditIncentive: boolean = false;
  canEditUserProfile: boolean = false;
  incentiveEntity: string = '';
  exportIncentivesactions: any[] = [];
  selectionFields: SelectionField[] = [];
  requestBody: any;
  eventData: EventsJson = new EventsJson();
  mailAttachmentPath = '';
  baseUrl = '';
  industryId = 1;
  emailSubject = "";
  constructor(
    public iamservice: IAMService,
    private incentiveService: IncentiveService,
    private incentivePaginationService: CustomPaginationService,
    private filterService: FilterService,
    private router: Router,
    private modalService: BsModalService,
    private currencySymbolPipe: CurrencySymbolPipe,
    private decimalPipe: DecimalPipe,
    private appTooltipService: AppTooltipService,
    private tableService: TableService,
    private utilService: UtilService,
    private eventTracking: EventTracking,
    private uploadService: UploadService,
    private ref: ChangeDetectorRef
  ) {
    this.tenantId = iamservice.getTenant()?.intId;
    this.manageUserAccess();
    this.exportIncentivesactions = [
      {
        name: 'Current Search',
        clickEvent: 'currentSearch',
      },
      {
        name: 'Advanced',
        clickEvent: 'advanced',
      },
    ];
    this.baseUrl = this.incentiveService.getMicroServicesBase()
      ? this.incentiveService
          .getMicroServicesBase()
          .replace('https://', 'http://')
      : this.incentiveService.getMicroServicesBase();
    this.industryId = this.iamservice.getIndustry();
  }

  ngOnInit(): void {
    this.incentiveEntity = TooltipEntity.getTooltipEnum(7);
    this.eventData.module = ModuleConstant.INCENTIVE_PLAN;
    this.eventData.subModule = SubModuleConstant.INCENTIVE_SEARCH;
    this.eventData.actionOn = ActionOnConstant.INCENTIVE_PLAN_LIST;
    this.eventData.entityType = EntityTypeConstant.INCENTIVE_PLAN;
    this.manageFilters();
    /** Intiailize table headers   */
    this._tableHeaders = [
      /* 
      {
        name: '',
        mappingField: 'activeStatus',
        columnType: 'statusLine',
        bodyclassName: '',
        headerClassName: '',
        width: '1%',
        showTooltip: false,
        hide: false,
        enumColors: Status.getAllStatusColors(),
      }, */
      {
        id: 'name',
        name: 'Name',
        mappingField: 'name',
        columnType: 'text',
        bodyclassName: '',
        headerClassName: '',
        width: '15%',
        enableSort: true,
        hide: false,
      },
      {
        id: 'locationName',
        name: 'Location',
        mappingField: 'locationName',
        columnType: 'text',
        bodyclassName: '',
        headerClassName: '',
        width: '16%',
        showTooltip: false,
        enableSort: false,
        hide: false,
      },
      {
        id: 'locationGroupName',
        name: 'Department',
        mappingField: 'locationGroupName',
        columnType: 'text',
        bodyclassName: '',
        headerClassName: '',
        width: '16%',
        showTooltip: false,
        enableSort: false,
        hide: false,
      },
      {
        id: 'frequency',
        name: 'Frequency',
        columnType: 'text',
        mappingField: 'frequency',
        bodyclassName: '',
        headerClassName: '',
        width: '10%',
        showTooltip: false,
        hide: false,
      },
      {
        id: 'timeFrame',
        name: 'Timeframe',
        columnType: 'dateRange',
        mappingField: 'fromDate,toDate',
        bodyclassName: '',
        headerClassName: '',
        width: '20%',
        showTooltip: false,
        enableSort: true,
        hasCommaSeparatedValue: true,
        hide: false,
        sortBy: 'fromDate',
      },
      {
        id: 'activeStatus',
        name: 'Status',
        mappingField: 'activeStatus',
        columnType: 'text',
        width: '10%',
        showTooltip: false,
        hide: true,
        enumColors: Status.getAllStatusColors(),
      },
      {
        id: 'createdByName',
        name: 'Created By',
        mappingField: 'createdByName',
        columnType: 'hyperLink',
        bodyclassName: '',
        headerClassName: '',
        width: '16%',
        showTooltip: false,
        enableSort: false,
        hide: true,
      },
      {
        id: 'createdOnStr',
        name: 'Created On',
        mappingField: 'createdOnStr',
        columnType: 'text',
        bodyclassName: '',
        headerClassName: '',
        width: '16%',
        showTooltip: false,
        enableSort: true,
        hide: true,
        sortBy: 'createdOn',
      },
      {
        id: 'updatedByName',
        name: 'Updated By',
        mappingField: 'updatedByName',
        columnType: 'hyperLink',
        bodyclassName: '',
        headerClassName: '',
        width: '16%',
        showTooltip: false,
        enableSort: false,
        hide: false,
      },
      {
        id: 'updatedOnStr',
        name: 'Updated On',
        mappingField: 'updatedOnStr',
        columnType: 'text',
        bodyclassName: '',
        headerClassName: '',
        width: '16%',
        showTooltip: false,
        enableSort: true,
        hide: false,
        sortBy: 'updatedOn',
      },
      {
        id: 'action',
        name: 'Action',
        mappingField: 'action',
        columnType: 'action',
        bodyclassName: '',
        headerClassName: '',
        width: '5%',
        showTooltip: false,
        hide: false,
      },
    ];
    // this.storedFilter = this.globalService.getSearchForm(this.userService.getEntity());

    /** Whenever page is changed  */
    this.pageChangedSubscription = this.incentivePaginationService
      .getPageChangedEvt()
      .subscribe((message: any) => {
        this.incentiveSearchForm.page = message.pageConfig.page - 1;
        this.pageConfig = message.pageConfig;
        this.pageConfig.size = 10;
        this.getDate();
      });
    setTimeout(() => {
      let tenantLocationId = this.incentiveSearchForm.customFilters?.find(
        (e: any) => e.id === 'location'
      )?.selectedValue;
      let dateFilter = this.incentiveSearchForm.customFilters?.find(
        (e: any) => e.id === 'incentivePlanDate'
      );
      let fromDate = dateFilter.fromDate;
      let toDate = dateFilter.toDate;
      let type = dateFilter.selectedFilter;
      const eventData = new EventsJson();
      eventData.module = ModuleConstant.INCENTIVE_PLAN;
      eventData.subModule = SubModuleConstant.INCENTIVE_SEARCH;
      eventData.action = ActionConstant.FETCH_DATA;
      eventData.actionOn = ActionOnConstant.INCENTIVE_PLAN_LIST;
      eventData.entityType = EntityTypeConstant.INCENTIVE_PLAN;
      (eventData.tenantLocationId = tenantLocationId),
        (eventData.varianceFromDate = fromDate),
        (eventData.varianceToDate = toDate),
        (eventData.varianceType = type),
        (eventData.version = 2),
        this.eventTracking.createEvent(eventData);
    }, 700);
    this.canEditUserProfile = this.iamservice.isUrlAccessible(
      '/configuration/user/edit',
      true
    );
  }
  ngOnDestroy() {
    this.pageChangedSubscription?.unsubscribe();
  }
  private manageFilters() {
    this.filterInput.push(
      /* {
        fieldDetails: {
          id: "name",
          name: "Name",
          type: 'text',
          searchField: "name",
          selectedFilter: 'contains',
          selectedValue: null
        },
        params: {
          filterOptions: ['contains', 'isEqual']
        }
      }, */
      {
        fieldDetails: {
          id: 'location',
          searchField: 'tenantLocationId',
          name: 'Location',
          type: 'entity',
          entityType: 'TenantLocation',
          parent: null,
        },
        params: {
          isChecked: true,
        },
      },
      {
        fieldDetails: {
          id: 'incentivePlanDate',
          searchFields: ['fromDate', 'toDate'],
          name: 'Timeframe',
          multipleSearchField: true,
          type: 'date',
        },
        params: {
          isChecked: true,
          mandatory: true,
        },
      },
      {
        fieldDetails: {
          id: 'status',
          name: 'Status',
          type: 'enum',
          enumType: 'incentiveStatus',
          searchField: 'activeStatus',
        },
      }
    );
    //Need to check of stored filter entity types
    this.filterService.processFilterInput(
      this.filterInput,
      this.storedFilterId
    );
  }
  resetSearch() {
    this._tableData = [];
    this.pageConfig.page = 1;
    this.pageConfig.totalElements = 0;
    this.pageConfig.totalPages = 0;
    this.incentivePaginationService.sendPageChangedEvt(this.pageConfig);
  }
  onHyperLinkClick(event: any) {
    const eventData = new EventsJson();
    eventData.module = ModuleConstant.ADMIN_PANEL;
    eventData.subModule = SubModuleConstant.INCENTIVE_SEARCH;
    eventData.action = ActionConstant.RECORD_VIEW;
    eventData.actionOn = ActionOnConstant.USER_PROFILE;
    eventData.entityType = EntityTypeConstant.USER;
    if (event.mappingField == 'updatedByName' && this.canEditUserProfile) {
      let navigationExtras: NavigationExtras = {
        queryParams: { tab: 'information' },
      };
      eventData.entityId = event.value.updatedByIntId;
      this.eventTracking.createEvent(eventData);
      //this.router.navigate(['user/profile/' + event.value.updatedByIntId],navigationExtras);
      this.router.navigate([]).then((result) => {
        window.open(
          'user/profile/' + event.value.updatedByIntId + '?tab=information',
          '_blank'
        );
      });
    } else if (
      event.mappingField == 'createdByName' &&
      this.canEditUserProfile
    ) {
      let navigationExtras: NavigationExtras = {
        queryParams: { tab: 'information' },
      };
      eventData.entityId = event.value.createdByIntId;
      this.eventTracking.createEvent(eventData);
      //this.router.navigate(['user/profile/' + event.value.createdByIntId],navigationExtras);
      this.router.navigate([]).then((result) => {
        window.open(
          'user/profile/' + event.value.createdByIntId + '?tab=information',
          '_blank'
        );
      });
    }
  }
  contextMenuActionEmit(e: any) {
    if (e.clickEvent == 'editIncentive') {
      // this.userData = event.rowData;
      this.router.navigate(['incentive', e.rowData.kid]);
    } else if (e.clickEvent == 'incentiveSummary') {
      this.fetchIncentiveDetails(e.rowData.kid);
    } else if (
      e.clickEvent == 'deactiveStatusChange' ||
      e.clickEvent == 'activeStatusChange' ||
      e.clickEvent == 'archiveStatusChange'
    ) {
      this.updateStatusConfirm(e.clickEvent, e.rowData);
    }
  }
  sortEvent(e: any) {
    if (e.sortBy == 'createdOnStr') {
      e.sortBy = 'createdOn';
    }
    if (e.sortBy == 'updatedOnStr') {
      e.sortBy = 'updatedOn';
    }
    this.incentiveSearchForm.orderBy = e.sortBy;
    this.incentiveSearchForm.sortBy = e.sortOrder;
    this.resetSearch();
  }

  applySearchFilter(filterSearch: any) {
    console.log('Applying search filter ', filterSearch);
    this.incentiveSearchForm.customFilters = filterSearch;
    localStorage.setItem(this.storedFilterId, JSON.stringify(filterSearch));
    this.resetSearch();
  }
  updateContextMenuActions(status: string) {
    let actions = [];
    if (this.canEditIncentive && Status.getEnum(status) != Status.Archive) {
      actions.push({
        name: 'Edit',
        clickEvent: 'editIncentive',
      });
    }
    if (
      Status.getEnum(status) != Status.Archive &&
      Status.getEnum(status) != Status.Draft
    ) {
      actions.push({
        name: 'Summary',
        clickEvent: 'incentiveSummary',
      });
    }
    if (Status.getEnum(status) == Status.Normal) {
      actions.push({
        name: 'Deactivate',
        clickEvent: 'deactiveStatusChange',
      });
    } else if (Status.getEnum(status) == Status.Deactivated) {
      actions.push({
        name: 'Activate',
        clickEvent: 'activeStatusChange',
      });
    } else if (Status.getEnum(status) == Status.Draft) {
      actions.push({
        name: 'Delete',
        clickEvent: 'archiveStatusChange',
      });
    }
    return actions;
  }
  addIncentive() {
    this.router.navigate(['incentive', 'add']);
  }
  getDate() {
    /* this.incentiveSearchForm.userIds = null;
    this.incentiveSearchForm.tenantLocationId = this.selectedLocationId;
    this.incentiveSearchForm.fromDate = this.initialDates[0].format('YYYY-MM-DD');
    this.incentiveSearchForm.toDate = this.initialDates[1].format('YYYY-MM-DD'); */
    this.dataLoaded = false;

    let incentiveRequest = `totalPages,totalElements,size,numberOfElements content {kid,createdOnStr,updatedOnStr,createdOn,updatedOn,activeStatus,name,locationGroupName,locationName,fromDate,toDate,frequency,noOfTiers,createdByIntId,createdByName,updatedByName,updatedByIntId}`;

    this.blockUI.start('Loading...');
    this.incentiveService
      .findByEntityIds(
        this.pageConfig.page - 1,
        this.pageConfig.size,
        this.incentiveSearchForm,
        incentiveRequest
      )
      .subscribe((response: any) => {
        this.blockUI.stop();
        this.dataLoaded = true;
        if (response && response.errors) {
          const error = response.errors[0].message;
          this.openModal(
            'Oops! Something went wrong. Please try again later.',
            false,
            false,
            true
          );
          console.log(error);
        } else if (
          response.data &&
          response.data.findByEntityIds &&
          response.data.findByEntityIds.content
        ) {
          this._tableData = response.data.findByEntityIds.content;
          this._tableData.forEach((x) => {
            x['action'] = this.updateContextMenuActions(x['activeStatus']);
            x['frequency'] =
              x['frequency'] == 'Semi_Monthly'
                ? 'Semi-monthly'
                : x['frequency'];
            x['activeStatus'] = Status.getStatusName(x['activeStatus']);
          });
          this.pageConfig.totalElements =
            response.data.findByEntityIds.totalElements;
          this.pageConfig.totalPages = response.data.findByEntityIds.totalPages;
        }
      });
  }

  openModalWithClass(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign(
        {},
        {
          class: 'gray modal-lg',
          ignoreBackdropClick: true,
          backdrop: true,
          keyboard: false,
        }
      )
    );
  }

  fetchIncentiveDetails(kid: string) {
    this.blockUI.start('Loading...');
    this.incentiveService.findById(kid).subscribe((response: any) => {
      this.blockUI.stop();
      if (response && response.errors) {
        const error = response.errors[0].message;
        this.openModal(
          'Oops! Something went wrong. Please try again later.',
          false,
          false,
          true
        );
        console.log(error);
      } else if (response.data && response.data.findById) {
        const eventData = new EventsJson();
        eventData.module = ModuleConstant.INCENTIVE_PLAN;
        eventData.subModule = SubModuleConstant.INCENTIVE_SEARCH;
        eventData.action = ActionConstant.RECORD_VIEW;
        eventData.actionOn = ActionOnConstant.INCENTIVE_PLAN_LIST;
        eventData.entityType = EntityTypeConstant.INCENTIVE_PLAN;
        eventData.entityId = kid;
        this.eventTracking.createEvent(eventData);
        this.openModalWithClass(this.template);
        this.finalObject = response.data.findById;
        this.emailSubject = this.finalObject.name;
        let currencyCode: any = this.currencySymbolPipe.transform(
          this.finalObject.locationCurrency
        );

        this.finalObject.currencySymbol = currencyCode;
        if (
          this.finalObject.majorKpiDetail.kpiPrepand !== undefined &&
          this.finalObject.majorKpiDetail.kpiPrepand !== ''
        ) {
          this.finalObject.majorKpiDetail.kpiPrepand = currencyCode;
        }
        if (
          this.finalObject.majorKpiKickerDetail.kpiPrepand !== undefined &&
          this.finalObject.majorKpiKickerDetail.kpiPrepand !== ''
        ) {
          this.finalObject.majorKpiKickerDetail.kpiPrepand = currencyCode;
        }
        const userIds: number[] = [];
        this.finalObject.selectedUsers = [];
        this.finalObject.userNames = '';
        let SEPRATOR = '';
        this.finalObject.incentivePlanEntitys?.forEach((val: any) => {
          userIds.push(val.userId);
          this.finalObject.userNames =
            this.finalObject.userNames + SEPRATOR + val.user.name;
          this.finalObject.selectedUsers.push({
            userId: val.userId,
            name: val.user.name,
          });
          SEPRATOR = ', ';
        });
        this.findSimilarIncentive(
          this.finalObject.kid,
          this.finalObject.fromDate,
          this.finalObject.toDate,
          userIds
        );
        this.cycleIntervals = getIntervalWithLockedInfo(
          moment(this.finalObject.fromDate, 'YYYY-MM-DD').toDate(),
          moment(this.finalObject.toDate, 'YYYY-MM-DD').toDate(),
          IncentiveFrequency.getEnum(this.finalObject.frequency),
          this.finalObject.incentivePlanPayouts,
          this.finalObject.payoutDateForIncentive,
          WeekDays.getEnum(this.finalObject.firstDayOfCycle),
          WeekDays.getEnum(this.finalObject.lastDayOfCycle),
          true
        );

        this.finalObject.productNames = '';
        SEPRATOR = '';
        let productThresholds = this.finalObject?.incentivePlanProductConfigs?.find(
          (x: any) => x.threshold > 0
        );
        this.finalObject.incentivePlanProductConfigs?.forEach(
          (product: any) => {
            let pthreshold = '';
            if (productThresholds) {
              //const isPer = product.isPercentage === '1';
              const thres = parseFloat(product.threshold);
              pthreshold =
                ' (Cap : ' +
                (!product.isPercentage ? currencyCode : '') +
                this.decimalPipe.transform(
                  parseFloat(product.threshold),
                  '1.2-2'
                ) +
                (product.isPercentage ? '%' : '') +
                ')';
            }
            this.finalObject.productNames =
              this.finalObject.productNames +
              SEPRATOR +
              product.locationGroupProduct.name +
              pthreshold;
            SEPRATOR = ', ';
          }
        );
      }
    });
  }
  findSimilarIncentive(
    kid: string,
    startDate: Date,
    endDate: Date,
    userIds: number[]
  ) {
    /* const selectedUsers = this.stepForm[2].get('users')?.value.filter((x: any) => x.userSelected === true);
    const userIds = selectedUsers?.map((x: any) => {
      return parseInt(x['userDetail']?.intId);
    }); */
    this.blockUI.start('Loading...');
    let incentiveSearchForm: IncentiveSearchForm = {
      customFilters: null,
      tenantLocationId: null,
      userIds: userIds,
      fromDate: moment(startDate).format('YYYY-MM-DD'),
      toDate: moment(endDate).format('YYYY-MM-DD'),
      activeStatus: null,
    };
    let incentiveRequest = `totalPages,totalElements,size,numberOfElements content {kid,activeStatus,name,locationGroupName,locationName,incentivePlanEntitys {user{intId,name}}}`;
    this.incentiveService
      .findByEntityIds(0, 1000, incentiveSearchForm, incentiveRequest)
      .subscribe((response: any) => {
        this.blockUI.stop();
        if (response && response.errors) {
          const error = response.errors[0].message;
          this.openModal(
            'Oops! Something went wrong. Please try again later.',
            false,
            false,
            true
          );
          console.log(error);
        } else if (
          response.data &&
          response.data.findByEntityIds &&
          response.data.findByEntityIds.content
        ) {
          this.userInAnotherPlan = [];
          response.data.findByEntityIds.content.forEach((plan: any) => {
            if (!kid || (kid && kid != plan.kid)) {
              let SEPRATOR = '';
              let users: any[] = [];
              plan.incentivePlanEntitys.forEach((entity: any) => {
                if (userIds.indexOf(entity.user.intId) > -1) {
                  users.push(entity.user);
                  SEPRATOR = ', ';
                }
              });
              this.userInAnotherPlan.push({
                kid: plan.kid,
                locationName: plan.locationName,
                locationGroupName: plan.locationGroupName,
                name: plan.name,
                activeStatus: plan.activeStatus,
                users: users,
              });
            }
          });
        }
      });
  }
  updateStatusConfirm(event: any, incentiveRowData: any) {
    let modalData = {
      modalClassName: 'modal-md',
      width: '100%',
      type: 'Confirm',
      title: 'Confirm',
      text: ' Are you sure you want to deactivate this Incentive Plan? ',
      data: incentiveRowData,
      buttons: [
        {
          text: 'Yes',
          value: true,
          visible: true,
          className: 'btn-primary',
          closeModal: true,
          onClick: 'deactivateConfirm',
        },
        {
          text: 'No',
          value: true,
          visible: true,
          className: 'btn-secondary',
          closeModal: true,
          onClick: 'cancel',
        },
      ],
      closeOnClickOutside: false,
      closeOnEsc: true,
    };
    if (event == 'deactiveStatusChange') {
      this.customModal.open(modalData);
    } else if (event == 'activeStatusChange') {
      modalData.text = 'Are you sure you want to activate this Incentive Plan?';
      modalData.buttons = [
        {
          text: 'Yes',
          value: true,
          visible: true,
          className: 'btn-primary',
          closeModal: true,
          onClick: 'activateConfirm',
        },
        {
          text: 'No',
          value: true,
          visible: true,
          className: 'btn-secondary',
          closeModal: true,
          onClick: 'cancel',
        },
      ];
      this.customModal.open(modalData);
    } else if (event == 'archiveStatusChange') {
      modalData.text =
        'Are you sure you want to delete this drafted Incentive Plan?';
      modalData.buttons = [
        {
          text: 'Yes',
          value: true,
          visible: true,
          className: 'btn-primary',
          closeModal: true,
          onClick: 'archiveConfirm',
        },
        {
          text: 'No',
          value: true,
          visible: true,
          className: 'btn-secondary',
          closeModal: true,
          onClick: 'cancel',
        },
      ];
      this.customModal.open(modalData);
    }
  }
  changeStatus(kid: string, status: Status) {
    this.blockUI.start('Loading...');
    this.incentiveService
      .updateUserStatus(kid, Status[status])
      .subscribe((response: any) => {
        this.blockUI.stop();
        if (response) {
          let modalData = {
            modalClassName: 'modal-md',
            width: '100%',
            type: 'Success',
            title: 'Success',
            text:
              'Incentive Plan ' +
              (status == Status.Normal
                ? 'activated'
                : status == Status.Deactivated
                ? 'deactivated'
                : 'deleted') +
              ' successfully!',
            data: {},
            buttons: [
              {
                text: 'Ok',
                value: true,
                visible: true,
                className: 'btn-primary',
                closeModal: true,
                onClick: 'resetSearch',
              },
            ],
            closeOnClickOutside: false,
            closeOnEsc: true,
          };
          if (response.data.changeStatus) {
            let action =
              status == Status.Normal
                ? ActionConstant.RECORD_ACTIVATE
                : status == Status.Deactivated
                ? ActionConstant.RECORD_DEACTIVATE
                : ActionConstant.RECORD_DELETE;
            const eventData = new EventsJson();
            eventData.module = ModuleConstant.INCENTIVE_PLAN;
            eventData.subModule = SubModuleConstant.INCENTIVE_SEARCH;
            eventData.action = action;
            eventData.actionOn = ActionOnConstant.INCENTIVE_PLAN_LIST;
            eventData.entityType = EntityTypeConstant.INCENTIVE_PLAN;
            eventData.entityId = kid;
            this.eventTracking.createEvent(eventData);
            this.customModal.open(modalData);
          } else {
            /* modalData.type = 'Error';
            modalData.title = 'Error';
            modalData.text =
              'Oops! Something went wrong. Please try again later.';
            this.customModal.open(modalData); */
            this.openModal(
              'Oops! Something went wrong. Please try again later.',
              false,
              false,
              true
            );
          }
        }
      });
  }
  modalClickEvent(event: any) {
    let action = event.event;
    let data = event.data;
    if (action == 'activateConfirm') {
      this.customModal.close();
      this.changeStatus(data.kid, Status.Normal);
    } else if (action == 'deactivateConfirm') {
      this.customModal.close();
      this.changeStatus(data.kid, Status.Deactivated);
    } else if (action == 'archiveConfirm') {
      this.customModal.close();
      this.changeStatus(data.kid, Status.Archive);
    } else if (action == 'resetSearch') {
      this.customModal.close();
      this.resetSearch();
    } else {
      this.customModal.close();
    }
  }
  manageUserAccess() {
    this.canEditIncentive = this.iamservice.isUrlAccessible(
      '/lightTable/incentivePlan/edit',
      true
    );
  }

  closeSummary() {
    this.modalRef.hide();
  }
  openModal(
    message: string,
    isSuccess: boolean,
    displayOkButton: boolean,
    showCloseButton: boolean
  ) {
    let modalData = {
      modalClassName: 'modal-md',
      width: '100%',
      type: isSuccess ? 'Success' : 'Error',
      title: isSuccess ? 'Success' : 'Error',
      html: {
        text: ' ' + message,
      },
      showCloseButton: showCloseButton,
      buttons: [
        displayOkButton
          ? {
              text: 'OK',
              value: true,
              visible: true,
              className: 'btn-primary',
              closeModal: true,
            }
          : {},
      ],
      closeOnClickOutside: false,
      closeOnEsc: true,
    };
    this.customModal.open(modalData);
  }

  //Export related
  exportIncentiveContextMenuActionClick(event: any) {
    if (event == 'currentSearch') {
      const eventData = new EventsJson();
      eventData.module = ModuleConstant.INCENTIVE_PLAN;
      eventData.subModule = SubModuleConstant.INCENTIVE_SEARCH;
      eventData.action = ActionConstant.EXPORT_INCENTIVE;
      eventData.actionOn = ActionOnConstant.INCENTIVE_PLAN_LIST;
      eventData.entityType = EntityTypeConstant.INCENTIVE_PLAN;
      this.eventTracking.createEvent(eventData);
      let incentiveRequest = `totalPages,totalElements,size,numberOfElements content {kid,timeFrame,createdOn,updatedOn,activeStatus,name,locationGroupName,locationName,fromDate,toDate,frequency,noOfTiers,createdByIntId,createdByName,updatedByName,updatedByIntId}`;
      this.requestBody = this.incentiveService.getQueryForExport(
        0,
        this.pageConfig.totalElements + 1,
        this.incentiveSearchForm,
        incentiveRequest
      );
      this.downloadFile();
    }
  }

  downloadFile() {
    if (this.pageConfig.totalElements == 0) {
      let modalData = {
        modalClassName: 'modal-md',
        width: '100%',
        type: 'Confirm',
        title: 'Info',
        text: 'No records available for exporting!',
        data: {},
        buttons: [],
        showCloseButton: true,
        closeOnClickOutside: false,
        closeOnEsc: true,
      };
      this.customModal.open(modalData);
    } else {
      this.manageSelectionFields();
      if (this.incentiveSearchForm.exportData) {
        this.incentiveSearchForm.exportData.fileName =
          'Incentive_Data' + '_' + moment().format('MM-DD-YYYY_HH-mm-ss');
        this.incentiveSearchForm.exportData.entityName = 'IncentivePlan';
      }
      // if(this.pageConfig.totalElements > 200){
      //   this.openExportModal();
      //   this.startExport();
      // }else{
      if (this.utilService.isEnableWaitIndicator) {
        this.blockUI.start('Loading...');
      }
      this.startExport();
      // }
    }
  }

  manageSelectionFields() {
    this.selectionFields = getSelectionFields();
    let exportExcelFields: SelectionField[] = [];
    // check displayed columns in table and as per t
    let displayedColumns: string[] = this.tableService.getDisplayedColumns();
    console.log(displayedColumns);
    this.selectionFields.forEach((field) => {
      if (field.default) {
        exportExcelFields.push(field);
      } else if (includes(displayedColumns, field.tableMappingField)) {
        exportExcelFields.push(field);
      }
    });
    let exportData = new ExportData();
    exportData.exportExcelFields = exportExcelFields;
    this.incentiveSearchForm.currentUser = this.iamservice.getUser()?.intId;
    this.incentiveSearchForm.tenantId = this.iamservice.getTenant()?.intId;
    this.incentiveSearchForm.tenantType = this.iamservice.getTenantType();
    this.incentiveSearchForm.exportData = exportData;
    this.incentiveSearchForm.totalPages = this.pageConfig.totalPages;
  }

  startExport() {
    this.incentiveService
      .exportIncentive(this.incentiveSearchForm, this.requestBody)
      .subscribe((response: any) => {
        if (this.utilService.isEnableWaitIndicator) {
          this.blockUI.stop();
        }
        let fileName = this.utilService.getFileNameFromHttpResponse(response);
        let blob = response.body;
        if (blob.size > 0) {
          FileSaver.saveAs(blob, fileName);
        }
      });
  }

  openExportModal() {
    let modalData = {
      modalClassName: 'modal-md',
      width: '100%',
      type: 'Success',
      title: 'Success',
      text:
        'There are large numbers of records to be exported. You will receive an email with the download link soon!',
      data: {},
      buttons: [
        {
          text: 'OK',
          value: true,
          visible: true,
          className: 'btn-primary',
          closeModal: true,
          onClick: 'export',
        },
      ],
      closeOnClickOutside: false,
      closeOnEsc: true,
    };
    this.customModal.open(modalData);
  }
  shareSummaryViaEmail() {
    this.mailAttachmentPath = '';
    this.blockUI.start('Loading...');
    this.hideOnEmail = true;
    this.ref.detectChanges();
    setTimeout(() => {
      htmlToImage
        .toPng(document.getElementById('summaryPage') as HTMLElement)
        .then((dataUrl) => {
          this.uploadImage(dataUrl);
        })
        .catch(function (error) {
          console.error('oops, something went wrong!', error);
        });
    }, 300);
  }
  uploadImage(data: string) {
    this.uploadService
      .uploadImage(data, this.finalObject.kid)
      .subscribe((response: any) => {
        if (response && this.utilService.notBlank(response.data)) {
          //$('#textboxx_' + this.node).val($('#textboxx_' + this.node).val().replace(/src="(.*?)"/g, this.mailAttachmentPath));
          let textbox1: string = (<HTMLInputElement>(
            document.getElementById('textboxx_summaryPage')
          )).value;
          textbox1 = textbox1.replace(
            /src="(.*?)"/g,
            'src="' +
              this.baseUrl +
              '/configuration/uploadData/getImage/' +
              this.industryId +
              '?path=' +
              response.data +
              '"'
          );
          (<HTMLInputElement>(
            document.getElementById('textboxx_summaryPage')
          )).value = textbox1;
          let textbox: any = document.getElementById('textboxx_summaryPage');
          let data = new Blob([textbox['value']], { type: 'text/plain' });
          saveAs(data, 'Outlook.eml');
          setTimeout(() => {
            this.hideOnEmail = false;
          }, 500);
          this.blockUI.stop();
        }
      });
  }
}
