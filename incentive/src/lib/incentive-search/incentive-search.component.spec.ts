import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncentiveSearchComponent } from './incentive-search.component';

describe('IncentiveSearchComponent', () => {
  let component: IncentiveSearchComponent;
  let fixture: ComponentFixture<IncentiveSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IncentiveSearchComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncentiveSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
