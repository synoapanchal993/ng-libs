import { Pipe, PipeTransform } from '@angular/core';
import { getCurrencySymbol } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
@Pipe({
  name: 'currencySymbol'
})
export class CurrencySymbolPipe extends CurrencyPipe {
  transform(
    code: string,
    format: 'wide' | 'narrow' = 'narrow',
    locale?: string
  ): any {
      // console.log("Code ==>"+ code);
      // console.log(getCurrencySymbol(code, format, locale));
      let temp:any = super.transform(0, code, 'symbol', '1.0-0');
      temp = temp.substring(0, temp.length - 1);
      return temp;
    // return getCurrencySymbol(code, format, locale);
  }
}