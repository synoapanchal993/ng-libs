import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Tenant } from '../model/tenant.model';
import { User } from '../model/user.model';
import { Role } from '../model/role.model';
import { LocalStorageService } from './local-storage.service';
import { IndustryMenu } from '../model/industryMenu.model';
import { Permission } from '../model/permission.model';
import { IndustryRoute } from '../model/industryRoute.model';

@Injectable()
export class IAMService {
  token?: string;
  industry!: number;
  tenantType: any;
  tenant!: Tenant;
  user?: User;
  role?: Role; //TODO
  isSuperAdmin: boolean = false;
  accessibleRouteUrls: string[] = [];
  accessibleMenus?: IndustryMenu[] = [];
  accessibleMenuUrls: string[] = [];
  accessibleRoutes?: IndustryRoute[] = [];
  rolePermission?: Permission[] = [];
  accessibleLocations: any[] = [];
  constructor(
    public router: Router,
    public localStorageService: LocalStorageService,
    public http: HttpClient
  ) {}
  setToken(token: string) {
    this.localStorageService.set('is-logged-in', true);
    this.localStorageService.set('token', token);
  }
  getToken() {
    return this.localStorageService.get('token');
  }

  /** Entity levels Setter getter */
  setIndustry(industry: number) {
    this.industry = industry;
    this.localStorageService.set('industry', this.industry);
    // this.updateIndustryBehavior(); //TODO
    // this.setLabel();//TODO
  }
  getIndustry() {
    if (this.industry == undefined) {
      this.industry = this.localStorageService.get('industry');
    }
    return this.industry;
  }

  setTenantType(tenantType: string) {
    this.tenantType = tenantType;
    /* Do not store in localstorage as we can reduce local storage and can get it from tenant itself */
  }

  getTenantType() {
    if (this.tenantType == undefined) {
      this.tenantType = this.localStorageService.get('tenant')['tenantType'];
    }
    return this.tenantType;
  }
  setTenant(tenant: Tenant) {
    this.tenant = tenant;
    this.localStorageService.set('tenant', this.tenant);
    if (this.localStorageService.get('tenantTheme') == undefined) {
      this.localStorageService.set('tenantTheme', this.tenant);
    }
  }
  getTenant() {
    if (this.tenant == undefined) {
      this.tenant = this.localStorageService.get('tenant');
    }
    return this.tenant;
  }
  setRole(role: Role) {
    this.role = role;
    this.localStorageService.set('role', this.role);
    let permissionStr: any[] = [];
    // if (this.role.rolePermissions) {
    //     this.role.rolePermissions.forEach((element: any) => {
    //         permissionStr.push(element.name);
    //     });
    // }
    // // this.setRolePermission(permissionStr);
    // let accessibleMenusTmp = [];
    // if (this.role.roleIndustryMenu) {
    //     this.role.roleIndustryMenu.forEach((element: any) => {
    //         accessibleMenusTmp.push(element);
    //     });
    // }
    // let accessibleRoutesTmp = [];
    // if (this.role.roleIndustryRoute) {
    //     this.role.roleIndustryRoute.forEach((element: any) => {
    //         accessibleRoutesTmp.push(element);
    //     });
    // }
    // this.setAccessibleMenus(accessibleMenusTmp); //TODO
    // this.setAccessibleRoutes(accessibleRoutesTmp);//TODO
  }
  getRole() {
    if (this.role == undefined) {
      this.role = this.localStorageService.get('role');
    }
    return this.role;
  }
  setIsSuperAdmin() {
    this.isSuperAdmin = this.checkRolePermission('isSuperAdmin');
  }
  getIsSuperAdmin() {
    if (!this.isSuperAdmin) {
      this.setIsSuperAdmin();
    }
    return this.isSuperAdmin;
  }
  setUser(user: any) {
    this.user = user;
    this.localStorageService.set('user', this.user);
  }
  getUser() {
    if (this.user == undefined) {
      this.user = this.localStorageService.get('user');
    }
    return this.user;
  }
  isUrlAccessible(url: string, isRoute: boolean = false) {
    if (this.isVirtualTenant() && this.getIsSuperAdmin()) {
      if (isRoute) {
        if (!this.accessibleRouteUrls || this.accessibleRouteUrls.length == 0) {
          this.accessibleRouteUrls = this.localStorageService.get(
            'accessibleRouteUrls'
          );
        }
        let routeUrls = this.accessibleRouteUrls;
        if (routeUrls && routeUrls.indexOf(url) > -1) {
          return true;
        } else {
          return false;
        }
      } else {
        if (!this.accessibleMenuUrls || this.accessibleMenuUrls.length == 0) {
          this.accessibleMenuUrls = this.localStorageService.get(
            'accessibleMenuUrls'
          );
        }
        let menuUrls = this.accessibleMenuUrls;
        if (menuUrls && menuUrls.indexOf(url) > -1) {
          return true;
        } else {
          return false;
        }
      }
    }

    if (isRoute) {
      if (this.getIsSuperAdmin()) {
        return true;
      }
      if (!this.accessibleRouteUrls || this.accessibleRouteUrls.length == 0) {
        this.getAccessibleRoutes();
      }
      let routeUrls = this.accessibleRouteUrls;
      if (routeUrls && routeUrls.indexOf(url) > -1) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.getIsSuperAdmin()) {
        return true;
      }
      if (!this.accessibleMenuUrls || this.accessibleMenuUrls.length == 0) {
        this.getAccessibleMenus();
      }
      let menuUrls = this.accessibleMenuUrls;
      if (menuUrls && menuUrls.indexOf(url) > -1) {
        return true;
      } else {
        return false;
      }
    }
  }
  isVirtualTenant() {
    if (this.getTenantType() == 'Virtual') {
      return true;
    } else {
      return false;
    }
  }
  getAccessibleRoutes() {
    if (
      this.accessibleRoutes == undefined ||
      this.accessibleRoutes.length == 0
    ) {
      let role: Role = this.localStorageService.get('role');
      this.setAccessibleRoutes(role.roleIndustryRoute);
    }
    return this.accessibleRoutes;
  }
  setAccessibleRoutes(routes?: IndustryRoute[]) {
    if (routes) {
      this.accessibleRoutes = routes;
      this.accessibleRouteUrls = [];
      this.accessibleRoutes.forEach((element) => {
        this.accessibleRouteUrls.push(element.url);
      });
      this.localStorageService.set(
        'accessibleRouteUrls',
        this.accessibleRouteUrls
      );
    }
  }
  getAccessibleMenus() {
    if (this.accessibleMenus == undefined || this.accessibleMenus.length == 0) {
      let role: Role = this.localStorageService.get('role');
      this.setAccessibleMenus(role.roleIndustryMenu);
    }
    return this.accessibleMenus;
  }
  setAccessibleMenus(menus?: IndustryMenu[]) {
    if (menus) {
      this.accessibleMenus = menus;
      this.accessibleMenuUrls = [];
      this.accessibleMenus.forEach((element) => {
        this.accessibleMenuUrls.push(element.url);
      });
      this.localStorageService.set(
        'accessibleMenuUrls',
        this.accessibleMenuUrls
      );
    }
  }
  checkRolePermission(permission: any) {
    var permissions = this.getRolePermission();
    if (permission && permissions && permissions.length > 0) {
      for (var i = 0; i < permissions.length; i++) {
        if (permissions[i].name === permission) return true;
      }
    }
    return false;
  }
  getRolePermission() {
    this.role = this.getRole();
    if (
      this.role != undefined &&
      (this.rolePermission == undefined || this.rolePermission.length == 0)
    ) {
      let role: Role = this.localStorageService.get('role');
      this.setRolePermission(role?.rolePermissions);
    }
    return this.rolePermission;
  }
  setRolePermission(rolePermissions?: Permission[]) {
    this.rolePermission = rolePermissions;
    this.setIsSuperAdmin();
  }
  getAccessibleLocations() {
    if (
      this.accessibleLocations == undefined ||
      this.accessibleLocations.length == 0
    ) {
      this.accessibleLocations = this.localStorageService.get(
        'accessibleLocations'
      );
    }
    return this.accessibleLocations;
  }
  setAccessibleLocations(locations?: any[]) {
    if (locations) {
      this.accessibleLocations = locations;
      this.localStorageService.set(
        'accessibleLocations',
        this.accessibleLocations
      );
    }
  }
}
