import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractService } from './abstract.service';
import { IAMService } from './iam.service';
import { UtilService } from './util.service';
@Injectable()
export class TenantReportService extends AbstractService {
  constructor(http: HttpClient, utilService: UtilService, iamService: IAMService) {
    super(http, 'configuration', 'tenant-report', utilService, iamService);
  }
  findAllByTenantIntIdAndForIncentive(tenantId: number) {
    let tenantReport = `intId
      name
      unit
      isMoney
      isMajorKpi
      isSummable
      reportUniqueCode`;
    let queryString = {
      query:
        `query{
        findAllByTenantIntIdAndForIncentive(tenantIntId:` +
        tenantId +
        `) {
          ` +
        tenantReport +
        `
        }
      }`,
    };
    return this.gql(queryString);
  }
}
export class TenantReport {
  intId!: number;
  name!: string;
  unit!: string;
  isMoney!: boolean;
  isSummable!: boolean;
  isMajorKpi: boolean = false;
  reportUniqueCode!: string;
}
