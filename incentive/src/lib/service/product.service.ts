import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractService } from './abstract.service';
import { IAMService } from './iam.service';
import { UtilService } from './util.service';
@Injectable()
export class ProductService extends AbstractService {
  constructor(http: HttpClient, utilService: UtilService, iamService: IAMService) {
    super(http, 'product', 'product', utilService, iamService);
  }
  findAllByLocationGroupIntId(locationGroupId: number) {
    let product = `intId
    name
    locationGroupIntId`;
    let queryString = {
      query:
        `query{
        findAllByLocationGroupIntId(locationGroupIntId:` +
        locationGroupId +
        `,excludeBasicProduct:true) {
          ` +
        product +
        `
        }
      }`,
    };
    return this.gql(queryString);
  }
}
export class Product {
  intId!: number;
  name!: string;
}
