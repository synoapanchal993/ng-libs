import { HttpClient } from '@angular/common/http';
import { AbstractService } from './abstract.service';
import { Injectable } from '@angular/core';
import { IncentiveSearchForm } from '../model/incentive-search-form.model';
import { UtilService } from './util.service';
import { Status } from '../status.enum';
import { IAMService } from './iam.service';
@Injectable()
export class IncentiveService extends AbstractService {
  incentiveFields = `kid
    createdOn
    updatedOn
    activeStatus
    locationName
    locationGroupName
    name
    fromDate
    toDate    
    payoutDateForIncentive
    locationCurrency
    currency
    noOfTiers
    note
    tenantReportId
    entity
    majorKpiUnit
    forProduct
    maxIncentivePaidOut
    majorPayoutInPercentage
    dirty
    majorKpiQualify
    majorKpiQualifyKpi
    majorKpiQualifyKpiValueFrom
    majorKpiQualifyKpiValueTo
    majorKpiMultiplier
    majorKpiMultiplierKpi
    majorKpiKicker
    majorKpiKickerKpi
    majorKpiKickerQualifyKpiValueFrom
    majorKpiKickerQualifyKpiValueTo
    majorKpiKickerValueInPercentage
    majorKpiKickerValue
    holdPayoutInPercentage
    holdPayoutAmount
    holdPayoutDate
    configureHoldAmount
    isLocked
    isLocationBased
    isLocationBasedCommon
    superTierStartFrom
    frequency
    firstDayOfCycle
    lastDayOfCycle
    planMetricType
    stepNumber
    userNotInGroupNow {
      intId
      name
    }
    incentivePlanEntitys {
      intId
      industryId
      roleId
      regionId
      tenantId
      tenantLocationId
      locationGroupId
      userId
      user {
        name
      }
    }
    incentivePlanProductConfigs {
      intId
      locationGroupProductId
      locationGroupProduct {
        name
      }
      threshold
      isPercentage
    }
    incentivePlanMinorKpis {
      intId
      tenantReportId
      otherMinorKpi
      minorKpiUnit
      indexId
      minorAboveIncentiveQualify
      topEligibility
      minorPayoutInPercentage
      minorQualify
      minorKpiQualify
      minorKpiQualifyKpi
      minorKpiQualifyKpiValueFrom
      minorKpiQualifyKpiValueTo
      minorKpiMultiplier
      minorKpiMultiplierKpi
      minorKpiKicker
      minorKpiKickerKpi
      minorKpiKickerQualifyKpiValueFrom
      minorKpiKickerQualifyKpiValueTo
      minorKpiKickerValueInPercentage
      minorKpiKickerValue
      incentivePlanMinorTiers{
        intId
        tierNo
        tierName
        tierThreshold
        tierPayout
        locationTierThreshold
        locationTierEnabled
      }    
    }    
    incentivePlanPayouts {
      kid
      fromDate
      toDate
      payoutDate
      locked
      dirty
    }
    incentivePlanTier {
      intId
      tierNo
      tierName
      tierThreshold
      tierPayout
      locationTierThreshold
      locationTierEnabled
    }
    monthlyLocationTiers {
      intId
      month
      year
      fromDate
      toDate
      frequency
      day
      tierNo
      tierThreshold
      tierName
      actionTier
      actionTierForTier
      isLocationTierEnabled
    }
    majorKpiDetail {
      name
      kpiPrepand
      kpiAppand
      revenueKpi
    }
    majorKpiMultiplierDetail {
      name
    }
    majorKpiKickerDetail {
      name
      kpiPrepand
      kpiAppand
    }`;

  constructor(http: HttpClient, utilService: UtilService, iamService: IAMService) {
    super(http, 'incentive', 'incentive', utilService, iamService);
  }
  findById(kid: string, retriveObj?:any) {
    let queryString = {
      query:
        `query {
                findById (kid:\"` +
        kid +
        `\") {
                    ` +
                    (retriveObj?retriveObj:this.incentiveFields) +
        `
                  }
              }`,
    };
    return this.gql(queryString);
  }
  findByEntityIds(
    page: number,
    pageSize: number,
    incentiveSearchForm: IncentiveSearchForm,
    requestString: string,
  ) {
    let customfilterQuery = '';
    if (this.utilService.notBlank(incentiveSearchForm.customFilters)) {
      let cf: any = incentiveSearchForm.customFilters;
      customfilterQuery = ' customFilters:[';
      cf.forEach((filter: any) => {
        customfilterQuery +=
          '{' +
          'id:' +
          (this.utilService.notBlank(filter.id)
            ? this.utilService.addQuotes(filter.id)
            : null) +
          ',type:' +
          (this.utilService.notBlank(filter.type)
            ? this.utilService.addQuotes(filter.type)
            : null) +
          ',multipleSearchField:' +
          filter.multipleSearchField +
          ',searchField:' +
          (this.utilService.notBlank(filter.searchField)
            ? this.utilService.addQuotes(filter.searchField)
            : null) +
          ',searchFields:' +
          (this.utilService.notBlank(filter.searchFields)
            ? this.utilService.stringifyArray(filter.searchFields)
            : null) +
          ',selectedFilter:' +
          (this.utilService.notBlank(filter.selectedFilter)
            ? this.utilService.addQuotes(filter.selectedFilter)
            : null) +
          ',selectedValue:' +
          (this.utilService.notBlank(filter.selectedValue)
            ? this.utilService.addQuotes(filter.selectedValue)
            : null) +
          ',from:' +
          (this.utilService.notBlank(filter.from) ? filter.from : null) +
          ',to:' +
          (this.utilService.notBlank(filter.to) ? filter.to : null) +
          ',fromDate:' +
          (this.utilService.notBlank(filter.fromDate)
            ? this.utilService.addQuotes(filter.fromDate)
            : null) +
          ',toDate:' +
          (this.utilService.notBlank(filter.toDate)
            ? this.utilService.addQuotes(filter.toDate)
            : null) +
          '}';
      });
      customfilterQuery += ']';
    }
    let queryString = {
      query:
        `query {findByEntityIds (pageRequest: {page: ` + page + `,pageSize: ` + pageSize +
        `},incentiveSearchForm:{userIds: ` + (incentiveSearchForm.userIds != null ? JSON.stringify(incentiveSearchForm.userIds)
          : null) + `,tenantLocationId: ` +incentiveSearchForm.tenantLocationId+ `,tenantType: ` + this.iamService.getTenantType()+`,tenantId: ` + this.iamService.getTenant().intId
          + `, activeStatus:` + incentiveSearchForm.activeStatus
          + `, orderBy: ` + (this.utilService.notBlank(incentiveSearchForm.orderBy)
          ? this.utilService.addQuotes(incentiveSearchForm.orderBy)
          : null) +
           `, sortBy: ` + (this.utilService.notBlank(incentiveSearchForm.sortBy)
           ? this.utilService.addQuotes(incentiveSearchForm.sortBy)
           : null) + `, fromDate: ` +
        (this.utilService.notBlank(incentiveSearchForm.fromDate)
          ? this.utilService.addQuotes(incentiveSearchForm.fromDate)
          : null) + `, toDate: ` + (this.utilService.notBlank(incentiveSearchForm.toDate)
          ? this.utilService.addQuotes(incentiveSearchForm.toDate)
          : null) + `, ` + customfilterQuery + `}) {` + requestString + `}}`,
    }; //`, orderBy: "` + incentiveSearchForm.orderBy +
    return this.gql(queryString);
  }
  unLockIncentivePayout(kid: string) {
    let queryString = {
      query: `mutation{unLockIncentivePayout(incentivePlanPayoutKid:\"${kid}\")}`,
    };
    return this.gql(queryString);
  }
  updateUserStatus(kid: string, status: string) {
    let queryString = {
      query: `mutation{changeStatus(kid:\"${kid}\", status:${status})}`,
    };
    return this.gql(queryString);
  }
  exportIncentive(incentiveSearchForm: any,query:any) {
    let reqBody = {gql:JSON.stringify(query),incentiveSearchForm:incentiveSearchForm};
    return this.http.post(
      this.baseEntityPath + '/export',
      reqBody,
      { headers: this.headers, responseType: 'blob', observe: 'response' }
    );
  }

  getQueryForExport(
    page: number,
    pageSize: number,
    incentiveSearchForm: IncentiveSearchForm,
    requestString: string,
  ) {
    let customfilterQuery = '';
    if (this.utilService.notBlank(incentiveSearchForm.customFilters)) {
      let cf: any = incentiveSearchForm.customFilters;
      customfilterQuery = ' customFilters:[';
      cf.forEach((filter: any) => {
        customfilterQuery +=
          '{' +
          'id:' +
          (this.utilService.notBlank(filter.id)
            ? this.utilService.addQuotes(filter.id)
            : null) +
          ',type:' +
          (this.utilService.notBlank(filter.type)
            ? this.utilService.addQuotes(filter.type)
            : null) +
          ',multipleSearchField:' +
          filter.multipleSearchField +
          ',searchField:' +
          (this.utilService.notBlank(filter.searchField)
            ? this.utilService.addQuotes(filter.searchField)
            : null) +
          ',searchFields:' +
          (this.utilService.notBlank(filter.searchFields)
            ? this.utilService.stringifyArray(filter.searchFields)
            : null) +
          ',selectedFilter:' +
          (this.utilService.notBlank(filter.selectedFilter)
            ? this.utilService.addQuotes(filter.selectedFilter)
            : null) +
          ',selectedValue:' +
          (this.utilService.notBlank(filter.selectedValue)
            ? this.utilService.addQuotes(filter.selectedValue)
            : null) +
          ',from:' +
          (this.utilService.notBlank(filter.from) ? filter.from : null) +
          ',to:' +
          (this.utilService.notBlank(filter.to) ? filter.to : null) +
          ',fromDate:' +
          (this.utilService.notBlank(filter.fromDate)
            ? this.utilService.addQuotes(filter.fromDate)
            : null) +
          ',toDate:' +
          (this.utilService.notBlank(filter.toDate)
            ? this.utilService.addQuotes(filter.toDate)
            : null) +
          '}';
      });
      customfilterQuery += ']';
    }
    let queryString = {
      query:
        `query {findByEntityIds (pageRequest: {page: ` + page + `,pageSize: ` + pageSize +
        `},incentiveSearchForm:{userIds: ` + (incentiveSearchForm.userIds != null ? JSON.stringify(incentiveSearchForm.userIds)
          : null) + `,tenantLocationId: ` +incentiveSearchForm.tenantLocationId+ `,tenantType: ` + this.iamService.getTenantType()+`,tenantId: ` + this.iamService.getTenant().intId
          + `, activeStatus:` + incentiveSearchForm.activeStatus
          + `, orderBy: ` + (this.utilService.notBlank(incentiveSearchForm.orderBy)
          ? this.utilService.addQuotes(incentiveSearchForm.orderBy)
          : null) +
           `, sortBy: ` + (this.utilService.notBlank(incentiveSearchForm.sortBy)
           ? this.utilService.addQuotes(incentiveSearchForm.sortBy)
           : null) + `, fromDate: ` +
        (this.utilService.notBlank(incentiveSearchForm.fromDate)
          ? this.utilService.addQuotes(incentiveSearchForm.fromDate)
          : null) + `, toDate: ` + (this.utilService.notBlank(incentiveSearchForm.toDate)
          ? this.utilService.addQuotes(incentiveSearchForm.toDate)
          : null) + `, ` + customfilterQuery + `}) {` + requestString + `}}`,
    }; //`, orderBy: "` + incentiveSearchForm.orderBy +
    return queryString;
  }
  uploadImage(data: string, id: string) {
    let reqBody = JSON.stringify(data);
    let industryId = this.iamService.getIndustry();
    return this.http.post(
      this.getMicroServicesBase() +
        '/configuration/uploadData/uploadImage/' +
        industryId + '/' + id,
      reqBody,
      { headers: this.headers}
    );
  }
}
