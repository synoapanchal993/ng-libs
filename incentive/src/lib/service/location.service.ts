import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractService } from './abstract.service';
import { IAMService } from './iam.service';
import { UtilService } from './util.service';
@Injectable()
export class LocationService extends AbstractService {
  constructor(http: HttpClient, utilService: UtilService, iamService: IAMService) {
    super(http, 'account', 'location', utilService, iamService);
  }
  getLocationByTenantIntId(tenantId: number) {
    let tenantLocation = `intId
      name
      localCurrency{
        code
      }
      region{
        currency
      }`;
    let queryString = {
      query:
        `query{
        findAllByTenantIntId(tenantIntId:` +
        tenantId +
        `) {
          ` +
        tenantLocation +
        `
        }
      }`,
    };
    return this.gql(queryString);
  }

  findAccessibleLocationsOfUsers(tenantId: number) {
    let tenantLocation = `intId
      name
      localCurrency{
        code
      }
      region{
        currency
      }`;
    let queryString = {
      query:
        `query{
        findAccessibleLocationsOfUsers(tenantId:` + tenantId + `,userId:` + this.iamService.getUser()?.intId +
        `,tenantType:` + this.iamService.getTenantType() +
        `) {
          ` +
        tenantLocation +
        `
        }
      }`,
    };
    return this.gql(queryString);
  }

}
export class Location {
  intId!: number;
  name!: string;
  localCurrency!: LocalCurrency;
  region!: Region;
}
export class LocalCurrency {
  code!: string;
}
export class Region {
  currency!: string
}
