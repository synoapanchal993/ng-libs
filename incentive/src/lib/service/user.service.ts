import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractService } from './abstract.service';
import { UtilService } from './util.service';
import { IAMService } from './iam.service';
@Injectable()
export class UserService extends AbstractService {
  constructor(http: HttpClient, utilService: UtilService, iamService: IAMService) {
    super(http, 'account', 'user', utilService, iamService);
  }
  findUsers(tenantId: number, locationGroupId: number) {
    let user = `intId
    fullName`;
    let queryString = {
      query:
        `query{
        findUsers(userSearchForm:{
          locationGroupId:` +
        locationGroupId +
        `
          isContributorRegardlessOfOperator: true
          isFPGMember: false
          userStatus: Normal
          currentTenantId: ` +
        tenantId +
        ` 
        }) {
          ` +
        user +
        `
        }
      }`,
    };
    return this.gql(queryString);
  }
}

export class User {
  intId!: number;
  fullName!: string;
}
