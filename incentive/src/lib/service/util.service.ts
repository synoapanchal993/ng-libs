import { Injectable } from '@angular/core';

@Injectable()
export class UtilService {
  public isEnableWaitIndicator = true;
  constructor() {}
  notBlank(val: any) {
    return val == undefined ||
      val == null ||
      (typeof val == 'string' && val.trim() == '') ||
      (val instanceof Array && val.length < 1)
      ? false
      : true;
  }
  blank(val: any) {
    return val == undefined ||
      val == null ||
      (typeof val == 'string' && val.trim() == '') ||
      (val instanceof Array && val.length < 1)
      ? true
      : false;
  }
  emptyValue(val: any) {
    if (val == undefined || val == null) {
      return '';
    } else {
      return val;
    }
  }
  convertArrayToString(_arr: any[]) {
    return this.notBlank(_arr) && _arr instanceof Array ? _arr.join(',') : '';
  }
  getJsonColumnData(org: any, column: any) {
    let check = column.startsWith('json');
    let value = org[column];
    if (check && this.notBlank(org.jsonMap)) {
      value = org.jsonMap[column];
    }
    return this.notBlank(value) ? value : '';
  }
  isTrue(value: any) {
    return value == true || value == 'true';
  }
  addQuotes(str: any) {
    return '"' + str + '"';
  }
  addQuotesInString(str: any) {
    return '\\"' + str + '\\"';
  }
  stringifyArray(arr: any[]) {
    let retStr = '[';
    arr.forEach((element) => {
      retStr += this.addQuotes(element) + ',';
    });
    return (retStr += ']');
  }

  getFileNameFromHttpResponse(httpResponse: any) {
    if (httpResponse.headers) {
      var contentDispositionHeader = httpResponse.headers.get(
        'Content-Disposition'
      );
      var result = '';
      if (
        contentDispositionHeader != undefined &&
        contentDispositionHeader != ''
      ) {
        result = contentDispositionHeader.split(';')[1].trim().split('=')[1];
      }
      return result.replace(/"/g, '');
    } else {
      return 'File';
    }
  }
}
