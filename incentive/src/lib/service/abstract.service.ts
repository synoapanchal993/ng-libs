import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { IAMService } from './iam.service';
import { UtilService } from './util.service';

export abstract class AbstractService {
  protocol = window.location.protocol;
  host = window.location.hostname;
  appName = 'm-api';
  basePath: string = this.protocol + '//' + this.host + '/' + this.appName;
  constructor(
    public http: HttpClient,
    private microservice: string,
    private entity: string,
    public utilService: UtilService,
    public iamService: IAMService
  ) {}
  get baseEntityPath(): string {
    return this.basePath + '/' + this.microservice + '/' + this.entity;
  }
  getMicroServicesBase(): string {
    return this.basePath;
  }
  getMicroservice(): string {
    return this.microservice;
  }
  getEntity(): string {
    return this.entity;
  }

  get headers(): HttpHeaders{
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append(
      'Authorization',
      'bearer ' + this.iamService.getToken()
    );
    headers = headers.append('IndustryId', this.iamService.getIndustry() + '');
    headers = headers.append('content-type', 'application/json');
    headers = headers.append('platform', '2');
    if (this.utilService.notBlank(this.iamService.getTenant())) {
      headers = headers.append(
        'TenantId',
        this.iamService.getTenant()?.intId + ''
      );
    }
    return headers;
  }


  get params(): HttpParams {
    let params: HttpParams = new HttpParams();
    return params;
  }

  /* POST GQL */
  gql(payload: any) {
    return this.http.post(this.baseEntityPath + '/gql', payload, {
      params: this.params,
      headers: this.headers,
    });
  }

  stringify(obj_from_json: string | any[] | Date | null): string | null {
    // In case object is null
    if (obj_from_json == null || obj_from_json == 'null') {
      return null;
    }
    // In case of an array we'll stringify all objects.
    if (Array.isArray(obj_from_json)) {
      return `[${obj_from_json
        .map((obj) => `${this.stringify(obj)}`)
        .join(',')}]`;
    }
    // not an object, stringify using native function
    if (
      typeof obj_from_json !== 'object' ||
      obj_from_json instanceof Date ||
      obj_from_json === null
    ) {
      return JSON.stringify(obj_from_json);
    }
    // Implements recursive object serialization according to JSON spec
    // but without quotes around the keys.
    return `{${Object.keys(obj_from_json)
      .map((key) => `${key}:${this.stringify(obj_from_json[key])}`)
      .join(',')}}`;
  }
}
