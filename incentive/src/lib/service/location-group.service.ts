import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractService } from './abstract.service';
import { UtilService } from './util.service';
import { IAMService } from './iam.service';
@Injectable()
export class LocationGroupService extends AbstractService {
  constructor(http: HttpClient, utilService: UtilService, iamService: IAMService) {
    super(http, 'account', 'location-group', utilService, iamService);
  }
  findByLocationId(tenantLocationId: number) {
    let locationGroup = `intId
      name`;
    let queryString = {
      query:
        `query{
        findByLocationId(locationId:` +
        tenantLocationId +
        `) {
          ` +
        locationGroup +
        `
        }
      }`,
    };
    return this.gql(queryString);
  }
}
export class LocationGroup {
  intId!: number;
  name!: string;
}
