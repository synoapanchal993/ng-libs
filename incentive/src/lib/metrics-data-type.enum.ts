export enum MetricsDataType {
  Arrival = 0,

  Daily = 1,

  Departure = 2,
}
