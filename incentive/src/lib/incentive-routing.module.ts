import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IncentiveAddComponent } from './incentive-add/incentive-add.component';
import { IncentiveSearchComponent } from './incentive-search/incentive-search.component';
import { IncentiveComponent } from './incentive.component';

const routes: Routes = [
   {
   path: '', component: IncentiveComponent, children: [
  { path: '', component: IncentiveSearchComponent },
  { path: 'add', component: IncentiveAddComponent },
  { path: ':kid', component: IncentiveAddComponent },
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IncentiveRoutingModule {}
