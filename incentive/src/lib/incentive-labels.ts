export const labels: any = {
  step1Info:
    'Configure the property and department for which you want to set the incentive plan.\n Modifing the department Step2 (Team members detail) and Step6 (Product Configuration) will be reset.',
  step1Details:
    'Configure the property and department for which you want to set the incentive plan.\n Modifing the department Step2 (Team members detail) and Step6 (Product Configuration) will be reset.',

  step2Info: 'Configure timeframe and payout cycle of your incentive.',
  step2Details: 'Configure timeframe and payout cycle of your incentive.',
  payoutDate:
    'This is the date that IN-Gauge stops calculating incentive income.  For example, if 5 days after the incentive cycle ends is selected, calculation of the incentive for the previous cycle will not end until 5 calendar days after incentive cycle close.',

  step3Info: 'Add the team and configure metric type to calculation incentive.',
  step3Details:
    'Add the team and configure metric type to calculation incentive.',
  agentSelection:
    'Please select participant(s) for the incentive plan.  If a new user is added to your location or team, they must be added to the incentive plan.  If they are not added to the incentive plan their commissionable revenue will not populate.',

  step4Info: 'Configure the KPI, on which commission to be calculated.',
  step4Details: 'Configure the KPI, on which commission to be calculated.',
  primaryKpi:
    'This is the KPI that will be used to calculate incentive payout.',
  payoutInPer:
    'Select whether you will be paying incentive based on a percentage or a flat amount of revenue.',
  multiplier:
    'Configure multiplier, if you are paying incentive on kpi other than Revenue',

  step5Info: 'Set the tier threshold and payouts',
  step5Details: 'Set the tier threshold and payouts',
  limitCommission: 'Select Yes to pay incentive per each qualifying tier level, select No to pay entire incentive at highest qualifying tier level.',
  step6Info:
    'Configure incentive for specific product or apply commission threshold on products.',
  step6Details:
    'Configure incentive for specific product or apply commission threshold on products.',
    productThreshold:'Check the box if there is an incentive restriction on a product.  For example if the first 10% or $250 of a  products revenue is not eligible towards incentive calculations.' ,

  step7Info: 'Step Information needs to be displayed here',
  step7Details: 'Step Information needs to be displayed here',

  step8Info: 'Step Information needs to be displayed here',
  step8Details: 'Step Information needs to be displayed here',
  kicker: 'Select Yes to pay an  additional performance incentive based on a selected KPI.',
  kickerMax:'If you are not capping on max limit, enter the highest number in Max value.',

  step9Info: 'Step Information needs to be displayed here',
  step9Details: 'Step Information needs to be displayed here',
  maxCap: 'Setting a cap will limit how much participants can earn in an incentive cycle.',
  
};
