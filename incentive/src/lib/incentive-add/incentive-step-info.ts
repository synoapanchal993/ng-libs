import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { maxLength, minLength, required } from '@fpg-component/validation';
import { StepInfo } from '@fpg-component/wizard';
import * as moment from 'moment';
import { IncentiveFrequency } from '../incentive-frequency.enum';
import { WeekDays } from '../weekdays.enum';
import {
  ValidateThreshold,
  atLeastOneCheckboxCheckedValidator,
  ValidateMinMax,
  ValidateLocationThreshold,
  ValidateMonthlyLocationThreshold,
} from './incentive.validator';
import { MetricsDataType } from '../metrics-data-type.enum';
const DATE_FORMAT: string = 'YYYY-MM-DD';
export class IncentiveStepInfo {
  today = moment();
  clickableStep = false;
  ranges: { value: Date[]; label: string }[] = [
    {
      value: [
        moment().startOf('week').toDate(),
        moment().endOf('week').toDate(),
      ],
      label: 'This Week',
    },
    {
      value: [
        moment().add(1, 'weeks').startOf('week').toDate(),
        moment().add(1, 'weeks').endOf('week').toDate(),
      ],
      label: 'Next Week',
    },
    {
      value: [
        moment().startOf('month').toDate(),
        moment().endOf('month').toDate(),
      ],
      label: 'This Month',
    },
    {
      value: [
        moment().add(1, 'months').startOf('month').toDate(),
        moment().add(1, 'months').endOf('month').toDate(),
      ],
      label: 'Next Month',
    },
    {
      value: [
        moment().startOf('quarter').toDate(),
        moment().endOf('quarter').toDate(),
      ],
      label: 'This Quarter',
    },
    {
      value: [
        moment().startOf('year').toDate(),
        moment().endOf('year').toDate(),
      ],
      label: 'This Year',
    },
  ];
  skippable: number[] = [];//[6, 7, 8];
  step: number = 1;
  /* fromSummary: boolean = false; */
  steps: StepInfo[] = [
    {
      title: 'Property Details',
      //wizardInfo: '<label class="required">*</label> - Mandatory fields. ',
      hasSubSteps: true,
      subSteps: [
        'Select Your Property',
        'Select Your Departement',
        'Select Your Incentive Plan',
      ],
      contButton: 'Continue',
      draftButton: 'Save as Draft',
    },
    
    {
      title: 'Timeframe and Payout Details',
      //wizardInfo: '<label class="required">*</label> - Mandatory fields. ',
      hasSubSteps: true,
      subSteps: ['Frequency of the Payout', 'Select the Payout Date'],
      contButton: 'Continue',
      draftButton: 'Save as Draft',
    },{
      title: 'Team Members Details',
      //wizardInfo: '<label class="required">*</label> - Mandatory fields. ',
      hasSubSteps: true,
      subSteps: ['Select Your Plan Metric Type', 'Label Your Incentive Plan'],
      contButton: 'Continue',
      draftButton: 'Save as Draft',
    },
    {
      title: 'KPIs Configuration',
      //wizardInfo: '<label class="required">*</label> - Mandatory fields. ',
      hasSubSteps: true,
      subSteps: [
        'KPIs Configuration',
        'Commission Payout',
        'Multiplier Configuration',
      ],
      contButton: 'Continue',
      draftButton: 'Save as Draft',
    },
    {
      title: 'Incentive Tiers Details',
      //wizardInfo: '<label class="required">*</label> - Mandatory fields. ',
      hasSubSteps: true,
      subSteps: [
        'Select the Number of Tiers',
        'Configure Qualification of Agents',
      ],
      contButton: 'Continue',
      draftButton: 'Save as Draft',
    },
    /* Location tier code */
    /* {
      title: 'Incentive Super Tiers Details',
      hasSubSteps: true,
      subSteps: [
        'Configure Threshold of Location',
      ],
      contButton: 'Continue',
      draftButton: 'Save as Draft',
    },
     */{
      title: 'Product Configuration',
      //wizardInfo: '<label class="required">*</label> - Mandatory fields. ',
      hasSubSteps: true,
      subSteps: ['Select Your Products', 'Configure Product Threshold'],
      contButton: 'Continue',
      draftButton: 'Save as Draft',
    },
    {
      title: 'Advanced Settings',
      //wizardInfo: '<label class="required">*</label> - Mandatory fields. ',
      hasSubSteps: true,
      subSteps: ['Kicker Configuration', 'Cap paticipant\'s commission'],
      contButton: 'Continue',
      draftButton: 'Save as Draft',
    },
    {
      title: 'Summary',
      wizardInfo: '',
      hasSubSteps: false,
      contButton: 'Activate Your Incentive Plan',
    },
  ];
}

export function getIncentiveForm(
  formBuilder: FormBuilder,
  initialDates: Date[]
): FormGroup[] {
  return [
    //Step 1
    formBuilder.group({
      name: ['', [required(), minLength(3, 'This field requires an entry with a minimum of three characters'), maxLength(40)]],
      location: ['', [required()]],
      department: ['', [required()]],
    }),
    //Step 2    
    formBuilder.group({
      timeframe: [initialDates, [required()]],
      frequency: ['' + IncentiveFrequency.Monthly, [required()]],
      /* firstDayOfCycle: [''], */
      payoutDateForIncentive: ['-1', [required()]],
    }),
    //Step 3
    formBuilder.group(
      {
        planMetricType: ['' + MetricsDataType.Departure, [required()]],
        teamMemberSrch: null,
        users: new FormArray([]),
      },
      {
        validators: atLeastOneCheckboxCheckedValidator(
          1,
          'users',
          'userSelected'
        ),
      }
    ),
    //Step 4
    formBuilder.group({
      primaryKpi: ['', [required()]],
      payoutInPercentage: '1',
      multiplier: '0',
      multiplierKpi: null,
    }),
    //Step 5
    formBuilder.group(
      {
        noOfTiers: '3',
        tiers: formBuilder.array([]),
        supertier: '-1',
      },
      { validators: ValidateThreshold }
    ),
    //Step 6  location tier code
   /*  formBuilder.group(
      {
        locationTierQualify: '0',
        noOfLocationTiers: '3',
        locationBasedCommon: '1',
        locationTiers: formBuilder.array([]),
        monthlyLocationTiers: formBuilder.array([]),
      },
      { validators: ValidateLocationThreshold, ValidateMonthlyLocationThreshold }
    ), */
    //Step 7
    formBuilder.group({
      productSrch: null,
      products: new FormArray([]),
      productSpecific: '0',
      productThreshold: '0',
      productThresholds: formBuilder.array([]),
    }),
    //Step 8
    formBuilder.group(
      {
        kicker: '0',
        maxPayout: '0',
        /* maxIncentivePaidOut: null, */
        holdPayout: '0',
        /* holdPayoutAmount: null,
      holdPayoutInPercentage: '1',
      holdPayoutDate: initialDates[0] */
        /* kickerKpi: [null, required()],
      kickerMin: [null, required()],
      kickerMax: [null, required()],
      kickerPayoutInPercentage: ['1', required()],
      kickerPayout: [null, required()], */
      },
      { validators: ValidateMinMax('kickerMin', 'kickerMax') }
    ),
  ];
}

export function getIntervalWithLockedInfo(
  startDate: Date,
  endDate: Date,
  frequency: IncentiveFrequency,
  incentivePlanPayouts: [] | null,
  payoutDay: number,
  firstDayOfCycle: WeekDays,
  lastDayOfCycle: WeekDays,
  fetchValidEndDate: boolean
): any {
  /* console.log(startDate);
  console.log(endDate);
  console.log(frequency);
  console.log(incentivePlanPayouts);
  console.log(payoutDay);
  console.log(firstDayOfCycle);
  console.log(lastDayOfCycle);
  console.log(fetchValidEndDate); */

  let returnObj: any = {};
  let intervals = [];
  let startIntervalDate!: moment.Moment;
  let endIntervalDate!: moment.Moment;
  let endDateMoment = moment(endDate);
  let startDateMoment = moment(startDate);
  let payoutDate;
  /* let frequncyIndex = frequency;
  if (parseInt(IncentiveFrequency[frequency])) {
    frequncyIndex = parseInt(IncentiveFrequency[frequency]);
  } */

  if (frequency == IncentiveFrequency.Monthly) {
    let lastDayOfMonth;
    let diff = endDateMoment.diff(startDateMoment, 'months');
    for (let i = 0; i <= diff; i++) {
      startIntervalDate = moment(startDate).add(i, 'months');
      if (startIntervalDate.toDate() > startDate) {
        startIntervalDate = startIntervalDate.startOf('month');
      }
      endIntervalDate = moment(startDate).add(i, 'months').endOf('month');
      lastDayOfMonth = moment(startDate).add(i, 'months').endOf('month');
      if (endIntervalDate.toDate() > endDate) {
        endIntervalDate = endDateMoment;
      }
      let payoutDate;
      if (payoutDay == -1) {
        payoutDate = lastDayOfMonth;
      } else {
        payoutDate = moment(
          lastDayOfMonth.set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
        ).add(payoutDay, 'days');
        if (
          payoutDate.toDate().getMonth() - lastDayOfMonth.toDate().getMonth() >
          1
        ) {
          payoutDate = moment(lastDayOfMonth).add(1, 'months').endOf('month');
        }
      }
      intervals.push({
        kid: null,
        fromDate: moment(startIntervalDate.format('YYYY-MM-DD')).valueOf(),
        toDate: moment(endIntervalDate.format('YYYY-MM-DD')).valueOf(),
        payoutDate: moment(payoutDate.format('YYYY-MM-DD')).valueOf(),
        fromDateStr: startIntervalDate.format('MMM DD, YYYY'),
        toDateStr: endIntervalDate.format('MMM DD, YYYY'),
        payoutDateStr: payoutDate.format('MMM DD, YYYY'),
        monthNum: startIntervalDate.month() + 1,
        monthName: startIntervalDate.format('MMMM'),
        year: startIntervalDate.year(),
        day: startIntervalDate.date(),
      });
    }
    if (endIntervalDate.toDate() < endDate) {
      startIntervalDate = endIntervalDate.add(1, 'day');
      endIntervalDate = endDateMoment;
      lastDayOfMonth = moment(startIntervalDate).endOf('month');
      intervals.push({
        kid: null,
        fromDate: moment(startIntervalDate.format('YYYY-MM-DD')).valueOf(),
        toDate: moment(endIntervalDate.format('YYYY-MM-DD')).valueOf(),
        payoutDate: moment
          (
            payoutDay == -1
              ? lastDayOfMonth.format('YYYY-MM-DD')
              : moment(
                  lastDayOfMonth.set({
                    hour: 0,
                    minute: 0,
                    second: 0,
                    millisecond: 0,
                  })
                )
                  .add(payoutDay, 'days')
                  .format('YYYY-MM-DD')
          )
          .valueOf(),
        fromDateStr: startIntervalDate.format('MMM DD, YYYY'),
        toDateStr: endIntervalDate.format('MMM DD, YYYY'),
        payoutDateStr:
          payoutDay == -1
            ? lastDayOfMonth.format('MMM DD, YYYY')
            : moment(
                lastDayOfMonth.set({
                  hour: 0,
                  minute: 0,
                  second: 0,
                  millisecond: 0,
                })
              )
                .add(payoutDay, 'days')
                .format('MMM DD, YYYY'),
        monthNum: startIntervalDate.month() + 1,
        monthName: startIntervalDate.format('MMMM'),
        year: startIntervalDate.year(),
        day: startIntervalDate.date(),
      });
    }
  } else if (
    frequency == IncentiveFrequency.Weekly ||
    frequency == IncentiveFrequency.Biweekly
  ) {
    let dayDuration = 7;
    let payoutDate;
    let break_loop = false;
    let i = 0;
    if (frequency == IncentiveFrequency.Biweekly) {
      dayDuration = 14;
    }
    startIntervalDate = startDateMoment;
    endIntervalDate = startDateMoment;
    let firstDayCycleSelected = WeekDays[firstDayOfCycle];
    let firstDayInCalendarSelected = WeekDays[moment(startIntervalDate).day()];

    while (!break_loop) {
      if (i == 0) {
        endIntervalDate = moment(startIntervalDate)
          .startOf('week')
          .add(lastDayOfCycle, 'days');
        if (moment(endIntervalDate) < moment(startIntervalDate)) {
          endIntervalDate = endIntervalDate.add(dayDuration, 'days');
        } else {
          if (
            firstDayCycleSelected == firstDayInCalendarSelected &&
            frequency == IncentiveFrequency.Biweekly
          ) {
            endIntervalDate = endIntervalDate.add(7, 'days');
          }
        }
      } else {
        startIntervalDate = moment(endIntervalDate).add(1, 'days');
        endIntervalDate = moment(startIntervalDate).add(
          dayDuration - 1,
          'days'
        );
      }
      payoutDate = endIntervalDate;
      if (endIntervalDate >= endDateMoment) {
        endIntervalDate = endDateMoment;
        break_loop = true;
      }
      if (payoutDay != -1) {
        payoutDate = moment(payoutDate).add(payoutDay, 'days');
      }
      intervals.push({
        kid: null,
        fromDate: moment(startIntervalDate.format('YYYY-MM-DD')).valueOf(),
        toDate: moment(endIntervalDate.format('YYYY-MM-DD')).valueOf(),
        payoutDate: moment(payoutDate.format('YYYY-MM-DD')).valueOf(),
        fromDateStr: startIntervalDate.format('MMM DD, YYYY'),
        toDateStr: endIntervalDate.format('MMM DD, YYYY'),
        payoutDateStr: payoutDate.format('MMM DD, YYYY'),
        monthNum: startIntervalDate.month() + 1,
        monthName: startIntervalDate.format('MMMM'),
        year: startIntervalDate.year(),
        day: startIntervalDate.date(),
      });
      i++;
    }
  } else if (frequency == IncentiveFrequency.Semi_Monthly) {
    let startDay = moment(startDate).date();
    let endDay = moment(endDate).date();
    let totalInterval = 0;
    if (
      moment(startDate).year() == moment(endDate).year() &&
      moment(startDate).month() == moment(endDate).month() &&
      ((startDay <= 15 && endDay <= 15) || (startDay > 15 && endDay > 15))
    ) {
      totalInterval = 1;
    } else {
      totalInterval =
        (moment(endDate)
          .endOf('months')
          .diff(moment(startDate).startOf('month'), 'months') +
          1) *
        2;
    }
    startIntervalDate = moment(startDate);
    startIntervalDate = moment(startDate);

    for (let i = 0; i < totalInterval; i++) {
      if (i > 0) {
        startIntervalDate = moment(endIntervalDate).add(1, 'days');
      }
      //endIntervalDate = moment(startIntervalDate).add(14, 'days');

      if (moment(startIntervalDate).date() <= 15) {
        startIntervalDate = moment(startIntervalDate).startOf('months');
        endIntervalDate = moment(startIntervalDate)
          .startOf('months')
          .add(14, 'days');
        payoutDate = moment(endIntervalDate).startOf('months').add(14, 'days');
      } else {
        endIntervalDate = moment(startIntervalDate).endOf('months');
        payoutDate = moment(endIntervalDate).endOf('months');
      }
      if (moment(startDate) > moment(startIntervalDate)) {
        startIntervalDate = moment(startDate);
      }
      if (moment(endDate) <= moment(endIntervalDate)) {
        endIntervalDate = moment(endDate);
        i = totalInterval;
      }

      if (payoutDay != -1) {
        payoutDate = moment(payoutDate).add(payoutDay, 'days');
      }
      intervals.push({
        kid: null,
        fromDate: moment(startIntervalDate.format('YYYY-MM-DD')).valueOf(),
        toDate: moment(endIntervalDate.format('YYYY-MM-DD')).valueOf(),
        payoutDate: moment(payoutDate.format('YYYY-MM-DD')).valueOf(),
        fromDateStr: startIntervalDate.format('MMM DD, YYYY'),
        toDateStr: endIntervalDate.format('MMM DD, YYYY'),
        payoutDateStr: payoutDate.format('MMM DD, YYYY'),
        monthNum: startIntervalDate.month() + 1,
        monthName: startIntervalDate.format('MMMM'),
        year: startIntervalDate.year(),
        day: startIntervalDate.date(),
        locked: false,
      });
    }
  }
  intervals = intervals.sort(function (a, b) {
    // Turn your strings into dates, and then subtract them
    // to get a value that is either negative, positive, or zero.
    if (a.payoutDate > b.payoutDate) return 1;
    if (a.payoutDate < b.payoutDate) return -1;
    return 0;
  });
  intervals.forEach((element) => {
    let strFromDate = moment(element.fromDate).format('YYYY-MM-DD');
    let strToDate = moment(element.toDate).format('YYYY-MM-DD');
    if (fetchValidEndDate && incentivePlanPayouts) {
      let data: any = null;
      if (frequency == IncentiveFrequency.Monthly) {
        data = incentivePlanPayouts.find(
          (x: any) =>
            moment(moment(x.fromDate, DATE_FORMAT)).month() ==
              moment(strFromDate, DATE_FORMAT).month() &&
            moment(x.fromDate, DATE_FORMAT).year() ==
              moment(strFromDate, DATE_FORMAT).year() &&
            x.locked
        );
      } else {
        data = incentivePlanPayouts.find(
          (x: {
            fromDate: moment.MomentInput;
            locked: any;
            toDate: moment.MomentInput;
          }) =>
            moment(moment(x.fromDate, DATE_FORMAT)).month() ==
              moment(strFromDate, DATE_FORMAT).month() &&
            moment(x.fromDate, DATE_FORMAT).year() ==
              moment(strFromDate, DATE_FORMAT).year() &&
            x.locked &&
            moment(x.fromDate, DATE_FORMAT).date() ==
              moment(strFromDate, DATE_FORMAT).date() &&
            moment(moment(x.toDate, DATE_FORMAT)).month() ==
              moment(strToDate, DATE_FORMAT).month() &&
            moment(x.toDate, DATE_FORMAT).year() ==
              moment(strToDate, DATE_FORMAT).year() &&
            moment(x.toDate, DATE_FORMAT).date() ==
              moment(strToDate, DATE_FORMAT).date()
        );
      }
      // console.log(strFromDate);
      // console.log(element.locked);
      if (data) {
        element.locked = true;
        element.kid = data?.kid;
      }

      // console.log(element.locked);
      if (element.locked) {
        returnObj.validEndDate = new Date(element.toDate);
      }
    }
  });
  console.log(intervals);
  returnObj.intervals = intervals;
  return returnObj;
}
export function setlastDayOfCycle(firstDayOfCycleValue: WeekDays): WeekDays {
  let lastDayOfCycle: WeekDays = WeekDays.Sunday;
  if (firstDayOfCycleValue == WeekDays.Sunday) {
    lastDayOfCycle = WeekDays.Saturday;
  } else if (firstDayOfCycleValue == WeekDays.Monday) {
    lastDayOfCycle = WeekDays.Sunday;
  } else if (firstDayOfCycleValue == WeekDays.Tuesday) {
    lastDayOfCycle = WeekDays.Monday;
  } else if (firstDayOfCycleValue == WeekDays.Wednesday) {
    lastDayOfCycle = WeekDays.Tuesday;
  } else if (firstDayOfCycleValue == WeekDays.Thursday) {
    lastDayOfCycle = WeekDays.Wednesday;
  } else if (firstDayOfCycleValue == WeekDays.Friday) {
    lastDayOfCycle = WeekDays.Thursday;
  } else if (firstDayOfCycleValue == WeekDays.Saturday) {
    lastDayOfCycle = WeekDays.Friday;
  } else {
    lastDayOfCycle = WeekDays.Sunday;
  }
  return lastDayOfCycle;
}
