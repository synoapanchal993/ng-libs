import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncentiveAddComponent } from './incentive-add.component';

describe('IncentiveAddComponent', () => {
  let component: IncentiveAddComponent;
  let fixture: ComponentFixture<IncentiveAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IncentiveAddComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncentiveAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
