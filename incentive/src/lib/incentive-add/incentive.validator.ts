import {
  FormArray,
  ValidatorFn,
  AbstractControl,
  ValidationErrors,
  FormGroup,
} from '@angular/forms';

export function ValidateThreshold(
  control: AbstractControl
): ValidationErrors | null {
  const formArray: FormArray = <FormArray>control.get('tiers');
  let returnVal = null;
  formArray.controls.forEach((formGroup, i) => {
    let c = parseFloat(formArray.controls[i].get('threshold')?.value);
    let n = parseFloat(formArray.controls[i + 1]?.get('threshold')?.value);
    if (c > n) {
      returnVal = {
        incremental: {
          message: '',
          value: control.value,
        },
      };
    }
  });
  return returnVal;
}

export function ValidateLocationThreshold(
  control: AbstractControl
): ValidationErrors | null {
  const formArray: FormArray = <FormArray>control.get('locationTiers');
  let returnVal = null;
  formArray.controls.forEach((formGroup, i) => {
    let c = parseFloat(formArray.controls[i].get('locationTierThreshold')?.value);
    let n = parseFloat(formArray.controls[i + 1]?.get('locationTierThreshold')?.value);
    if (c > n) {
      returnVal = {
        incremental: {
          message: '',
          value: control.value,
        },
      };
    }
  });
  return returnVal;
}

export function ValidateMonthlyLocationThreshold(
  control: AbstractControl
): ValidationErrors | null {
  const formArray: FormArray = <FormArray>control.get('monthlyLocationTiers');
  let returnVal = null;
  formArray.controls.forEach((formGroup, i) => {
    const form: FormArray = <FormArray>formArray.controls[i]?.get('tierThresholds');
    form.controls.forEach((fGroup, j) => {
    let c = parseFloat(form.controls[j].get('tierThreshold')?.value);
    let n = parseFloat(form.controls[j + 1]?.get('tierThreshold')?.value);
    if (c > n) {
      returnVal = {
        incremental: {
          message: '',
          value: control.value,
        },
      };
    }
  });
  });
  return returnVal;
}
export function ValidateMinMax(
  minControl: string,
  maxControl: string
): ValidatorFn | null {
  return function validate(control: AbstractControl) {
    let returnVal = null;
    let c = parseFloat(control.get(minControl)?.value);
    let n = parseFloat(control?.get(maxControl)?.value);
    if (c > n) {
      returnVal = {
        incremental: {
          message: '',
          value: control.value,
        },
      };
    }
    return returnVal;
  };
}

export function atLeastOneCheckboxCheckedValidator(
  minRequired = 1,
  formArrayName: string,
  formControlName: string
): ValidatorFn | null {
  return function validate(control: AbstractControl) {
    const formArray: FormArray = <FormArray>control.get(formArrayName);
    let returnVal = null;
    let checked = 0;
    formArray.controls.forEach((formGroup, i) => {
      let c = formArray.controls[i].get(formControlName)?.value;
      if (c === true) {
        checked++;
      }
    });
    if (checked < minRequired) {
      returnVal = {
        minSelection: {
          message: '',
          value: control.value,
        },
      };
    }
    return returnVal;
  };
}

/* export function atLeastOneCheckboxCheckedValidator(minRequired = 1, formControlKey: string): ValidationErrors | null {
    return function validate(formGroup: FormGroup) {
        let checked = 0;
        Object.keys(formGroup.controls).forEach(key => {
            if (formControlKey === key) {
                const control = formGroup.controls[key];
                if (control.value === true) {
                    checked++;
                }
            }
        });
        if (checked < minRequired) {
            return {
                requireCheckboxToBeChecked: true
            };
        }
        return null;
    };
} */
