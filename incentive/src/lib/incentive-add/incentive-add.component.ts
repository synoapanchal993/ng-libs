import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { CustomModalComponent } from '@fpg-component/custom-modal';
import {
  focusInvalidControls,
  maxLength,
  positiveNumber,
  required,
  ValidateMinMax,
} from '@fpg-component/validation';
import * as moment from 'moment';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { forkJoin, Subject } from 'rxjs';
import { Subscription } from 'rxjs/internal/Subscription';
import { IncentiveFrequency } from '../incentive-frequency.enum';
import { labels } from '../incentive-labels';
import { MetricsDataType } from '../metrics-data-type.enum';
import { IncentiveSearchForm } from '../model/incentive-search-form.model';
import { IncentiveService } from '../service/incentive.service';
import {
  LocationGroup,
  LocationGroupService,
} from '../service/location-group.service';
import { Location, LocationService } from '../service/location.service';
import { Product, ProductService } from '../service/product.service';
import {
  TenantReport,
  TenantReportService,
} from '../service/tenant-report.service';
import { User, UserService } from '../service/user.service';
import { WeekDays } from '../weekdays.enum';
import {
  getIncentiveForm,
  getIntervalWithLockedInfo,
  IncentiveStepInfo,
  setlastDayOfCycle,
} from './incentive-step-info';
import { atLeastOneCheckboxCheckedValidator } from './incentive.validator';
import { Status } from '../status.enum';
import { IAMService } from '../service/iam.service';
import { SelectorOptions } from '@fpg-component/selector';
import { UtilService } from '../service/util.service';
import { CurrencySymbolPipe } from '../currencySymbolPipe.pipe';
import { FilterService } from '@fpg-component/filter';
import { DecimalPipe } from '@angular/common';
import { incentiveTooltipKeys } from './incentive-tooltipkeys';
import { AppTooltipService, TooltipEntity } from '@fpg-component/app-tooltip';
import { EventTracking, EventsJson, ModuleConstant, SubModuleConstant, ActionConstant, ActionOnConstant, EntityTypeConstant  } from 'event-framework';
@Component({
  templateUrl: './incentive-add.component.html',
  styleUrls: ['./incentive-add.component.scss'],
})
export class IncentiveAddComponent extends IncentiveStepInfo implements OnInit {
  @BlockUI()
  blockUI!: NgBlockUI;
  @ViewChild(CustomModalComponent)
  customModal!: CustomModalComponent;
  kid: string;
  addingProductControls = false;
  debugMode = false;
  industryId!: number;
  tenantId!: number;
  incentiveEditData: any;
  globalValidationMessage = `<label class='required'>*</label> - Mandatory fields.`;
  /* Calendar */
  currency: string = 'USD';
  firstDayOfMonth = moment([moment().year(), moment().month()]);
  initialDates = [
    this.firstDayOfMonth.toDate(),
    moment(this.firstDayOfMonth).endOf('month').toDate(),
  ];
  finalObject: any = {};
  disableControl = false;
  editabledControl = false;
  isLoaded = false;
  showUserWarning: boolean = true;
  stepForm: FormGroup[] = getIncentiveForm(this.formBuilder, this.initialDates);
  array: number[] = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
  ];
  locations: Location[] = [];
  locationGroups: LocationGroup[] = [];
  userList: User[] = [];
  userInAnotherPlan: any[] = [];
  warningMsg: string | null = null;
  warnOnOptionValues: any[] = [];
  isSaveDraftEnabled: boolean = false;
  incentiveStatus: string | null = null;
  productList: Product[] = [];
  productFetchForLocationGroupId!: number;
  selectedMajorKpi: TenantReport | undefined = undefined;
  iRKpi: TenantReport | null = null;
  isPrimaryARevenuKPI: Boolean = false;
  incentiveTenantReports: TenantReport[] = [];
  majorKpis: TenantReport[] = [];
  amountResultingKpi: TenantReport[] = [];
  quantityResultingKpi: TenantReport[] = [];
  kickerKpis: TenantReport[] = [];
  emptyFormGroupArray: AbstractControl[] = [];
  lastDayOfCycle: WeekDays = WeekDays.Sunday;
  firstDayOfCycleSub!: Subscription;
  kickerSub!: Subscription;
  cycleIntervals: any = {};
  findByIdObs = new Subject<boolean>();
  locationObs = new Subject<boolean>();
  kpiObs = new Subject<boolean>();
  userObs = new Subject<boolean>();
  tierObs = new Subject<boolean>();
  locationTierObs = new Subject<boolean>();
  productObs = new Subject<boolean>();
  productThresholdVal: boolean = false;
  productThresholdObs = new Subject<boolean>();
  thresholdKPI = [
    { intId: 0, name: 'as flat' },
    { intId: 1, name: 'per Upsell' },
  ];
  tooltipEntity: string = '';
  tooltipInformation: string  = 'information';
  tooltipWizardEntity: string = '';
  wizardKey = '';
  stepCancel: any;
  stepContinue: any;
  stepFinish: any
  stepSkip: any;
  stepPrev: any;
  draftButtonTooltip: any;


  canEditUserProfile: boolean = false;
  get weekDays() {
    return WeekDays;
  }
  get incentiveFrequency() {
    return IncentiveFrequency;
  }
  get metricsDataType() {
    return MetricsDataType;
  }
  get labels() {
    return labels;
  }
  warningUsersNotInGroup: string = '';
  baseUrl: any;
  currTimestamp!: number;
  profileUrl: string = '';
  userOptions: SelectorOptions = {
    items: [],
    bindLabel: '',
    bindValue: '',
    tooltip: '',
    bindAvtar: '',
  };
  productOptions: SelectorOptions = {
    items: [],
    bindLabel: '',
    bindValue: '',
    tooltip: '',
    bindAvtar: '',
  };
  payoutMaxMessage: string = '';
  isLocationChanged: boolean = false;
  warnUserIdsListNotInGroup: any[] = [];
  storedFilterId: string = 'incentive-search';
  incentiveActiveStatus: string = '';
  constructor(
    public iamservice: IAMService,
    private incentiveService: IncentiveService,
    private tenantReportService: TenantReportService,
    private locationService: LocationService,
    private locationGroupService: LocationGroupService,
    private userService: UserService,
    private productService: ProductService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private el: ElementRef,
    private utilService: UtilService,
    private filterService: FilterService,
    private currencySymbolPipe: CurrencySymbolPipe,
    private decimalPipe: DecimalPipe,
    private eventTracking: EventTracking,
  ) {
    super();
    this.wizardKey = "incentive.incentiveForm.step";
    this.industryId = iamservice.getIndustry();
    this.tenantId = iamservice.getTenant()?.intId;
    this.kid = route.snapshot.params['kid'];
    this.baseUrl = this.incentiveService.getMicroServicesBase();
    this.industryId = this.iamservice.getIndustry();
    this.currTimestamp = moment().toDate().getTime();
    this.profileUrl =
      this.baseUrl +
      '/account/user/avatar/' +
      this.industryId +
      '/mappingField' +
      '?isThumbnail=false&timestamp=' +
      this.currTimestamp;
    this.tooltipEntity = TooltipEntity.getTooltipEnum(7);
  }
  ngOnInit(): void {
    this.setFormValueForEdit();
    this.findById();
    this.getKpis();
    this.getLocations();
    this.onChanges();
    if (!this.kid) {
      this.initTiers(3);
      this.setButtonTooltip(this.step);
    }
    this.canEditUserProfile = this.iamservice.isUrlAccessible(
      '/configuration/user/edit',
      true
    );
    // this.cycleIntervals = getIntervalWithLockedInfo(this.getDateRange()[0], this.getDateRange()[1], this.stepForm[1].get('frequency')?.value, null, this.stepForm[1].get('payoutDateForIncentive')!.value, this.stepForm[1].get('firstDayOfCycle')?.value, this.lastDayOfCycle, false);
  }
  setFormValueForEdit() {
    forkJoin(this.findByIdObs, this.locationObs, this.kpiObs).subscribe((val) => {
      // console.log(val);

      if (this.incentiveEditData) {
        this.currency = this.incentiveEditData.locationCurrency;
        this.clickableStep = true;
        const hasLockedPayout = this.incentiveEditData.incentivePlanPayouts?.find(
          (x: any) => x.locked === true
        );
        this.disableControl = hasLockedPayout ? true : false;
        this.steps.forEach((element) => {
          if (this.disableControl) {
            element.wizardInfo =
              '<label class="required mb-0"><small class="fas fa-lock"></small> </label> - Not editable as incentive is activated. If you want to change not editable fields, please deactivate this incentive plan and create new plan.';
          }
          if (element.title == 'Summary') {
            element.contButton = 'Save';
          }
        });
        this.incentiveActiveStatus = this.incentiveEditData.activeStatus;
        /* Step 1 */
        this.stepForm[0].patchValue({
          name: this.incentiveEditData.name,
          location:
            '' +
            this.incentiveEditData.incentivePlanEntitys[0].tenantLocationId,
          department:
            '' + this.incentiveEditData.incentivePlanEntitys[0].locationGroupId,
        });
        this.finalObject.activeStatus = this.incentiveEditData.activeStatus;
        /* Step 2 */
        this.stepForm[2].patchValue({
          planMetricType:
            '' + MetricsDataType[this.incentiveEditData.planMetricType],
        });
        const userIds: any[] = this.incentiveEditData?.incentivePlanEntitys.map(
          (x: any) => '' + x.userId
        );
        this.userObs.subscribe((val) => {
          // console.log("this.userObs ==> " + val);

          if (
            this.incentiveEditData?.userNotInGroupNow &&
            this.incentiveEditData?.userNotInGroupNow.length > 0
          ) {
            let userNames = '';
            let SEPRATOR = '';
            this.incentiveEditData?.userNotInGroupNow.forEach((o: any) => {
              this.warnUserIdsListNotInGroup.push(o.intId);
              this.userOptions.items.push({
                intId: o.intId,
                fullName: o.name,
              });
              this.users.push(
                this.formBuilder.group({
                  userDetail: {
                    intId: o.intId,
                    fullName: o.name,
                  },
                  userSelected: true,
                })
              );
              userNames = userNames + SEPRATOR + o.name;
              SEPRATOR = ', ';
            });
            this.warnOnOptionValues.push.apply(
              this.warnOnOptionValues,
              this.warnUserIdsListNotInGroup
            );
            this.userOptions.items.sort((a, b) =>
              a.fullName.localeCompare(b.fullName)
            );
            this.warningUsersNotInGroup =
              `Agent ` +
              userNames +
              ` has a role updated or not a part of selected department.`;
          }

          this.users.controls.forEach((formGroup, i) => {
            const userId = formGroup.get('userDetail')?.value?.intId;
            if (userIds.indexOf('' + userId) > -1) {
              formGroup.get('userSelected')?.patchValue(true);
            }
          });
          this.userOptions.items.forEach((item) => {
            if (userIds.indexOf('' + item.intId) > -1) {
              item.isSelected = true;
            }
          });
          this.findSimilarIncentive();
        });
        /* Step 3 */
        this.stepForm[1].patchValue({
          timeframe: [
            moment(this.incentiveEditData.fromDate, 'YYYY-MM-DD').toDate(),
            moment(this.incentiveEditData.toDate, 'YYYY-MM-DD').toDate(),
          ],
          frequency: '' + IncentiveFrequency[this.incentiveEditData.frequency],
          payoutDateForIncentive:
            '' + this.incentiveEditData.payoutDateForIncentive,
          firstDayOfCycle: this.incentiveEditData.firstDayOfCycle
            ? '' + WeekDays[this.incentiveEditData.firstDayOfCycle]
            : '' + IncentiveFrequency.Monthly,
        });

        /* Step 4 */
        this.stepForm[3].patchValue({
          primaryKpi: '' + this.incentiveEditData.tenantReportId,
          payoutInPercentage: this.incentiveEditData.majorPayoutInPercentage
            ? '1'
            : '0',
          multiplier: this.incentiveEditData.majorKpiMultiplier ? '1' : '0',
          multiplierKpi: '' +this.incentiveEditData.majorKpiMultiplierKpi,
        });
        this.isPrimaryARevenuKPI = this.incentiveEditData.majorKpiDetail.revenueKpi;
        if (this.incentiveEditData.majorPayoutInPercentage && !this.isPrimaryARevenuKPI && this.utilService.blank(this.incentiveEditData.majorKpiMultiplierKpi)){
          this.stepForm[3].get('multiplier')?.patchValue('1');
          this.stepForm[3].get('multiplierKpi')?.patchValue('' +this.iRKpi?.intId);
        }
        
        /* Step 5 */
        this.stepForm[4].patchValue({
          noOfTiers: '' + this.incentiveEditData.noOfTiers,
          /* locationTierQualify: this.incentiveEditData.isLocationBased ? '1' : '0', */
          supertier: this.incentiveEditData.superTierStartFrom > 0 ? '1' : '-1',
        });

        this.tierObs.subscribe((val) => {
          // console.log("this.tierObs ==> " + val);
          this.tiers.controls.forEach((formGroup, i) => {
            const tier = this.incentiveEditData.incentivePlanTier[i];
            formGroup.get('tierName')?.patchValue(tier.tierName);
            formGroup.get('payout')?.patchValue(tier.tierPayout);
            formGroup.get('threshold')?.patchValue(tier.tierThreshold);
          });
        });
        /* step-6 */
        /* Location tier code hide */
        /*  this.stepForm[5].patchValue({
          noOfLocationTiers: this.incentiveEditData.noOfLocationTiers,
          locationTierQualify: this.incentiveEditData.isLocationBased ? '1' : '0',
        });
        if (this.incentiveEditData.isLocationBased) {
          this.stepForm[5].patchValue({locationBasedCommon: this.incentiveEditData.isLocationBasedCommon ? '1' : '0', });
        }
        this.locationTierObs.subscribe((val) => {
          if (this.incentiveEditData.isLocationBased) {
          if (this.incentiveEditData.isLocationBasedCommon) {
            this.locationTiers.controls.forEach((formGroup, i) => {
              const tier = this.incentiveEditData.incentivePlanTier[i];
              if (tier.locationTierThreshold !== null) {
                formGroup.get('locationTierName')?.patchValue(tier.tierName);
                formGroup.get('locationTierpayout')?.patchValue(tier.tierPayout);
                formGroup.get('locationTierThreshold')?.patchValue(tier.locationTierThreshold);
                formGroup.get('locationTierEnabled')?.patchValue(tier.locationTierEnabled ? '1' : '0');
              }
            });
          }
          else {
            let fromDate = '';
            let j = 0;
            let i = 0;
            this.incentiveEditData.monthlyLocationTiers.forEach((tier: any) => {
              if (tier.tierThreshold !== null){
              if (tier.fromDate !== fromDate) {
                i = 0;
                this.tierThresholds(j)?.controls[i]?.get('tierThreshold')?.patchValue(tier.tierThreshold);
                this.tierThresholds(j)?.controls[i]?.
                get('isLocationTierEnabled')?.patchValue(tier.isLocationTierEnabled ? '1' : '0');
                j = j + 1;
                i = i + 1;
              } else {
                let k = j - 1;
                this.tierThresholds(k)?.controls[i]?.get('tierThreshold')?.patchValue(tier.tierThreshold);
                this.tierThresholds(k)?.controls[i]?.
                get('isLocationTierEnabled')?.patchValue(tier.isLocationTierEnabled ? '1' : '0');
                i = i + 1;
              }
              fromDate = tier.fromDate;
            }
            });
            console.log(this.monthlyLocationTiers.controls[0]);
          }
        }
        });
 */
        let productThresholds = this.incentiveEditData?.incentivePlanProductConfigs?.find(
          (x: any) => x.threshold > 0
        );
        let productIds = this.incentiveEditData?.incentivePlanProductConfigs?.map(
          (x: any) => '' + x.locationGroupProductId
        );

        /* Step 7 */
        this.stepForm[5].patchValue({
          productSpecific: this.incentiveEditData.forProduct ? '1' : '0',
          productThreshold: productThresholds ? '1' : '0',
        });
        this.productObs.subscribe((val) => {
          // console.log("this.productObs ==> " + val);
          // console.log('Products');
          // console.log(this.products);
          this.products.controls.forEach((formGroup, i) => {
            const productId = formGroup.get('productDetail')?.value?.intId;
            if (productIds.indexOf(productId) > -1) {
              formGroup.get('productSelected')?.patchValue(true);
            }
          });
          this.productOptions.items.forEach((item) => {
            if (productIds.indexOf('' + item.intId) > -1) {
              item.isSelected = true;
            }
          });
        });
        this.productThresholdObs.subscribe((val) => {
          // console.log("this.productThresholdObs ==> " + val);
          if (productThresholds) {
            this.productThresholds.controls.forEach((formGroup, i) => {
              const productId = formGroup.get('productDetail')?.value?.intId;
              const prod = this.incentiveEditData?.incentivePlanProductConfigs?.find(
                (x: any) => x.locationGroupProductId == productId
              );
              // console.log(prod);
              if (prod) {
                formGroup.get('threshold')?.patchValue('' + prod.threshold);
                formGroup
                  .get('thresholdInPercentage')
                  ?.patchValue(prod.isPercentage ? '1' : '0');
                //thresholdPerKpi
              }
            });
          }
        });
        /* Step 8 */
        this.stepForm[6].patchValue({
          kicker: this.incentiveEditData.majorKpiKicker ? '1' : '0',
          maxPayout: this.incentiveEditData.maxIncentivePaidOut > 0 ? '1' : '0',
        });
        if (this.stepForm[6].contains('maxIncentivePaidOut')) {
          this.stepForm[6]
            .get('maxIncentivePaidOut')
            ?.patchValue(this.incentiveEditData.maxIncentivePaidOut);
        }
        if (this.stepForm[6].contains('kickerKpi')) {
          this.stepForm[6]
            .get('kickerKpi')
            ?.patchValue('' + this.incentiveEditData.majorKpiKickerKpi);
        }
        if (this.stepForm[6].contains('kickerMin')) {
          this.stepForm[6]
            .get('kickerMin')
            ?.patchValue(
              '' + this.incentiveEditData.majorKpiKickerQualifyKpiValueFrom
            );
        }
        if (this.stepForm[6].contains('kickerMax')) {
          this.stepForm[6]
            .get('kickerMax')
            ?.patchValue(
              '' + this.incentiveEditData.majorKpiKickerQualifyKpiValueTo
            );
        }
        if (this.stepForm[6].contains('kickerPayoutInPercentage')) {
          this.stepForm[6]
            .get('kickerPayoutInPercentage')
            ?.patchValue(
              this.incentiveEditData.majorKpiKickerValueInPercentage ? '1' : '0'
            );
        }
        if (this.stepForm[6].contains('kickerPayout')) {
          this.stepForm[6]
            .get('kickerPayout')
            ?.patchValue(this.incentiveEditData.majorKpiKickerValue);
        }
      }
      if (this.incentiveEditData.activeStatus === 'Draft') {
        this.editabledControl = true;
        this.steps.forEach((element) => {
          if (element.title == 'Summary') {
            element.contButton = 'Activate Your Incentive Plan';
          }
        });
        setTimeout(() => {
          this.validateAllStep(this.incentiveEditData.stepNumber);
        }, 700);
      }
      this.blockUI.stop();
      // this.directSummaryPage()
    });
  }
  /* Agent Tier Table Control  */
  get tiers(): FormArray {
    return this.stepForm[4].get('tiers') as FormArray;
  }
  newTier(payout: number, threshold: number): FormGroup {
    let payoutInPercentage =
      this.stepForm[3].controls['payoutInPercentage'].value === '1'
        ? true
        : false;
    if (payoutInPercentage) {
      return this.formBuilder.group({
        tierNo: this.tiers.length + 1,
        tierName: [
          'tier' + (this.tiers.length + 1),
          [Validators.required, maxLength(20)],
        ],
        payout: [
          payout,
          [
            Validators.required,
            positiveNumber('Enter valid positive numerical amount.'),
            this.max(100, 'Commission Payout should not be more than 100%.'),
          ],
        ],
        threshold: [
          threshold,
          [
            Validators.required,
            positiveNumber('Enter valid positive numerical amount.'),
            maxLength(12),
          ],
        ],
        locationTierEnabled: '1',
      });
    } else {
      return this.formBuilder.group({
        tierNo: this.tiers.length + 1,
        tierName: [
          'tier' + (this.tiers.length + 1),
          [Validators.required, maxLength(20)],
        ],
        payout: [
          '' + payout,
          [
            Validators.required,
            positiveNumber('Enter valid positive numerical amount.'),
            maxLength(12, 'More than 12 characters are not allowed.'),
          ],
        ],
        threshold: [
          '' + threshold,
          [
            Validators.required,
            positiveNumber('Enter valid positive numerical amount.'),
            maxLength(12),
          ],
        ],
        locationTierEnabled: '1',
      });
    }
  }
  initTiers(noOfTier: number): void {
    this.removeAllTier();
    let payout = 5;
    let threshold = 1000;
    for (let index = 0; index < noOfTier; index++) {
      this.addTier(payout, threshold);
      payout = payout + 5;
      threshold = threshold + 1000;
    }
    setTimeout(() => {
      this.tierObs.next(true);
      this.tierObs.complete();
    }, 500);
  }
  addTier(payout: number, threshold: number): void {
    this.tiers.push(this.newTier(payout, threshold));
  }
  removeTier(i: number): void {
    this.tiers.removeAt(i);
  }
  removeAllTier(): void {
    this.tiers.clear();
  }

  get users(): FormArray {
    return this.stepForm[2].get('users') as FormArray;
  }
  private addUserCheckboxes(): void {
    this.users.clear();
    this.userList.forEach((o, i) => {
      this.users.push(
        this.formBuilder.group({
          userDetail: o,
          userSelected: false,
        })
      );
    });
    this.userObs.next(true);
    this.userObs.complete();
    /* this.userOptions.items.forEach(item => {
      if (!item.isSelected){
        item.isSelected = false;
      }
    }); */
  }
  selectedUserItems(event: any[]): void {
    const userIds = event?.map((x: any) => {
      return x?.intId;
    });
    this.users.controls.forEach((formGroup, i) => {
      formGroup.get('userSelected')?.patchValue(false);
    });
    this.users.controls.forEach((formGroup, i) => {
      const userId = formGroup.get('userDetail')?.value?.intId;
      if (userIds.indexOf(userId) > -1) {
        formGroup.get('userSelected')?.patchValue(true);
      }
    });
    this.warnOnOptionValues = [];
    this.findUsersNotInDepartment();
    this.findSimilarIncentive();
  }

  get products(): FormArray {
    return this.stepForm[5].get('products') as FormArray;
  }
  private addProductCheckboxes(): void {
    this.addingProductControls = true;
    this.products.clear();
    this.productList.forEach((o, i) => {
      this.products.push(
        this.formBuilder.group({
          productDetail: o,
          productSelected: false,
        })
      );
    });
    this.addingProductControls = false;
    // console.log('New Product List....');

    this.productObs.next(true);
    this.productObs.complete();
    /* this.productOptions.items.forEach(item => {
      if (!item.isSelected){
        item.isSelected = false;
      }
    }); */
  }

  /* Product Threshold Table Control */
  get productThresholds(): FormArray {
    return this.stepForm[5].get('productThresholds') as FormArray;
  }
  newProductThreshold(productDetail: Product): FormGroup {
    return this.formBuilder.group({
      productDetail: productDetail,
      threshold: [
        '0.0',
        [
          Validators.required,
          positiveNumber('Enter valid positive numerical amount.'),
        ],
      ],
      thresholdInPercentage: '0',
      //thresholdPerKpi: '0',
    });
  }
  addProductThreshold(productDetail: Product): void {
    this.productThresholds.push(this.newProductThreshold(productDetail));
  }
  removeAllProductThreshold(): void {
    this.productThresholds.clear();
  }
  removeProductThreshold(i: number): void {
    this.productThresholds.removeAt(i);
  }
  initProductThreshold() {
    this.removeAllProductThreshold();
    this.stepForm[5].get('products')?.value.forEach((val: any, i: number) => {
      if (val?.productSelected === true) {
        // console.log(val?.productDetail?.intId + '---->' + (val?.productSelected === true));
        this.addProductThreshold(this.productList[i]);
      }
    });
    // console.log("initProductThreshold");
    this.productThresholdVal = !this.productThresholdVal;
    this.productThresholdObs.next(this.productThresholdVal);
  }

  addKickerControls() {
    this.stepForm[6].addControl('kickerKpi', new FormControl(null, required()));
    /* this.stepForm[6].addControl(
      'kickerMin',
      new FormControl(null, [required(), positiveNumber()])
    );
    this.stepForm[6].addControl(
      'kickerMax',
      new FormControl(null, [required(), positiveNumber()])
    );
    this.stepForm[6].addControl(
      'kickerPayoutInPercentage',
      new FormControl('1', required())
    );
    this.stepForm[6].addControl(
      'kickerPayout',
      new FormControl(null, [required(), positiveNumber()])
    ); */
  }
  removeKickerControls() {
    this.stepForm[6].removeControl('kickerKpi');
    // this.stepForm[6].removeControl('kickerMin');
    // this.stepForm[6].removeControl('kickerMax');
    // this.stepForm[6].removeControl('kickerPayoutInPercentage');
    // this.stepForm[6].removeControl('kickerPayout');
  }
  addKickerKpiControls() {
    this.stepForm[6].addControl(
      'kickerMin',
      new FormControl(null, [
        required(),
        positiveNumber('Enter valid positive numerical amount.'),
      ])
    );
    this.stepForm[6].addControl(
      'kickerMax',
      new FormControl(null, [
        required(),
        positiveNumber('Enter valid positive numerical amount.'),
      ])
    );
    this.stepForm[6].addControl(
      'kickerPayoutInPercentage',
      new FormControl('1', required())
    );
    this.stepForm[6].addControl(
      'kickerPayout',
      new FormControl(null, [
        required(),
        positiveNumber('Enter valid positive numerical amount.'),
        this.max(100, 'Kicker value should not be more than 100%.'),
      ])
    );
    this.stepForm[6].controls[
      'kickerPayoutInPercentage'
    ]!.valueChanges.subscribe((val) => {
      this.stepForm[6].get('kickerPayout')?.clearValidators();
      this.stepForm[6].get('kickerPayout')?.patchValue(null);
      if (val === '0') {
        this.stepForm[6]
          .get('kickerPayout')
          ?.setValidators([
            Validators.required,
            positiveNumber('Enter valid positive numerical amount.'),
          ]);
      } else {
        this.stepForm[6]
          .get('kickerPayout')
          ?.setValidators([
            Validators.required,
            positiveNumber('Enter valid positive numerical amount.'),
            this.max(100, 'Kicker value should not be more than 100%.'),
          ]);
      }
    });
  }
  removeKickerKpiControls() {
    this.stepForm[6].removeControl('kickerMin');
    this.stepForm[6].removeControl('kickerMax');
    this.stepForm[6].removeControl('kickerPayoutInPercentage');
    this.stepForm[6].removeControl('kickerPayout');
  }
  addMaxPayoutControls() {
    this.stepForm[6].addControl(
      'maxIncentivePaidOut',
      new FormControl(null, [
        required(),
        positiveNumber('Enter valid positive numerical amount.'),
      ])
    );
  }
  removeMaxPayoutControls() {
    this.stepForm[6].removeControl('maxIncentivePaidOut');
  }
  /* addHoldPayoutControls() {
    this.stepForm[6].addControl('holdPayoutAmount', new FormControl(null, [required(),positiveNumber()]));
    this.stepForm[6].addControl('holdPayoutInPercentage', new FormControl('1', [required()]));
    this.stepForm[6].addControl('holdPayoutDate', new FormControl(this.initialDates[0], [required()]));  
  }
  removeHoldPayoutControls() {
    this.stepForm[6].removeControl('holdPayoutAmount');
    this.stepForm[6].removeControl('holdPayoutInPercentage');
    this.stepForm[6].removeControl('holdPayoutDate');
  } */
  get locationTiers(): FormArray {
    return this.stepForm[5].get('locationTiers') as FormArray;
  }
  newLocationTier(tier: any, threshold: number): FormGroup {
    return this.formBuilder.group({
      locationTierName: [tier.tierName],
      locationTierPayout: [tier.payout],
      locationTierEnabled: '1',
      locationTierThreshold: [
        '' + threshold,
        [
          Validators.required,
          positiveNumber('Enter valid positive numerical amount.'),
          maxLength(12),
        ],
      ],
    });
  }
  initLocationTiers(): void {
    this.removeLocationAllTier();
    const tierInfo = this.stepForm[4].get('tiers')?.value;
    let threshold = 10000;
    tierInfo.forEach((tier: any) => {
      this.addLocationTiers(tier, threshold);
      threshold = threshold + 5000;
    });
    setTimeout(() => {
      this.locationTierObs.next(true);
      this.locationTierObs.complete();
    }, 500);
  }
  addLocationTiers(tier: any, threshold: number): void {
    this.locationTiers.push(this.newLocationTier(tier, threshold));
  }
  removeLocationTier(i: number): void {
    this.locationTiers.removeAt(i);
  }
  removeLocationAllTier(): void {
    this.locationTiers.clear();
  }

  get monthlyLocationTiers(): FormArray {
    return this.stepForm[5].get('monthlyLocationTiers') as FormArray;
  }
  newMonthlyLocationTier(interval: any): FormGroup {
    return this.formBuilder.group({
      year: [interval.year],
      month: [interval.monthNum],
      monthName: [interval.monthName],
      fromDate: [interval.fromDate],
      toDate: [interval.toDate],
      fromDateStr: [interval.fromDateStr],
      toDateStr: [interval.toDateStr],
      day: [interval.day],
      frequency: [interval.frequency],
      tierThresholds: this.formBuilder.array([]),
    });
  }
  initMonthlyLocationTiers(): void {
    this.removeMonthlyLocationAllTier();
    (<any[]>this.cycleIntervals?.intervals)?.forEach((interval, i) => {
      this.addMonthlyLocationTiers(interval);
      let tiers = this.stepForm[4].get('tiers')?.value;
      let threshold = 10000;
      this.removeTierThresholds(i);
      tiers.forEach((tier: any) => {
        this.addTierThresholds(threshold, i, tier);
        threshold = threshold + 5000;
      });
    });
    setTimeout(() => {
      this.locationTierObs.next(true);
      this.locationTierObs.complete();
    }, 500);
  }
  addMonthlyLocationTiers(interval: any): void {
    this.monthlyLocationTiers.push(this.newMonthlyLocationTier(interval));
  }
  removeMonthlyLocationTier(i: number): void {
    this.monthlyLocationTiers.removeAt(i);
  }
  removeMonthlyLocationAllTier(): void {
    this.monthlyLocationTiers.clear();
  }
  tierThresholds(i: number): FormArray {
    return (this.stepForm[5].get('monthlyLocationTiers') as FormArray).controls[
      i
    ]?.get('tierThresholds') as FormArray;
  }
  newTierThreshold(threshold: number, tier: any): FormGroup {
    return this.formBuilder.group({
      tierThreshold: [
        '' + threshold,
        [
          Validators.required,
          positiveNumber('Enter valid positive numerical amount.'),
          maxLength(12),
        ],
      ],
      isLocationTierEnabled: '1',
      tierNo: tier.tierNo,
      tierName: tier.tierName,
    });
  }
  addTierThresholds(threshold: number, i: number, tier: any): void {
    this.tierThresholds(i).push(this.newTierThreshold(threshold, tier));
  }
  removeTierThresholds(i: number): void {
    this.tierThresholds(i).clear();
  }
  addFirstDayOfCycle() {
    this.stepForm[1].addControl(
      'firstDayOfCycle',
      new FormControl('' + WeekDays.Sunday, required())
    );
  }
  getDateRange() {
    return this.stepForm[1].get('timeframe')!.value;
  }
  onChanges(): void {
    // console.log('onChanges');

    this.stepForm[0].get('location')!.valueChanges.subscribe((val) => {
      const location: Location | undefined = this.locations.find(
        (x) => x.intId == val
      );
      this.setLocationCurrency(location);
      this.getLocationGroups(val);
    });
    this.stepForm[0].get('department')!.valueChanges.subscribe((val) => {
      this.getUsers(this.tenantId, val);
      this.resetProductSelection();
    });
    this.stepForm[1].get('timeframe')!.valueChanges.subscribe((val: Date[]) => {
      if (this.kid && this.incentiveEditData) {
        this.cycleIntervals = getIntervalWithLockedInfo(
          moment(this.incentiveEditData.fromDate, 'YYYY-MM-DD').toDate(),
          moment(this.incentiveEditData.toDate, 'YYYY-MM-DD').toDate(),
          this.stepForm[1].get('frequency')?.value,
          this.incentiveEditData.incentivePlanPayouts,
          this.stepForm[1].get('payoutDateForIncentive')!.value,
          this.stepForm[1].get('firstDayOfCycle')?.value,
          this.lastDayOfCycle,
          true
        );
        if (val[1] < this.cycleIntervals.validEndDate) {
          this.stepForm[1].get('timeframe')!.setErrors({ invalidDate: true });
        } else {
          this.stepForm[1].get('timeframe')!.setErrors(null);
        }
      }
      const selectedUsers = this.stepForm[2]
        .get('users')
        ?.value.filter((x: any) => x.userSelected === true);
      if (selectedUsers && selectedUsers.length > 0) {
        this.cleanUserWarning();
        this.findUsersNotInDepartment();
        this.findSimilarIncentive();
      }
    });
    this.stepForm[1].get('frequency')!.valueChanges.subscribe((val) => {
      if (
        val !== '' + IncentiveFrequency.Monthly &&
        val !== '' + IncentiveFrequency.Semi_Monthly
      ) {
        this.addFirstDayOfCycle();
        this.lastDayOfCycle = setlastDayOfCycle(WeekDays.Sunday);
        if (this.firstDayOfCycleSub) {
          this.firstDayOfCycleSub.unsubscribe();
        }
        this.firstDayOfCycleSub = this.stepForm[1]
          .get('firstDayOfCycle')!
          .valueChanges.subscribe((val) => {
            this.lastDayOfCycle = setlastDayOfCycle(val);
            // this.cycleIntervals = getIntervalWithLockedInfo(this.getDateRange()[0], this.getDateRange()[1], this.stepForm[1].get('frequency')?.value, null, this.stepForm[1].get('payoutDateForIncentive')!.value, val, this.lastDayOfCycle, false);
          });
      } else {
        this.stepForm[1].removeControl('firstDayOfCycle');
      }
      // this.cycleIntervals = getIntervalWithLockedInfo(this.getDateRange()[0], this.getDateRange()[1], val, null, this.stepForm[1].get('payoutDateForIncentive')!.value, this.stepForm[1].get('firstDayOfCycle')?.value, this.lastDayOfCycle, false);
    });
    this.stepForm[1]
      .get('payoutDateForIncentive')!
      .valueChanges.subscribe((val) => {
        // this.cycleIntervals = getIntervalWithLockedInfo(this.getDateRange()[0], this.getDateRange()[1], this.stepForm[1].get('frequency')?.value, null, val, this.stepForm[1].get('firstDayOfCycle')?.value, this.lastDayOfCycle, false);
      });
    this.stepForm[3]
      .get('payoutInPercentage')!
      .valueChanges.subscribe((val) => {
        this.setMultiplierKpiControl(val == '1',!this.isPrimaryARevenuKPI);
        this.initTiers(3);
      });
    /* this.stepForm[3].get('multiplier')!.valueChanges.subscribe((val) => {
      if (val === '1') {
        // this.stepForm[3].get('multiplierKpi')?.setValidators(required('This is a required field.'));
        this.stepForm[3].get('multiplierKpi')?.updateValueAndValidity();
      } else {
        this.stepForm[3].get('multiplierKpi')?.clearValidators();
        this.stepForm[3].get('multiplierKpi')?.updateValueAndValidity();
      }
    }); */
    /* this.stepForm[3].get('multiplierKpi')!.valueChanges.subscribe((val) => {
      console.log('multiplierKpi==>'+val);
      if(val){
        this.stepForm[3].get('multiplier')?.patchValue('1');
      }else{
        this.stepForm[3].get('multiplier')?.patchValue('0');
      }
    }); */
    this.stepForm[3].get('primaryKpi')!.valueChanges.subscribe((val) => {
      /* this.getLocationGroups(val); */
      this.selectedMajorKpi = this.majorKpis.find((x) => x.intId === val);
      this.isPrimaryARevenuKPI = (this.selectedMajorKpi?.isMoney=== true && this.selectedMajorKpi?.isSummable=== true);
      this.setMultiplierKpiControl(this.stepForm[3].get('payoutInPercentage')?.value == '1',!this.isPrimaryARevenuKPI);
    });
    this.stepForm[4].get('noOfTiers')!.valueChanges.subscribe((val) => {
      this.initTiers(parseInt(val));
      //this.initLocationTiers();
      //this.initMonthlyLocationTiers();
    });
    /* Location tier code hide */
    /* this.stepForm[4].get('tiers')!.valueChanges.subscribe((val) => {
      this.tiers.controls.forEach((formGroup, i) => {
        this.locationTiers?.controls[i]?.get('locationTierPayout')?.patchValue(formGroup.get('payout')?.value);
      });
    });
    this.stepForm[5].get('locationTierQualify')!
      .valueChanges.subscribe((val) => {
        if (val == 0) {
          this.locationTiers.clear();
          this.monthlyLocationTiers.clear();
        } else if (val == 1) {
          this.initLocationTiers();
          this.initMonthlyLocationTiers();
        }
      });
    this.stepForm[5].get('locationBasedCommon')!
    .valueChanges.subscribe((val) => {
        if (val == 0) {
          this.initMonthlyLocationTiers();
        } else if (val == 1) {
          this.initLocationTiers();
        }
    }); */
    this.stepForm[5].get('productSrch')!.valueChanges.subscribe((val) => {
      if (this.stepForm[5].get('productThreshold')?.value === '1') {
        // console.log(3);
        this.initProductThreshold();
      }
    });
    this.stepForm[5].get('productSpecific')!.valueChanges.subscribe((val) => {
      // not to call again if productThreshold was already selected
      if (this.stepForm[5].get('productThreshold')?.value !== '1') {
        this.getProducts(this.stepForm[0].get('department')?.value);
        this.productValidation();
      }
    });
    this.stepForm[5].get('productThreshold')!.valueChanges.subscribe((val) => {
      // not to call again if productSpecific was already selected
      if (this.stepForm[5].get('productSpecific')?.value !== '1') {
        this.getProducts(this.stepForm[0].get('department')?.value);
        this.productValidation();
      }
      // console.log(1);
      this.initProductThreshold();
    });
    this.stepForm[5].get('products')!.valueChanges.subscribe((val) => {
      if (!this.addingProductControls) {
        // console.log(2);
        this.initProductThreshold();
      }
    });
    this.stepForm[5].get('productThresholds')!.valueChanges.subscribe((val) => {
      this.productThresholds.controls.forEach((formGroup, i) => {
        formGroup.get('threshold')?.clearValidators();
        if (formGroup.get('thresholdInPercentage')?.value === '0') {
          formGroup
            .get('threshold')
            ?.setValidators([
              Validators.required,
              positiveNumber('Enter valid positive numerical amount.'),
            ]);
        } else {
          formGroup
            .get('threshold')
            ?.setValidators([
              Validators.required,
              positiveNumber('Enter valid positive numerical amount.'),
              this.max(100, 'Product Threshold should not be more than 100%.'),
            ]);
        }
      });
    });
    this.stepForm[6].get('kicker')!.valueChanges.subscribe((val) => {
      if (this.kickerSub) {
        this.kickerSub.unsubscribe();
      }
      if (val === '1') {
        this.addKickerControls();
        this.kickerSub = this.stepForm[6]
          .get('kickerKpi')!
          .valueChanges.subscribe((val) => {
            if (val > 0) {
              this.addKickerKpiControls();
            } else {
              this.removeKickerKpiControls();
            }
            let majorKpiKickerKpiObj = this.incentiveTenantReports.find(
              (x) => x.intId === val
            );
            /* Delete */
            let currCode: any = this.currencySymbolPipe.transform(
              this.finalObject.currency
            );
            this.finalObject.majorKpiKickerDetail = {
              name: majorKpiKickerKpiObj?.name,
              kpiPrepand:
                majorKpiKickerKpiObj?.isMoney === true &&
                majorKpiKickerKpiObj?.unit === null
                  ? currCode
                  : '',
              kpiAppand:
                majorKpiKickerKpiObj?.isMoney === false &&
                majorKpiKickerKpiObj?.unit !== null
                  ? majorKpiKickerKpiObj.unit
                  : '',
            };
          });
      } else {
        this.removeKickerControls();
        this.removeKickerKpiControls();
      }
    });

    this.stepForm[6].get('maxPayout')!.valueChanges.subscribe((val) => {
      if (val === '1') {
        this.addMaxPayoutControls();
      } else {
        this.removeMaxPayoutControls();
      }
    });
    /*  this.stepForm[6].get('holdPayout')!.valueChanges.subscribe((val) => {
       if (val === '1') {
         this.addHoldPayoutControls();
       } else {
         this.removeHoldPayoutControls();
       }
     }); */
  }
  setMultiplierKpiControl(isPercentage: boolean, isPrimaryANonRevenuKPI: boolean){
    this.stepForm[3].get('multiplierKpi')?.setValue(null);
    if(isPercentage && isPrimaryANonRevenuKPI){
      this.stepForm[3].get('multiplierKpi')?.setValidators(required('This is a required field.'));
      this.stepForm[3].get('multiplierKpi')?.patchValue('' +this.iRKpi?.intId);
      this.stepForm[3].get('multiplier')?.patchValue('1');
    }else if(isPercentage && !isPrimaryANonRevenuKPI){
      this.stepForm[3].get('multiplierKpi')?.patchValue(this.stepForm[3].get('primaryKpi')?.value);
      this.stepForm[3].get('multiplier')?.patchValue('1');
    }else{
      this.stepForm[3].get('multiplierKpi')?.setValidators(null);
      this.stepForm[3].get('multiplier')?.patchValue('0');
    }
    this.stepForm[3].get('multiplierKpi')?.updateValueAndValidity();
  }
  resetProductSelection() {
    this.stepForm[5].get('productSpecific')!.patchValue('0');
    this.stepForm[5].get('productThreshold')!.patchValue('0');
  }
  productValidation() {
    if (
      this.stepForm[5].get('productSpecific')!.value === '1' ||
      this.stepForm[5].get('productThreshold')!.value === '1'
    ) {
      this.stepForm[5].setValidators(
        atLeastOneCheckboxCheckedValidator(1, 'products', 'productSelected')
      );
      this.stepForm[5]?.updateValueAndValidity();
    } else {
      this.stepForm[5].setValidators(null);
      this.stepForm[5]?.updateValueAndValidity();
    }
  }
  jumpTo(moveStep: number) {
    this.blockUI.start('Loading...');
    if (this.step > moveStep) {
      for (let i = this.step; i > moveStep; i--) {
        // console.log('Move ::' + i);
        this.prev();
      }
    } else {
      for (let i = this.step; i < moveStep; i++) {
        // console.log('Move ::' + i);
        this.next();
      }
    }
    this.blockUI.stop();
    // this.step = moveStep;
  }
  goto(moveStep: number) {
    this.step = moveStep;
    console.log('go to called:', this.step)
    this.setButtonTooltip( this.step);
    /* this.fromSummary = true; */
  }
  cancel() {
    let modalData = {
      modalClassName: 'modal-md',
      width: '100%',
      type: 'Confirm',
      title: 'Confirm',
      text: 'Plan has some modification, Are you sure you want to cancel? ',
      /* "data": userData, */
      buttons: [
        {
          text: 'Yes',
          value: true,
          visible: true,
          className: 'btn-primary',
          closeModal: true,
          onClick: 'confirmCancel',
        },
        {
          text: 'No',
          value: true,
          visible: true,
          className: 'btn-secondary',
          closeModal: true,
          onClick: 'cancel',
        },
      ],
      closeOnClickOutside: false,
      closeOnEsc: true,
    };
    let dirtyForm = false;
    for (let i = 0; i < this.steps.length - 1; i++) {
      if (this.stepForm[i].dirty) {
        dirtyForm = true;
      }
    }
    if (dirtyForm) {
      this.customModal.open(modalData);
    } else {
      this.router.navigate(['incentive']);
    }
  }
  prev() {
    /* if(this.fromSummary){
      this.fromSummary=false;
      this.step1Submitt(this.stepForm[this.step - 1]);
      // move to last summary step
      this.step = this.steps.length;
    }else{ */
    this.step = this.step - 1;
    this.setButtonTooltip( this.step);
    /* } */
  }
  skip() {
    let modalData = {
      modalClassName: 'modal-md',
      width: '100%',
      type: 'Confirm',
      title: 'Confirm',
      text: 'Step has some modification, Are you sure you want to skip? ',
      /* "data": userData, */
      buttons: [
        {
          text: 'Yes',
          value: true,
          visible: true,
          className: 'btn-primary',
          closeModal: true,
          onClick: 'confirm',
        },
        {
          text: 'No',
          value: true,
          visible: true,
          className: 'btn-secondary',
          closeModal: true,
          onClick: 'cancel',
        },
      ],
      closeOnClickOutside: false,
      closeOnEsc: true,
    };

    if (this.stepForm[this.step - 1].dirty) {
      this.customModal.open(modalData);
      /* if (confirm("Modified fields on the step will be reset, Are you sure you want to skip?")) {
        this.step = this.step + 1;
      } */
    } else if (this.stepForm[this.step - 1].valid) {
      this.formSumbittion(false);
      this.step = this.step + 1;
      this.setButtonTooltip( this.step);
    }
  }
  /** To handle modal popup click event  */
  modalClickEvent(event: any) {
    let action = event.event;
    let data = event.data;
    if (action == 'confirm') {
      this.customModal.close();
      this.step = this.step + 1;
    } else if (action == 'confirmCancel') {
      this.customModal.close();
      this.router.navigate(['incentive']);
    } else if (action == 'cancel') {
      this.customModal.close();
    } else {
      this.customModal.close();
    }
  }
  next() {
    /* if(this.fromSummary){
      this.fromSummary=false;
    } */
    // stepForm starts with 0
    // Step counter starts with 1
    if (this.step < this.steps.length) {
      if (this.step == 3) {
        this.stepForm[this.step - 1].get('users')?.markAsDirty();
      }
      if (this.step == 6) {
        this.stepForm[this.step - 1].get('products')?.markAsDirty();
      }
      if (this.stepForm[this.step - 1] && this.stepForm[this.step - 1].valid) {
        this.formSumbittion(false);
        this.step = this.step + 1;
        this.setButtonTooltip( this.step);
        // console.log('Moving next ::'+this.step);
      } else {
        focusInvalidControls(this.stepForm[this.step - 1], this.el);
      }
      // console.log(this.finalObject);
    } else if (this.step === this.steps.length) {
      this.setButtonTooltip( this.step);
      this.save(false);
    }

  }
  directSummaryPage() {
    while (this.step < this.steps.length) {
      if (this.stepForm[this.step - 1] && this.stepForm[this.step - 1].valid) {
        this.formSumbittion(false);
        this.step = this.step + 1;
      } else {
        focusInvalidControls(this.stepForm[this.step - 1], this.el);
        return;
      }
      // console.log(this.finalObject);
    }
  }
  formSumbittion(isDraftMode: boolean) {
    switch (this.step - 1) {
      case 0:
        this.step1Submitt(this.stepForm[this.step - 1], isDraftMode);
        break;
      case 1:
        this.step2Submitt(this.stepForm[this.step - 1], isDraftMode);
        break;
      case 2:
        this.step3Submitt(this.stepForm[this.step - 1], isDraftMode);
        break;
      case 3:
        this.step4Submitt(this.stepForm[this.step - 1]);
        break;
      case 4:
        this.step5Submitt(this.stepForm[this.step - 1]);
        break;
      /* location tier Code */
      /* case 5:
        this.step6Submitt(this.stepForm[this.step - 1]);
        break; */
      case 5:
        this.step7Submitt(this.stepForm[this.step - 1]);
        break;
      case 6:
        this.step8Submitt(this.stepForm[this.step - 1]);
        break;
      default:
        this.save(false);
        break;
    }
  }

  save(isDraftMode: boolean) {
    this.blockUI.start('Loading...');
    if (this.kid) {
      this.finalObject.kid = this.kid;
      // Feature not on UI, Setting same fetched from edit
      this.finalObject.note = this.incentiveEditData.note;
      this.finalObject.incentivePlanMinorKpis = this.incentiveEditData.incentivePlanMinorKpis;
      this.finalObject.majorKpiQualify = this.incentiveEditData.majorKpiQualify;
      this.finalObject.majorKpiQualifyKpi = this.incentiveEditData.majorKpiQualifyKpi;
      this.finalObject.majorKpiQualifyKpiValueFrom = this.incentiveEditData.majorKpiQualifyKpiValueFrom;
      this.finalObject.majorKpiQualifyKpiValueTo = this.incentiveEditData.majorKpiQualifyKpiValueTo;
      this.finalObject.holdPayoutInPercentage = this.incentiveEditData.holdPayoutInPercentage;
      this.finalObject.holdPayoutAmount = this.incentiveEditData.holdPayoutAmount;
      this.finalObject.holdPayoutDate = this.incentiveEditData.holdPayoutDate;
      this.finalObject.configureHoldAmount = this.incentiveEditData.configureHoldAmount;
      if (!isDraftMode && this.incentiveEditData.activeStatus == 'Draft') {
        this.finalObject.activeStatus = 'Normal';
      }
      if (isDraftMode) {
        this.finalObject.stepNumber = this.step;
        /*  if (this.step < 6) { Location tier data hide
          this.finalObject.isLocationBased = false;
          } */
        if (this.step < 5) {
          this.finalObject.noOfTiers = 3;
        }
      } else {
        this.finalObject.isLocationBased = this.incentiveEditData.isLocationBased;
      }
      /* REmove code when location tier data show */
      this.finalObject.isLocationBased = this.incentiveEditData.isLocationBased;
      this.finalObject.noOfLocationTiers = this.incentiveEditData.noOfLocationTiers;
      this.finalObject.monthlyLocationTiers = this.incentiveEditData.monthlyLocationTiers;
      this.finalObject.isLocationBasedCommon = this.incentiveEditData.isLocationBasedCommon;
    } else {
      if (isDraftMode) {
        this.finalObject.activeStatus = 'Draft';
        /* if (this.step < 6) { Location tier data hide
          this.finalObject.isLocationBased = false;
          } */
        if (this.step < 5) {
          this.finalObject.noOfTiers = 3;
        }
        this.finalObject.stepNumber = this.step;
      } else {
        this.finalObject.activeStatus = 'Normal';
      }
      /* remove when location tier data display start */
      this.finalObject.isLocationBased = false;
      this.finalObject.isLocationBasedCommon = false;
      this.finalObject.noOfLocationTiers = 0;
      /* remove when location tier data display start */

      this.finalObject.majorKpiQualify = false;
      this.finalObject.holdPayoutInPercentage = true;
      this.finalObject.configureHoldAmount = false;
    }
    /*  delete this.finalObject.locationId;
     delete this.finalObject.locationName;
     delete this.finalObject.locationGroupId;
     delete this.finalObject.locationGroupName
     delete this.finalObject.userNames;
     delete this.finalObject.majorKpiName;
     delete this.finalObject.majorKpiPrepand;
     delete this.finalObject.majorKpiAppand;
     delete this.finalObject.majorKpiMultiplierKpiName;
     delete this.finalObject.productNames;
     delete this.finalObject.majorKpiKickerKpiName;
     delete this.finalObject.majorKpiKickerKpiPrepand;
     delete this.finalObject.majorKpiKickerKpiAppand; */

    this.finalObject.incentivePlanPayouts = [];
    (<any[]>this.cycleIntervals?.intervals)?.forEach((interval) => {
      this.finalObject.incentivePlanPayouts.push({
        fromDate: moment(interval.fromDate).format('YYYY-MM-DD'),
        toDate: moment(interval.toDate).format('YYYY-MM-DD'),
        payoutDate: moment(interval.payoutDate).format('YYYY-MM-DD'),
        locked: interval.locked ? interval.locked : false,
        dirty: true,
      });
    });
    delete this.finalObject.currencySymbol;
    delete this.finalObject.selectedUsers;
    const queryString = {
      query: `mutation{saveOrUpdate(incentivePlan:${this.incentiveService.stringify(
        this.finalObject
      )}){${this.incentiveService.incentiveFields}}}`,
    };
    // console.log(queryString);
    this.incentiveService.gql(queryString).subscribe((response: any) => {
      this.blockUI.stop();
      if (response && response.errors) {
        const error = response.errors[0].message;
        this.openModal(
          'Oops! Something went wrong. Please try again later.',
          false,
          false,
          true
        );
        console.log(error);
      } else if (response.data && response.data.saveOrUpdate) {
        let action;
        this.changeFilter(response.data.saveOrUpdate);
        if (isDraftMode) {
          action = ActionConstant.RECORD_DRAFT;
        } else {
          action = this.kid
           ? this.incentiveActiveStatus === 'Draft'
              ? ActionConstant.RECORD_ADD
              : ActionConstant.RECORD_UPDATE
            : ActionConstant.RECORD_ADD;
        }
        //const action = this.kid ? UPDATE_INCENTIVE : ADD_INCENTIVE;
        const entityId = this.kid ? this.kid : response.data.saveOrUpdate.kid;
        const eventData = new EventsJson();
        eventData.module = ModuleConstant.INCENTIVE_PLAN;
        eventData.subModule = SubModuleConstant.INCENTIVE_WIZARD;
        eventData.action = action;
        eventData.actionOn = ActionOnConstant.INCENTIVE_PLAN;
        eventData.entityType = EntityTypeConstant.INCENTIVE_PLAN;
        eventData.entityId = entityId;
        this.eventTracking.createEvent(eventData);
        this.router.navigate(['incentive']);
      }
    });
  }

  step1Submitt(formGroup: FormGroup, isDraftMode: boolean) {
    this.finalObject.name = formGroup.get('name')?.value;
    /* Delete */
    this.finalObject.locationId = formGroup.get('location')?.value;
    /* Delete */
    this.finalObject.locationName = this.locations.find(
      (x) => x.intId === this.finalObject.locationId
    )?.name;
    /* Delete */
    this.finalObject.locationGroupId = formGroup.get('department')?.value;
    /* Delete */
    this.finalObject.locationGroupName = this.locationGroups.find(
      (x) => x.intId === this.finalObject.locationGroupId
    )?.name;
    this.finalObject.industryId = this.industryId;
    this.finalObject.currency = this.currency;
    let currCode: any = this.currencySymbolPipe.transform(this.currency);
    this.finalObject.currencySymbol = currCode;
    this.finalObject.dirty = true;
    this.finalObject.entity = 'User';
    this.finalObject.forProduct = false;
    this.finalObject.majorKpiKicker = false;
    this.finalObject.majorPayoutInPercentage = false;
    if (isDraftMode) {
      let entity = [];
      entity.push({
        industryId: this.industryId,
        tenantId: this.tenantId,
        tenantLocationId: parseInt(this.finalObject.locationId),
        locationGroupId: parseInt(this.finalObject.locationGroupId),
      });
      this.finalObject.incentivePlanEntitys = entity;
      this.finalObject.planMetricType = MetricsDataType[0];
      this.defaultValueForDraft();
    }
  }

  defaultValueForDraft() {
    this.finalObject.fromDate = moment(this.getDateRange()[0]).format(
      'YYYY-MM-DD'
    );
    this.finalObject.toDate = moment(this.getDateRange()[1]).format(
      'YYYY-MM-DD'
    );
    this.finalObject.payoutDateForIncentive = -1;
    this.finalObject.frequency = 'Monthly';
    this.finalObject.tenantReportId = Number(this.majorKpis[0].intId);
  }

  step3Submitt(formGroup: FormGroup, isDraftMode: boolean) {
    this.finalObject.planMetricType =
      MetricsDataType[formGroup.get('planMetricType')?.value];
    let selectedUsers = formGroup
      .get('users')
      ?.value.filter((x: any) => x.userSelected === true);
    let entity: any[] = [];
    /* Delete */
    this.finalObject.selectedUsers = [];
    this.finalObject.userNames = '';
    let SEPRATOR = '';
    selectedUsers?.forEach((val: any) => {
      this.finalObject.userNames =
        this.finalObject.userNames + SEPRATOR + val.userDetail.fullName;
      this.finalObject.selectedUsers.push({
        userId: val.userDetail.intId,
        name: val.userDetail.fullName,
      });
      SEPRATOR = ', ';
      entity.push({
        industryId: this.industryId,
        tenantId: this.tenantId,
        tenantLocationId: parseInt(this.finalObject.locationId),
        locationGroupId: parseInt(this.finalObject.locationGroupId),
        userId: parseInt(val.userDetail.intId),
      });
    });
    this.finalObject.incentivePlanEntitys = entity;
    if (isDraftMode) {
      this.finalObject.tenantReportId = Number(this.majorKpis[0].intId);
    }
    // this.findSimilarIncentive();
  }

  step2Submitt(formGroup: FormGroup, isDraftMode: boolean) {
    this.finalObject.fromDate = moment(this.getDateRange()[0]).format(
      'YYYY-MM-DD'
    );
    this.finalObject.toDate = moment(this.getDateRange()[1]).format(
      'YYYY-MM-DD'
    );
    this.finalObject.frequency =
      IncentiveFrequency[formGroup.get('frequency')?.value];
    if (
      formGroup.get('firstDayOfCycle')?.value !== null &&
      formGroup.get('firstDayOfCycle')?.value !== undefined
    ) {
      this.finalObject.firstDayOfCycle =
        WeekDays[formGroup.get('firstDayOfCycle')?.value];
    }
    if (this.lastDayOfCycle?.toString()) {
      this.finalObject.lastDayOfCycle = WeekDays[this.lastDayOfCycle];
    }
    this.finalObject.payoutDateForIncentive = parseInt(
      formGroup.get('payoutDateForIncentive')?.value
    );
    this.cycleIntervals = getIntervalWithLockedInfo(
      this.getDateRange()[0],
      this.getDateRange()[1],
      this.stepForm[1].get('frequency')?.value,
      this.kid ? this.incentiveEditData.incentivePlanPayouts : null,
      this.stepForm[1].get('payoutDateForIncentive')!.value,
      this.stepForm[1].get('firstDayOfCycle')?.value,
      this.lastDayOfCycle,
      this.kid ? true : false
    );
    this.finalObject.tenantReportId = Number(this.majorKpis[0].intId);
    if (isDraftMode) {
      if (
        this.finalObject.incentivePlanEntitys == null ||
        this.finalObject.incentivePlanEntitys.length == 0
      ) {
        let entity = [];
        entity.push({
          industryId: this.industryId,
          tenantId: this.tenantId,
          tenantLocationId: parseInt(this.finalObject.locationId),
          locationGroupId: parseInt(this.finalObject.locationGroupId),
        });
        this.finalObject.incentivePlanEntitys = entity;
        this.finalObject.planMetricType = MetricsDataType[0];
      }
    }
  }

  step4Submitt(formGroup: FormGroup) {
    this.finalObject.tenantReportId = parseInt(
      formGroup.get('primaryKpi')?.value
    );
    this.finalObject.majorKpiUnit = this.selectedMajorKpi?.unit;
    /* Delete */
    let currCode: any = this.currencySymbolPipe.transform(
      this.finalObject.currency
    );
    this.finalObject.majorKpiDetail = {
      name: this.selectedMajorKpi?.name,
      kpiPrepand:
        this.selectedMajorKpi?.isMoney === true &&
        this.selectedMajorKpi?.unit === null
          ? currCode
          : '',
      kpiAppand:
        this.selectedMajorKpi?.isMoney === false &&
        this.selectedMajorKpi?.unit !== null
          ? this.selectedMajorKpi.unit
          : '',
    };
    this.isPrimaryARevenuKPI = (this.selectedMajorKpi?.isMoney=== true && this.selectedMajorKpi?.isSummable=== true);
    this.finalObject.majorPayoutInPercentage =
      formGroup.get('payoutInPercentage')?.value === '1';
    this.finalObject.majorKpiMultiplier =
      formGroup.get('multiplier')?.value === '1';
    if (this.finalObject.majorKpiMultiplier === true) {
      this.finalObject.majorKpiMultiplierKpi = parseInt(
        formGroup.get('multiplierKpi')?.value
      );
      /* Delete */
      this.finalObject.majorKpiMultiplierDetail = {
        name: this.incentiveTenantReports.find(
          (x) => x.intId === formGroup.get('multiplierKpi')?.value
        )?.name,
      };
    }
  }

  step5Submitt(formGroup: FormGroup) {
    this.finalObject.noOfTiers = parseInt(formGroup.get('noOfTiers')?.value);
    this.finalObject.superTierStartFrom = parseInt(
      formGroup.get('supertier')?.value
    );
    this.finalObject.incentivePlanTier = [];
    const tierInfo = formGroup.get('tiers')?.value;
    tierInfo.forEach((tier: any) => {
      this.finalObject.incentivePlanTier.push({
        tierNo: parseInt(tier.tierNo),
        tierName: tier.tierName,
        tierThreshold: parseFloat(tier.threshold),
        tierPayout: parseFloat(tier.payout),
      });
    });
  }
  step6Submitt(formGroup: FormGroup) {
    this.finalObject.noOfLocationTiers = this.finalObject.noOfTiers;
    if (formGroup.get('locationTierQualify')?.value === '1') {
      this.finalObject.isLocationBased = true;
    } else if (formGroup.get('locationTierQualify')?.value === '0') {
      this.finalObject.isLocationBased = false;
    }
    if (this.finalObject.isLocationBased) {
      if (formGroup.get('locationBasedCommon')?.value === '1') {
        this.finalObject.isLocationBasedCommon = true;
      } else if (formGroup.get('locationBasedCommon')?.value === '0') {
        this.finalObject.isLocationBasedCommon = false;
      }
      if (this.finalObject.isLocationBasedCommon) {
        this.finalObject.monthlyLocationTiers = [];
        const tierInfo = formGroup.get('locationTiers')?.value;
        tierInfo.forEach((tier: any, index: number) => {
          this.finalObject.incentivePlanTier[
            index
          ].locationTierThreshold = parseFloat(tier.locationTierThreshold);
          if (tier.locationTierEnabled === '1') {
            this.finalObject.incentivePlanTier[
              index
            ].locationTierEnabled = true;
          } else {
            this.finalObject.incentivePlanTier[
              index
            ].locationTierEnabled = false;
          }
        });
      } else {
        this.finalObject.monthlyLocationTiers = [];
        const tierInfo = formGroup.get('monthlyLocationTiers')?.value;
        tierInfo.forEach((tier: any) => {
          //const thresholds = tier.get('tierThresholds')?.value;
          tier.tierThresholds.forEach((tierData: any) => {
            this.finalObject.monthlyLocationTiers.push({
              month: tier.month,
              year: tier.year,
              fromDate: moment(tier.fromDate).format('YYYY-MM-DD'),
              toDate: moment(tier.toDate).format('YYYY-MM-DD'),
              day: tier.day,
              tierThreshold: parseFloat(tierData.tierThreshold),
              //frequency: this.finalObject.frequency,
              isLocationTierEnabled:
                tierData.isLocationTierEnabled === '1' ? true : false,
              tierNo: tierData.tierNo,
              tierName: tierData.tierName,
            });
          });
        });
        for (let index = 0; index < this.finalObject.noOfTiers; index++) {
          this.finalObject.incentivePlanTier[
            index
          ].locationTierThreshold = null;
          this.finalObject.incentivePlanTier[index].locationTierEnabled = null;
        }
      }
    }
  }
  step7Submitt(formGroup: FormGroup) {
    this.finalObject.forProduct =
      formGroup.get('productSpecific')?.value === '1';
    const productThreshold = formGroup.get('productThreshold')?.value === '1';
    this.finalObject.incentivePlanProductConfigs = [];
    /* Delete */
    this.finalObject.productNames = '';
    let SEPARATOR = '';
    let pthreshold = '';
    if (this.finalObject.forProduct === true || productThreshold === true) {
      const productInfo = formGroup.get('productThresholds')?.value;
      let currCode: any = this.currencySymbolPipe.transform(
        this.finalObject.currency
      );
      productInfo?.forEach((product: any) => {
        let obj: any = {};
        pthreshold = '';
        obj['locationGroupProductId'] = parseInt(product.productDetail.intId);
        if (productThreshold) {
          obj['threshold'] = parseFloat(product.threshold);
          obj['isPercentage'] = product.thresholdInPercentage === '1';
          //thresholdPerKpi
          pthreshold =
            ' (Cap : ' +
            (!obj['isPercentage'] ? currCode : '') +
            this.decimalPipe.transform(obj['threshold'], '1.2-2') +
            (obj['isPercentage'] ? '%' : '') +
            ')';
        }
        /* Delete */
        this.finalObject.productNames =
          this.finalObject.productNames +
          SEPARATOR +
          product.productDetail.name +
          pthreshold;
        SEPARATOR = ', ';
        this.finalObject.incentivePlanProductConfigs.push(obj);
      });
    }
  }

  step8Submitt(formGroup: FormGroup) {
    this.finalObject.majorKpiKicker = formGroup.get('kicker')?.value === '1';
    if (this.finalObject.majorKpiKicker) {
      this.finalObject.majorKpiKickerKpi = parseInt(
        formGroup.get('kickerKpi')?.value
      );
      let majorKpiKickerKpiObj = this.incentiveTenantReports.find(
        (x) => x.intId === formGroup.get('kickerKpi')?.value
      );
      /* Delete */
      let currCode: any = this.currencySymbolPipe.transform(
        this.finalObject.currency
      );
      this.finalObject.majorKpiKickerDetail = {
        name: majorKpiKickerKpiObj?.name,
        kpiPrepand:
          majorKpiKickerKpiObj?.isMoney === true &&
          majorKpiKickerKpiObj?.unit === null
            ? currCode
            : '',
        kpiAppand:
          majorKpiKickerKpiObj?.isMoney === false &&
          majorKpiKickerKpiObj?.unit !== null
            ? majorKpiKickerKpiObj.unit
            : '',
      };

      this.finalObject.majorKpiKickerQualifyKpiValueFrom = parseFloat(
        formGroup.get('kickerMin')?.value
      );
      this.finalObject.majorKpiKickerQualifyKpiValueTo = parseFloat(
        formGroup.get('kickerMax')?.value
      );
      this.finalObject.majorKpiKickerValueInPercentage =
        formGroup.get('kickerPayoutInPercentage')?.value === '1';
      this.finalObject.majorKpiKickerValue = parseFloat(
        formGroup.get('kickerPayout')?.value
      );
    }
    this.finalObject.maxIncentivePaidOut = parseFloat(
      formGroup.get('maxIncentivePaidOut')?.value
    );
    this.finalObject.maxIncentivePaidOut = isNaN(
      this.finalObject.maxIncentivePaidOut
    )
      ? null
      : this.finalObject.maxIncentivePaidOut;
  }

  getKpis(): void {
    this.tenantReportService
      .findAllByTenantIntIdAndForIncentive(this.tenantId)
      .subscribe((response: any) => {
        if (response && response.errors) {
          const error = response.errors[0].message;
          this.openModal(
            'Oops! Something went wrong. Please try again later.',
            false,
            false,
            true
          );
          console.log(error);
        } else if (
          response.data &&
          response.data.findAllByTenantIntIdAndForIncentive
        ) {
          this.incentiveTenantReports =
            response.data.findAllByTenantIntIdAndForIncentive;
          if (
            this.incentiveTenantReports &&
            this.incentiveTenantReports.length > 0
          ) {
            this.majorKpis = this.incentiveTenantReports.filter(
              (x) => x.isMajorKpi === true
            );
            this.amountResultingKpi = this.incentiveTenantReports.filter(
              (x) => x.isMoney === true && x.isSummable === true
            );
            this.quantityResultingKpi = this.incentiveTenantReports.filter(
              (x) =>
                x.isMoney !== true &&
                x.isSummable === true &&
                ((x.unit && x.unit.trim() != '%') || x.unit)
            );
            const irReport = this.incentiveTenantReports.find(
              (x) => x.reportUniqueCode === 'IR'
            );
            this.selectedMajorKpi = irReport ? irReport : this.majorKpis[0];
            this.iRKpi= irReport? irReport : null;
              this.isPrimaryARevenuKPI = this.selectedMajorKpi.isMoney=== true && this.selectedMajorKpi.isSummable=== true;
              this.stepForm[3]
              .get('primaryKpi')!
              .patchValue(this.selectedMajorKpi.intId);              
          }
          this.kpiObs.next(true);
          this.kpiObs.complete();
        }
      });
  }
  findById(): void {
    if (!this.kid) {
      return;
    }
    this.blockUI.start('Loading...');
    this.incentiveService.findById(this.kid).subscribe((response: any) => {
      this.blockUI.stop();
      if (response && response.errors) {
        const error = response.errors[0].message;
        this.openModal(
          'Oops! Something went wrong. Please try again later.',
          false,
          false,
          true
        );
        console.log(error);
      } else if (response.data && response.data.findById) {
        this.incentiveEditData = response.data.findById;
        /* if (this.incentiveEditData.activeStatus === 'Normal'){
          this.isSaveDraftEnabled = false;
        } */
        this.incentiveStatus = this.incentiveEditData.activeStatus;
        this.setButtonTooltip(this.step);
        this.findByIdObs.next(true);
        this.findByIdObs.complete();
      }
    });
  }
  getLocations(): void {
    this.blockUI.start('Loading...');
    this.locationService
      .findAccessibleLocationsOfUsers(this.tenantId)
      .subscribe((response: any) => {
        this.blockUI.stop();
        if (response && response.errors) {
          const error = response.errors[0].message;
          this.openModal(
            'Oops! Something went wrong. Please try again later.',
            false,
            false,
            true
          );
          console.log(error);
        } else if (
          response.data &&
          response.data.findAccessibleLocationsOfUsers
        ) {
          this.locations = response.data.findAccessibleLocationsOfUsers;
          this.locations.sort((a, b) => a.name.localeCompare(b.name));
          this.isLocationChanged = false;
          if (!this.kid && this.locations && this.locations.length > 0) {
            this.setLocationCurrency(this.locations[0]);
            this.stepForm[0]
              .get('location')!
              .patchValue(this.locations[0].intId);
          } else {
            this.locationObs.next(true);
            this.locationObs.complete();
          }
        }
      });
  }
  locationChange() {
    this.isLocationChanged = true;
  }
  setLocationCurrency(location: Location | undefined) {
    if (location != undefined) {
      this.currency = this.utilService.notBlank(location.localCurrency.code)
        ? location.localCurrency.code
        : location.region.currency;
    }
  }
  getLocationGroups(tenantLocationId: number): void {
    this.blockUI.start('Loading...');
    this.locationGroupService
      .findByLocationId(tenantLocationId)
      .subscribe((response: any) => {
        this.blockUI.stop();
        if (response && response.errors) {
          const error = response.errors[0].message;
          this.openModal(
            'Oops! Something went wrong. Please try again later.',
            false,
            false,
            true
          );
          console.log(error);
        } else if (response.data && response.data.findByLocationId) {
          this.locationGroups = response.data.findByLocationId;
          if (
            !this.kid &&
            this.locationGroups &&
            this.locationGroups.length > 0
          ) {
            this.stepForm[0]
              .get('department')!
              .patchValue(this.locationGroups[0].intId);
          }
          if (
            this.kid &&
            this.isLocationChanged &&
            this.locationGroups &&
            this.locationGroups.length > 0
          ) {
            this.stepForm[0]
              .get('department')!
              .patchValue(this.locationGroups[0].intId);
            this.isLocationChanged = false;
          }
        }
      });
  }
  getUsers(tenantId: number, locationGroupId: number): void {
    this.blockUI.start('Loading...');
    this.userService
      .findUsers(tenantId, locationGroupId)
      .subscribe((response: any) => {
        this.blockUI.stop();
        if (response && response.errors) {
          const error = response.errors[0].message;
          this.openModal(
            'Oops! Something went wrong. Please try again later.',
            false,
            false,
            true
          );
          console.log(error);
        } else if (response.data && response.data.findUsers) {
          this.userList = response.data.findUsers;
          this.userOptions.items = response.data.findUsers;
          this.userOptions.title = 'Search Your Team Members';
          this.userOptions.bindLabel = 'fullName';
          this.userOptions.bindValue = 'intId';
          this.userOptions.items.sort((a, b) =>
            a.fullName.localeCompare(b.fullName)
          );
          this.userOptions.tooltip = incentiveTooltipKeys['incentiveAgentSelection'];
          this.userOptions.bindAvtar = this.profileUrl;
          this.addUserCheckboxes();
          this.cleanUserWarning();
        }
      });
  }
  getProducts(locationGroupId: number): void {
    if (locationGroupId !== this.productFetchForLocationGroupId) {
      this.productFetchForLocationGroupId = locationGroupId;
      this.blockUI.start('Loading...');
      this.productService
        .findAllByLocationGroupIntId(locationGroupId)
        .subscribe((response: any) => {
          this.blockUI.stop();
          if (response && response.errors) {
            const error = response.errors[0].message;
            this.openModal(
              'Oops! Something went wrong. Please try again later.',
              false,
              false,
              true
            );
            console.log(error);
          } else if (
            response.data &&
            response.data.findAllByLocationGroupIntId
          ) {
            this.productList = response.data.findAllByLocationGroupIntId;
            this.productOptions.items =
              response.data.findAllByLocationGroupIntId;
            this.productOptions.bindLabel = 'name';
            this.productOptions.bindValue = 'intId';
            this.productOptions.bindAvtar = 'assets/images/product.png';
            this.productOptions.title = 'Select Your Product';
            this.productOptions.items.sort((a, b) =>
              a.name.localeCompare(b.name)
            );
            this.addProductCheckboxes();
          }
        });
    }
  }
  cleanUserWarning() {
    this.userInAnotherPlan = [];
    this.warningUsersNotInGroup = '';
    this.warnOnOptionValues = [];
  }
  findUsersNotInDepartment() {
    this.warnUserIdsListNotInGroup = [];
    const selectedUsers = this.stepForm[2]
      .get('users')
      ?.value.filter((x: any) => x.userSelected === true);
    const userIds = selectedUsers?.map((x: any) => {
      return parseInt(x['userDetail']?.intId);
    });
    if (userIds != null && userIds.length > 0) {
      const userNotInGroupNow = `userNotInGroupNow {
        intId
        name
      }`;
      this.blockUI.start('Loading...');
      this.incentiveService
        .findById(this.kid, userNotInGroupNow)
        .subscribe((response: any) => {
          this.blockUI.stop();
          if (response && response.errors) {
            const error = response.errors[0].message;
            this.openModal(
              'Oops! Something went wrong. Please try again later.',
              false,
              false,
              true
            );
            console.log(error);
          } else if (response.data && response.data.findById) {
            let notInGrpup = response.data.findById;
            if (
              notInGrpup?.userNotInGroupNow &&
              notInGrpup?.userNotInGroupNow.length > 0
            ) {
              let userNames = '';
              let SEPRATOR = '';
              notInGrpup?.userNotInGroupNow.forEach((o: any) => {
                if (userIds.indexOf(o.intId) > -1) {
                  this.warnUserIdsListNotInGroup.push(o.intId);
                  userNames = userNames + SEPRATOR + o.name;
                  SEPRATOR = ', ';
                }
              });
              this.warnOnOptionValues.push.apply(
                this.warnOnOptionValues,
                this.warnUserIdsListNotInGroup
              );
              if (
                this.warnUserIdsListNotInGroup &&
                this.warnUserIdsListNotInGroup.length > 0
              ) {
                this.warningUsersNotInGroup =
                  `Agent ` +
                  userNames +
                  ` has a role updated or not a part of selected department.`;
              } else {
                this.warningUsersNotInGroup = '';
              }
            } else {
              this.warningUsersNotInGroup = '';
            }
          }
        });
    }
  }
  findSimilarIncentive() {
    const selectedUsers = this.stepForm[2]
      .get('users')
      ?.value.filter((x: any) => x.userSelected === true);
    const userIds = selectedUsers?.map((x: any) => {
      return parseInt(x['userDetail']?.intId);
    });
    let incentiveSearchForm: IncentiveSearchForm = {
      customFilters: null,
      tenantLocationId: null,
      userIds: userIds,
      fromDate: moment(this.getDateRange()[0]).format('YYYY-MM-DD'),
      toDate: moment(this.getDateRange()[1]).format('YYYY-MM-DD'),
      activeStatus: null,
    };
    let incentiveRequest = `totalPages
      totalElements
      size
      numberOfElements
      content {
        kid
        activeStatus
        name
        locationGroupName
        locationName,
        incentivePlanEntitys {
          user{
            intId
            name
          }
        }
      }`;
    if (userIds != null && userIds.length > 0) {
      this.blockUI.start('Loading...');
      const userIdss: any[] = [];
      this.incentiveService
        .findByEntityIds(0, 1000, incentiveSearchForm, incentiveRequest)
        .subscribe((response: any) => {
          if (response && response.errors) {
            const error = response.errors[0].message;
            this.openModal(
              'Oops! Something went wrong. Please try again later.',
              false,
              false,
              true
            );
            console.log(error);
          } else if (
            response.data &&
            response.data.findByEntityIds &&
            response.data.findByEntityIds.content
          ) {
            this.userInAnotherPlan = [];
            this.showUserWarning = true;
            // this.warnOnOptionValues = [];
            response.data.findByEntityIds.content.forEach((plan: any) => {
              if (!this.kid || (this.kid && this.kid != plan.kid)) {
                let SEPRATOR = '';
                let users: any[] = [];
                plan.incentivePlanEntitys.forEach((entity: any) => {
                  if (userIds.indexOf(entity.user.intId) > -1) {
                    userIdss.push(entity.user.intId);
                    users.push(entity.user);
                    SEPRATOR = ', ';
                  }
                });
                this.userInAnotherPlan.push({
                  kid: plan.kid,
                  locationName: plan.locationName,
                  locationGroupName: plan.locationGroupName,
                  name: plan.name,
                  activeStatus: plan.activeStatus,
                  users: users,
                });
              }
            });
          }
          if (this.userInAnotherPlan.length > 0) {
            // this.warningMsg = 'Please check the warning message in summary.';
            this.warnOnOptionValues.push.apply(
              this.warnOnOptionValues,
              userIdss
            );
          } else {
            this.userInAnotherPlan = [];
            // this.warnOnOptionValues = [];
            // this.warningMsg = null;
          }
          this.blockUI.stop();
        });
    } else {
      this.userInAnotherPlan = [];
      // this.warnOnOptionValues = [];
      // this.warningMsg = null;
    }
  }

  developerMode(e: any) {
    var evtobj = window.event ? event : e;
    if (
      (evtobj.keyCode == 112 && evtobj.ctrlKey) ||
      (evtobj.keyCode == 68 && evtobj.ctrlKey && evtobj.altKey)
    ) {
      this.debugMode = !this.debugMode;
    }
  }
  selectedProductItems(event: any[]): void {
    const productIds = event?.map((x: any) => {
      return x?.intId;
    });
    this.products.controls.forEach((formGroup, i) => {
      formGroup.get('productSelected')?.patchValue(false);
    });
    this.products.controls.forEach((formGroup, i) => {
      const prodId = formGroup.get('productDetail')?.value?.intId;
      if (productIds.indexOf(prodId) > -1) {
        formGroup.get('productSelected')?.patchValue(true);
      }
    });
  }
  enabledLoctionTier(i: number, $event: any): void {
    if ($event.target.checked) {
      (this.stepForm[5].get(
        'monthlyLocationTiers'
      ) as FormArray).controls.forEach((tier: any) => {
        (tier.get('tierThresholds') as FormArray).controls[i]
          ?.get('isLocationTierEnabled')
          ?.patchValue('1');
      });
    } else {
      (this.stepForm[5].get(
        'monthlyLocationTiers'
      ) as FormArray).controls.forEach((tier: any) => {
        (tier.get('tierThresholds') as FormArray).controls[i]
          ?.get('isLocationTierEnabled')
          ?.patchValue('0');
      });
    }
  }
  applyToAll(index: number): void {
    const data = (this.stepForm[5].get('monthlyLocationTiers') as FormArray)
      .controls[index];
    const selectedThreshold = data.get('tierThresholds');
    (this.stepForm[5].get(
      'monthlyLocationTiers'
    ) as FormArray).controls.forEach((tier: any) => {
      (tier.get('tierThresholds') as FormArray).controls.forEach(
        (tierThreshold: any, i: number) => {
          tierThreshold
            .get('tierThreshold')
            .patchValue(
              (selectedThreshold as FormArray)?.controls[i]?.get(
                'tierThreshold'
              )?.value
            );
        }
      );
    });
  }
  printLabel(index: number): string {
    const tier = (this.stepForm[5].get('monthlyLocationTiers') as FormArray)
      ?.controls[0];
    return (tier?.get('tierThresholds') as FormArray)?.controls[index]?.get(
      'tierThreshold'
    )?.value
      ? 'Yes'
      : 'No';
  }
  draft() {
    if (this.step < this.steps.length) {
      if (this.stepForm[this.step - 1] && this.stepForm[this.step - 1].valid) {
        this.formSumbittion(true);
        //this.saveAsDraft();
        this.save(true);
      } else {
        focusInvalidControls(this.stepForm[this.step - 1], this.el);
      }
    }
  }
  saveAsDraft() {
    this.blockUI.start('Loading...');
    if (this.kid) {
      this.finalObject.kid = this.kid;
      // Feature not on UI, Setting same fetched from edit
      this.finalObject.note = this.incentiveEditData.note;
      this.finalObject.incentivePlanMinorKpis = this.incentiveEditData.incentivePlanMinorKpis;
      this.finalObject.majorKpiQualify = this.incentiveEditData.majorKpiQualify;
      this.finalObject.majorKpiQualifyKpi = this.incentiveEditData.majorKpiQualifyKpi;
      this.finalObject.majorKpiQualifyKpiValueFrom = this.incentiveEditData.majorKpiQualifyKpiValueFrom;
      this.finalObject.majorKpiQualifyKpiValueTo = this.incentiveEditData.majorKpiQualifyKpiValueTo;
      this.finalObject.holdPayoutInPercentage = this.incentiveEditData.holdPayoutInPercentage;
      this.finalObject.holdPayoutAmount = this.incentiveEditData.holdPayoutAmount;
      this.finalObject.holdPayoutDate = this.incentiveEditData.holdPayoutDate;
      this.finalObject.configureHoldAmount = this.incentiveEditData.configureHoldAmount;
      this.finalObject.isLocationBased = this.incentiveEditData.isLocationBased;
      this.finalObject.stepNumber = this.step;
      //this.finalObject.noOfLocationTiers = this.incentiveEditData.noOfLocationTiers;
      //this.finalObject.monthlyLocationTiers = this.incentiveEditData.monthlyLocationTiers;
      //this.finalObject.isLocationBasedCommon = this.incentiveEditData.isLocationBasedCommon;
    } else {
      this.finalObject.activeStatus = 'Draft';
      if (this.step < 6) {
        this.finalObject.isLocationBased = false;
      }
      this.finalObject.stepNumber = this.step;
      this.finalObject.majorKpiQualify = false;
      this.finalObject.holdPayoutInPercentage = true;
      this.finalObject.configureHoldAmount = false;
    }

    delete this.finalObject.currencySymbol;
    const queryString = {
      query: `mutation{saveOrUpdate(incentivePlan:${this.incentiveService.stringify(
        this.finalObject
      )}){${this.incentiveService.incentiveFields}}}`,
    };
    // console.log(queryString);
    this.incentiveService.gql(queryString).subscribe((response: any) => {
      this.blockUI.stop();
      if (response && response.errors) {
        const error = response.errors[0].message;
        this.openModal(
          'Oops! Something went wrong. Please try again later.',
          false,
          false,
          true
        );
        console.log(error);
      } else if (response.data && response.data.saveOrUpdate) {
        this.router.navigate(['incentive']);
      }
    });
  }
  validateAllStep(step: number) {
    let isValid = false;
    for (let index = 1; index <= step; index++) {
      this.step = index;
      if (this.stepForm[this.step] && this.stepForm[this.step].valid) {
        this.formSumbittion(true);
        isValid = true;
      } else {
        focusInvalidControls(this.stepForm[this.step - 1], this.el);
        this.setButtonTooltip(this.step);
        isValid = false;
      }
    }
    if (isValid) {
      this.goto(step + 1);
    }
  }
  max(max: number, message?: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const defaultMessage = `Max length of this field is ${length}`;
      let val: number = control.value;

      if (val == undefined || val.toString().trim() == '') {
        return null;
      }
      if (val <= max) {
        return null;
      }
      return {
        max: { value: control.value, message: message || defaultMessage },
      };
    };
  }
  getmultiplierKpiName() {
    if (this.stepForm[3].controls['payoutInPercentage'].value === '1') {
      if (this.stepForm[3].controls['multiplier'].value === '1') {
        return this.stepForm[3].controls['payoutInPercentage'].value === '1'
          ? this.amountResultingKpi.find(
              (x: any) =>
                x.intId == this.stepForm[3].controls['multiplierKpi'].value
            )?.name
          : this.quantityResultingKpi.find(
              (x: any) =>
                x.intId == this.stepForm[3].controls['multiplierKpi'].value
            )?.name;
      } else {
        const irReport = this.incentiveTenantReports.find(
          (x) => x.reportUniqueCode === 'IR'
        );
        return irReport?.name;
      }
    } else if (this.stepForm[3].controls['payoutInPercentage'].value === '0') {
      if (this.stepForm[3].controls['multiplier'].value === '1') {
        return this.stepForm[3].controls['payoutInPercentage'].value === '1'
          ? this.amountResultingKpi.find(
              (x: any) =>
                x.intId == this.stepForm[3].controls['multiplierKpi'].value
            )?.name
          : this.quantityResultingKpi.find(
              (x: any) =>
                x.intId == this.stepForm[3].controls['multiplierKpi'].value
            )?.name;
      }
    }
    return '';
  }
  openModal(
    message: string,
    isSuccess: boolean,
    displayOkButton: boolean,
    showCloseButton: boolean
  ) {
    let modalData = {
      modalClassName: 'modal-md',
      width: '100%',
      type: isSuccess ? 'Success' : 'Error',
      title: isSuccess ? 'Success' : 'Error',
      html: {
        text: ' ' + message,
      },
      showCloseButton: showCloseButton,
      buttons: [
        displayOkButton
          ? {
              text: 'OK',
              value: true,
              visible: true,
              className: 'btn-primary',
              closeModal: true,
            }
          : {},
      ],
      closeOnClickOutside: false,
      closeOnEsc: true,
    };
    this.customModal.open(modalData);
  }

  changeFilter(incentiveObj: any) {
    if (incentiveObj.kid == null || incentiveObj.kid == '') {
      return;
    }
   // let timeString = 'T' + moment().format('HH:mm:ss') + '.000Z';
    let locationId = incentiveObj.incentivePlanEntitys[0].tenantLocationId?.toString();
    let filterArray = this.filterService.getFilterMetaData(this.storedFilterId);
    filterArray = filterArray ? filterArray : [];
    let locationFilter = filterArray.find((x: any) => x.id === 'location');
    let statusFilter = filterArray.find((x: any) => x.id === 'status');
    let dateFilter = filterArray.find((x: any) => x.id === 'incentivePlanDate');

    if (dateFilter) {
      filterArray.forEach((element: any) => {
        if (element.id === 'incentivePlanDate') {
          element.selectedDateValue = [
            incentiveObj.fromDate,
            incentiveObj.toDate,
          ];
          element.fromDate = incentiveObj.fromDate;
          element.toDate = incentiveObj.toDate;
          element.selectedFilter = 'range';
        }
      });
    } else {
      filterArray.push({
        id: 'incentivePlanDate',
        searchFields: ['fromDate', 'toDate'],
        name: 'Date Range',
        multipleSearchField: true,
        type: 'date',
        selectedFilter: 'range',
        fromDate: incentiveObj.fromDate,
        toDate: incentiveObj.toDate,
        selectedDateValue: [
          incentiveObj.fromDate,
          incentiveObj.toDate,
        ],
      });
    }

    if (locationFilter) {
      filterArray.forEach((element: any) => {
        if (element.id === 'location') {
          element.selectedValue = locationId;
          element.fromDate = incentiveObj.fromDate;
          element.toDate = incentiveObj.toDate;
        }
      });
    } else {
      filterArray.push({
        id: 'location',
        searchField: 'tenantLocationId',
        name: 'Location',
        type: 'entity',
        entityType: 'TenantLocation',
        parent: null,
        selectedValue: locationId,
        multipleSearchField: false,
        selectedFilter: null,
        fromDate: incentiveObj.fromDate,
        toDate: incentiveObj.toDate,
      });
    }
    /*
    if(statusFilter){
      filterArray.forEach((element: any) => {
        if(element.id==='status'){
          element.selectedValue=incentiveObj.activeStatus;
          element.fromDate= incentiveObj.fromDate+timeString;
				  element.toDate= incentiveObj.toDate+timeString;
        }
      });
    }else{
      filterArray.push({
				"id": "status",
				"name": "Status",
				"type": "enum",
				"enumType": "incentiveStatus",
				"searchField": "activeStatus",
				"selectedValue": incentiveObj.activeStatus,
				"multipleSearchField": false,
				"selectedFilter": null,
				"fromDate": incentiveObj.fromDate+timeString,
				"toDate": incentiveObj.toDate+timeString
			})
    }
    */

    this.filterService.setFilterMetaData(this.storedFilterId, filterArray);
  }

  setButtonTooltip(step:any){
    this.stepCancel = this.wizardKey ? (this.wizardKey + step + ".cancel") : '';
    this.stepContinue = this.wizardKey ? (this.wizardKey + step + ".continue") : '';
    this.stepFinish = this.wizardKey ? (this.wizardKey + step + ".finish") : '';
    this.stepSkip = this.wizardKey ? (this.wizardKey + step + ".skip") : '';
    this.stepPrev = this.wizardKey ? (this.wizardKey + step + ".prev") : '';
    this.draftButtonTooltip = this.wizardKey ? (this.wizardKey + step + ".draft") : '';
  }
  openUser(event: any) {
    if (this.canEditUserProfile) {
      const eventData = new EventsJson();
      eventData.module = ModuleConstant.INCENTIVE_PLAN;
      eventData.subModule = SubModuleConstant.INCENTIVE_WIZARD;
      eventData.action = ActionConstant.RECORD_VIEW;
      eventData.actionOn = ActionOnConstant.USER_PROFILE;
      eventData.entityType = EntityTypeConstant.USER;
      eventData.entityId = event.intId;
      this.eventTracking.createEvent(eventData);
      this.router.navigate([]).then((result) => {
        window.open(
          'user/profile/' + event.intId + '?tab=information',
          '_blank'
        );
      });
    }
  }
  get incentiveTooltipKeys(){
    return incentiveTooltipKeys;
  }
}

