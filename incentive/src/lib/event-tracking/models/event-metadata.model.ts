export class EventMetaData {
  eventId!: any;
  tenantId!: any;
  tenantLocationId!: any;
  userId!: any;
  roleId!: any;
  geolocation!: any;
  latitude!: any;
  longitude!: any;
  browser!: any;
  browserVersion!: any;
  platform!: any;
  localTimezone!: any;
  localTime!: any;
  locale!: string;
  updatedOn!: any;
  notes!: string;
}
