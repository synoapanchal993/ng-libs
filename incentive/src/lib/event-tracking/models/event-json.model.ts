import { EventMetaData } from './event-metadata.model';
export class EventJson {
  module!: any;
  subModule!: any;
  entity!: any;
  entityId!: any;
  action!: any;
  subAction!: any;
  isEventFromUIUX!: any;
  tenantLocationId!: any;
  tenantId!: any;
  actionBy!: any;
  actionOn!: any;
  isWeb!: any;
  eventMetaData!: EventMetaData;
  industryId!: any;
}
