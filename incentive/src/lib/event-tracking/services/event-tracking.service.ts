import { Injectable } from '@angular/core';
import { LocalStorageService } from '../../service/local-storage.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { EventBody } from '../models/event-body.model';
import { EventJson } from '../models/event-json.model';
import { EventMetaData } from '../models/event-metadata.model';
import { DeviceDetectorService } from 'ngx-device-detector';
import { UtilService } from '../../service/util.service';
import { IAMService } from '../../service/iam.service';
import * as moment from 'moment';
import { EventsJson } from '../models/events-json';

@Injectable()
export class EventTrackingService {
  protocol: string;
  host: string;
  port?: string;
  constructor(
    router: Router,
    private localStorageService: LocalStorageService,
    public http: HttpClient,
    private deviceDetectorService: DeviceDetectorService,
    private utilService: UtilService,
    private iamService: IAMService
  ) {
    this.protocol = window.location.protocol;
    this.host = window.location.hostname;
    this.getPosition();
  }

  registerEvent(diffEventJson: any) {
    let eventJson = new EventJson();
    eventJson.module = this.utilService.notBlank(diffEventJson['module'])
      ? diffEventJson['module']
      : undefined;
    eventJson.subModule = this.utilService.notBlank(diffEventJson['subModule'])
      ? diffEventJson['subModule']
      : diffEventJson['module'];
    eventJson.action = this.utilService.notBlank(diffEventJson['action'])
      ? diffEventJson['action']
      : undefined;
    eventJson.subAction = this.utilService.notBlank(diffEventJson['subAction'])
      ? diffEventJson['subAction']
      : diffEventJson['action'];
    eventJson.entity = this.utilService.notBlank(diffEventJson['entity'])
      ? diffEventJson['entity']
      : undefined;
    eventJson.entityId = this.utilService.notBlank(diffEventJson['entityId'])
      ? diffEventJson['entityId']
      : undefined;
    eventJson.isEventFromUIUX = this.localStorageService.get('newUI') ? 1 : 0;
    eventJson.isWeb = 1;
    eventJson.actionBy = this.iamService.getUser()
      ? this.iamService.getUser()?.intId
      : undefined;
    eventJson.industryId = this.iamService.getIndustry();
    let eventMetaData = new EventMetaData();
    eventMetaData.tenantId = this.iamService.getTenant()
      ? this.iamService.getTenant()?.intId
      : undefined;
    eventMetaData.tenantLocationId = this.utilService.notBlank(
      diffEventJson['tenantLocationId']
    )
      ? diffEventJson['tenantLocationId']
      : undefined;
    eventMetaData.userId = this.utilService.notBlank(diffEventJson['userId'])
      ? diffEventJson['userId']
      : this.iamService.getUser()
      ? this.iamService.getUser()?.intId
      : undefined;
    eventMetaData.roleId = this.utilService.notBlank(diffEventJson['roleId'])
      ? diffEventJson['roleId']
      : this.iamService.getRole()
      ? this.iamService.getRole()?.intId
      : undefined;
    eventMetaData.geolocation = this.utilService.notBlank(
      diffEventJson['geolocation']
    )
      ? diffEventJson['geolocation']
      : undefined;
    eventMetaData.latitude = this.utilService.notBlank(
      this.localStorageService.get('latitude')
    )
      ? this.localStorageService.get('latitude')
      : undefined;
    eventMetaData.longitude = this.utilService.notBlank(
      this.localStorageService.get('longitude')
    )
      ? this.localStorageService.get('longitude')
      : undefined;
    eventMetaData.browser = this.deviceDetectorService.browser;
    eventMetaData.browserVersion = this.deviceDetectorService.browser_version;
    eventMetaData.platform = 2;
    eventMetaData.localTimezone = new Date()
      .toString()
      .match(/\((.*)\)/)
      ?.pop();
    eventMetaData.locale = moment.locale();
    eventJson.eventMetaData = eventMetaData;
    eventMetaData.notes = this.utilService.notBlank(diffEventJson['notes'])
      ? diffEventJson['notes']
      : undefined;
    let eventBody = new EventBody();
    eventBody.topic = 'Event-tracking';
    eventBody.message = eventJson;
    let params: HttpParams = new HttpParams();
    let baseUrl =
      this.protocol +
      '//' +
      this.host +
      '/event-tracking' +
      '/sendEventToKafka';
    return this.http.post(baseUrl, eventBody, { params: params });
  }

  setEventObjectToKafkaV2(diffEventJson: any) {
    let eventsJson = new EventsJson();
    eventsJson.roleId = this.iamService.getRole()
      ? this.iamService.getRole()?.intId
      : undefined;
    eventsJson.roleName = this.iamService.getRole()
      ? this.iamService.getRole()?.name
      : undefined;
    eventsJson.module = this.utilService.notBlank(diffEventJson['module'])
      ? diffEventJson['module']
      : undefined;
    eventsJson.subModule = this.utilService.notBlank(diffEventJson['subModule'])
      ? diffEventJson['subModule']
      : diffEventJson['module'];
    eventsJson.action = this.utilService.notBlank(diffEventJson['action'])
      ? diffEventJson['action']
      : undefined;
    eventsJson.actionOn = this.utilService.notBlank(diffEventJson['actionOn'])
      ? diffEventJson['actionOn']
      : diffEventJson['action'];
    eventsJson.entityType = this.utilService.notBlank(
      diffEventJson['entityType']
    )
      ? diffEventJson['entityType']
      : undefined;
    eventsJson.entityId = this.utilService.notBlank(diffEventJson['entityId'])
      ? diffEventJson['entityId']
      : undefined;
    eventsJson.actionBy = this.iamService.getUser()
      ? this.iamService.getUser()?.intId
      : undefined;
    eventsJson.kpi = this.utilService.notBlank(diffEventJson['kpi'])
      ? diffEventJson['kpi']
      : undefined;
    eventsJson.varianceType = this.utilService.notBlank(
      diffEventJson['varianceType']
    )
      ? diffEventJson['varianceType']
      : undefined;
    eventsJson.varianceFromDate = this.utilService.notBlank(
      diffEventJson['varianceFromDate']
    )
      ? diffEventJson['varianceFromDate']
      : undefined;
    eventsJson.varianceToDate = this.utilService.notBlank(
      diffEventJson['varianceToDate']
    )
      ? diffEventJson['varianceToDate']
      : undefined;
    eventsJson.industryId = this.iamService.getIndustry();
    //eventsJson.regionId = this.utilService.notBlank(diffEventJson['tenantLocationId']) ? this.getRegionId(diffEventJson['tenantLocationId']) : undefined;
    eventsJson.tenantId = this.iamService.getTenant()
      ? this.iamService.getTenant()?.intId
      : undefined;
    eventsJson.tenantLocationId = this.utilService.notBlank(
      diffEventJson['tenantLocationId']
    )
      ? diffEventJson['tenantLocationId']
      : undefined;
    eventsJson.isEventFromUIUX = this.localStorageService.get('newUI') ? 1 : 0;
    eventsJson.platform = 'WEB';
    eventsJson.browserName = this.deviceDetectorService.browser;
    eventsJson.browserVersion = this.deviceDetectorService.browser_version;
    eventsJson.localTimezone = new Date()
      .toString()
      .match(/\((.*)\)/)
      ?.pop();
    eventsJson.locale = moment.locale();
    eventsJson.version = this.utilService.notBlank(diffEventJson['version'])
      ? diffEventJson['version']
      : 1;
    eventsJson.latitude = this.utilService.notBlank(
      this.localStorageService.get('latitude')
    )
      ? this.localStorageService.get('latitude')
      : undefined;
    eventsJson.longitude = this.utilService.notBlank(
      this.localStorageService.get('longitude')
    )
      ? this.localStorageService.get('longitude')
      : undefined;

    let eventBody = new EventBody();
    eventBody.topic = 'Event-tracking';
    eventBody.message = eventsJson;
    let params: HttpParams = new HttpParams();
    let baseUrl =
      this.protocol +
      '//' +
      this.host +
      '/event-tracking' +
      '/sendEventToKafka';
    return this.http.post(baseUrl, eventBody, { params: params });
  }

  getRegionId(locationId: any) {
    let regionId = 0;
    let locations = this.iamService.getAccessibleLocations();
    locations.forEach((location) => {
      if (location.id == locationId) {
        regionId = location.regionId;
      }
    });
    return regionId;
  }

  getPosition(): Promise<any> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (resp) => {
          resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
          this.localStorageService.set('latitude', resp.coords.latitude);
          this.localStorageService.set('longitude', resp.coords.longitude);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
}
