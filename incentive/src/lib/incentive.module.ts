import { CommonModule, CurrencyPipe, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CustomModalModule } from '@fpg-component/custom-modal';
import { FilterModule, FilterService } from '@fpg-component/filter';
import { SelectorModule, SelectorService } from '@fpg-component/selector';
import { TableModule, TableService } from '@fpg-component/table';
import { ValidationModule } from '@fpg-component/validation';
import { WizardModule } from '@fpg-component/wizard';
import { NgSelectModule } from '@ng-select/ng-select';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { IncentiveAddComponent } from './incentive-add/incentive-add.component';
import { IncentiveRoutingModule } from './incentive-routing.module';
import { IncentiveSearchComponent } from './incentive-search/incentive-search.component';
import { IncentiveSummaryComponent } from './incentive-summary/incentive-summary.component';
import { IncentiveComponent } from './incentive.component';
import { IAMService } from './service/iam.service';
import { IncentiveService } from './service/incentive.service';
import { LocationGroupService } from './service/location-group.service';
import { LocationService } from './service/location.service';
import { ProductService } from './service/product.service';
import { TenantReportService } from './service/tenant-report.service';
import { UserService } from './service/user.service';
import { UtilService } from './service/util.service';
import { LocalStorageService } from './service/local-storage.service';
import { EventTrackingService } from './event-tracking/services/event-tracking.service';
import { CurrencySymbolPipe } from './currencySymbolPipe.pipe';
import { AppTooltipModule, AppTooltipService } from '@fpg-component/app-tooltip';
import { EventFrameworkModule } from 'event-framework';
import { UploadService, SharedUtilityModule } from 'shared-utility';

@NgModule({
  declarations: [
    IncentiveComponent,
    IncentiveAddComponent,
    IncentiveSearchComponent,
    IncentiveSummaryComponent,
    CurrencySymbolPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    IncentiveRoutingModule,
    NgSelectModule,
    ButtonsModule.forRoot(),
    TooltipModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    WizardModule,
    ValidationModule,
    FilterModule,
    TableModule,
    CustomModalModule,
    SelectorModule,
    AppTooltipModule,
    EventFrameworkModule,
    SharedUtilityModule
  ],
  providers: [
    LocalStorageService,
    IncentiveService,
    LocationService,
    FilterService,
    UserService,
    ProductService,
    LocationGroupService,
    TenantReportService,
    UtilService,
    IAMService,
    TableService,
    SelectorService,
    EventTrackingService,
    CurrencySymbolPipe,
    DecimalPipe,
    AppTooltipService,
    UploadService
  ],
})
export class IncentiveModule {}
